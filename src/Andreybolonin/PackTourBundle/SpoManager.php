<?php

namespace Andreybolonin\PackTourBundle;

use Buzz\Browser;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

class SpoManager
{

    public function __construct(
        Container $container,
        Filesystem $filesystem,
        LogManager $log_manager,
        Browser $buzz,
        \PDO $pdo_backend,
        \PDO $pdo_master,
        \GearmanClient $gearman_client
    ) {
        $this->container = $container;
        $this->filesystem = $filesystem;
        $this->log_manager = $log_manager;
        $this->buzz = $buzz;
        $this->pdo_backend = $pdo_backend;
        $this->pdo_master = $pdo_master;
        $this->gearman_client = $gearman_client;
    }

    /**
     * Получение и запись в БД списка SPO по каждому оператору
     */
    public function getOperatorList()
    {
        $operators = $this->container->getParameter('operators');
        foreach ($operators as $operator_id) {
            $parser = $this->getParser($operator_id);
            $parser->getAndSaveSpoList($operator_id);
        }
    }

    /**
     * Создает и возвращает обьект парсера по operator_id
     * с установленными параметрами
     *
     * @param $operator_id
     * @return mixed
     */
    public function getParser($operator_id)
    {
        $class_name = 'Andreybolonin\\PackTourBundle\\Parser\\' . ucfirst($operator_id) . 'Parser';
        $parser = new $class_name();
        $parser->setParams($this->container, $this->log_manager, $this->buzz, $this->pdo_backend, $this->pdo_master, $this->gearman_client);

        return $parser;
    }

    /**
     * Подготовка файлов для скачивания - изменение статуса на start_downloaded.
     *
     * @return array
     */
    public function getSpoToParse()
    {
        $limit = $this->container->getParameter('count_of_download_spo');
        $operators = $this->container->getParameter('operators');
        $operators_str = "'" . implode("','", $operators) . "'";
        $new_spo = array();

        // Если уже есть скачанные spo
//        $rows = db::rows('SELECT * FROM ' . self::$table . ' WHERE status in (\'downloaded\') AND operator IN (' . $operators . ') ORDER BY id LIMIT ' . $limit);
//        if (!empty($rows)) {
//            return $rows;
//        }

        // Если уже есть подготовленные для скачивания spo
//        $rows = db::rows(
//            'SELECT * FROM ' . self::$table . ' WHERE status in (\'start_downloaded\') AND operator IN (' . $operators . ') ORDER BY id LIMIT ' . $limit
//        );
//        if (!empty($rows)) {
//            return $rows;
//        }

        // Если spo нет - подготавливаем
        $spo = $this->pdo_backend->query(
            'SELECT * FROM xml_tour.spo
             WHERE status in (\'nottouched\') AND operator IN (' . $operators_str . ')
             ORDER BY id LIMIT ' . $limit)
            ->fetchAll();

        foreach ($spo as $row) {
            $row['status'] = 'start_downloaded';
            $new_spo[] = $row;
        }
        if (count($new_spo)) {
            PdoHelper::MultiInsertOrUpdate($this->pdo_backend, 'xml_tour.spo', $new_spo);

            return $new_spo;
        }
    }

    /**
     * Скачивает XML файл по записи SPO
     *
     * @param $spo
     * @return mixed
     */
    public function downloadSpo($spo)
    {
        // Т.к. у Mouzenidis туры приходят в одном файле который мы получаем по HTTP
        if ($spo['operator'] == 'mouzenidis') {
            $spo['status'] = 'downloaded';

            return $spo;
        }

        // Если пришел уже скачанный файл - ничего не делаем
        if ($spo['status'] == 'downloaded') {
            return $spo;
        }

        $ext = strtolower(substr($spo['url'], strrpos($spo['url'], '.') + 1));
        $filePath = $this->createFolders(__DIR__ . '/Files/', $ext);

        $filePathArr = explode('/', $filePath);
        $fileName = $filePathArr[count($filePathArr) - 1];
        unset($filePathArr[count($filePathArr) - 1]);
        $filePathArr[] = $spo['operator'] . '_' . $fileName;
        $file = implode('/', $filePathArr);

        $proc = popen(' wget -c --tries=10 -O "' . $file . '" "' . html_entity_decode($spo['url']) . '" 2>&1','rb');
        $stringAll = NULL;

        while (!feof($proc)) {
            $data = fgets($proc, 100);
//            print($data);
            $stringAll .= trim($data);
        }

        $spo['path'] = $file;
        $spo['status'] = 'downloaded';
        $spo['count_load']++;

        $stmt = $this->pdo_backend->prepare('UPDATE xml_tour.spo SET status = :status, path = :path, count_load = :count_load WHERE id = :id');
        $stmt->bindParam('id', $spo['id']);
        $stmt->bindParam('status', $spo['status']);
        $stmt->bindParam('path', $spo['path']);
        $stmt->bindParam('count_load', $spo['count_load']);
        $stmt->execute();

        pclose($proc);

        return $spo;
    }

    /**
     * Возвращает путь и имя файла для записи в заданном каталоге($baseFolder)
     * Создаёт иерархию каталогов
     *
     * @param $baseFolder
     * @param  string      $ext
     * @return bool|string
     */
    public function createFolders($baseFolder, $ext = 'xml')
    {
        // Уникальная директория и имя
        $FileName = uniqid() . '.' . $ext;
        $folder = $baseFolder. uniqid() . '/';

        // Создаём папку
        if (!$this->filesystem->exists($folder)) {
            try {
                $this->filesystem->mkdir($folder);
            } catch (IOException $e) {
                echo "An error occurred while creating your directory";
            }
        }

        return $folder . $FileName;
    }

    /**
     * Получает обьект парсера.
     * Если у парсера определен метод domParser() (обьектный парсер) - вызывает его.
     * Если нет - то обычный метод params()
     *
     * @param array $spo
     * @return bool
     */
    public function parseSpo(array $spo)
    {
        $parser = $this->getParser($spo['operator']);

        if (is_callable(array($parser, 'domParser'))) {
            $parser->domParser($spo);
        } else {
            $parser->params($spo);
        }
    }

    /**
     * @param  array $spo
     * @return array
     */
    public function analyzeSpo(array $spo)
    {
        if (!file_exists($spo['path'])) {
            return false;
        }

        $extention = trim(strtolower(substr($spo['path'], strrpos($spo['path'], '.') + 1)));
        switch ($extention) {
            case 'gz':
                $spo = $this->unGzip($spo);
                break;
            case 'xml':
                $spo['status'] = 'analised';
//                if (isset(self::$availableToEncode[$spo['operator']])) {
//                    $spo['path'] = self::encoding($spo['path']);
//                }
                break;
            case 'zip':
                $spo = $this->unZip($spo);
                break;
        }

        $stmt = $this->pdo_backend->prepare('UPDATE xml_tour.spo SET status = :status, path = :path WHERE id = :id');
        $stmt->bindParam('id', $spo['id']);
        $stmt->bindParam('status', $spo['status']);
        $stmt->bindParam('path', $spo['path']);
        $stmt->execute();

        return $spo;

    }

    /**
     * Распаковывает файл из zip-архива
     *
     * @param $file
     * @return bool|string
     */
    public function unpack($file)
    {
        $ext = strtolower( substr( $file, strrpos( $file, '.') + 1) );
        $dir = FS_FILES . '/xml_unzip_tmp/' . time();

        if ( mkdir($dir, 0777) === false ) {
            return false;
        }

        // Распаковываем
        if ($ext == 'zip') {
            exec('unzip   ' . $file . ' -d ' . $dir , $output);
        } elseif ($ext == 'rar') {
            exec('unrar e   ' . $file . ' ' . $dir , $output);
        }
        $dir = $dir . '/';

        // Находим файл
        $file_unpack = $dir . current( scandir( $dir,1 ) );

        if (!file_exists($file_unpack)) {
            return false;
        }

        // Создаем новый файл куда будем копировать разархивированый
        $ext_unpack = strtolower( substr( $file_unpack, strrpos( $file_unpack, '.') + 1) );
        $error = array();
        $file_name = $this->createFolders( FS_FILES_XML, $error, $ext_unpack);
        if (empty($error)) {
            exec('mv "' . $file_unpack . '"  "' . $file_name . '"');
            unlink( $file );
            rmdir( $dir );

            return $file_name;
        }

        return false;
    }

    /**
     * Проверяем кодировку и если нужно конвертируем в UTF-8
     *
     * @param $file
     * @return mixed
     */
    public function encoding($file)
    {
        $proc = popen('file -i ' . '"' . $file .'"' ,'r');
        $encodingString = null;
        while (!feof($proc)) {
            $data = fgets($proc,100);
            $encodingString .= $data;
        }
        $arrEncod = explode('charset=', $encodingString);
        $encoding = trim($arrEncod[1]);

        $file_content = file_get_contents($file);
        mb_internal_encoding('utf-8');
        $file_content_utf8 = mb_convert_encoding($file_content, 'UTF-8', $encoding);
        if (file_exists($file)) {
            unlink($file);
        }
        file_put_contents($file, $file_content_utf8);

        return $file;
    }

    /**
     * @param $spo
     * @internal param $filename
     * @return mixed
     */
    public function unZip($spo)
    {
        //$dstName = str_replace('.zip', '', $filename);
//        $filePath = Spo::createFolders(FS_FILES_XML, $error, 'xml', true);
//
//        $zip = new \ZipArchive();
//        //$zip->get;
//        $open = $zip->open($filename);
//        var_dump($open);
//        $extract = $zip->extractTo($filePath);
//        var_dump($extract);
//        $zip->close();

        $s_file_unpack = $this->unpack($spo['path']);
        if ($s_file_unpack == false) {
            $spo['status'] = 'downloaded';
        } else {
            $spo['status'] = 'analised';
            $spo['path'] = $s_file_unpack;
        }

        return $spo;
    }

    /**
     * @param $spo
     * @return mixed
     */
    public function unGzip($spo)
    {
//        $dstName = str_replace('.gz', '', $srcName);
//        $sfp = gzopen($srcName, "rb");
//        $fp = fopen($dstName, "w");
//
//        while ($string = gzread($sfp, 4096)) {
//            fwrite($fp, $string, strlen($string));
//        }
//        gzclose($sfp);
//        fclose($fp);

        $new_path = str_replace('.gz', '', $spo['path']);
        exec('gunzip ' . $spo['path']);
        $spo['status'] = 'analised';
        if (rename($new_path, $new_path . '.xml')) {
            $spo['path']= ($new_path . '.xml');

            return $spo;
        }

        return $spo;
    }

    /**
     * Проверяет расширение перевадаемого пути файла,
     * если расширение входит в список допустимых - true, если не входит - false
     *
     * @param $filepath
     * @param  null $needle_ext
     * @return bool
     */
    public function checkExtension($filepath, $needle_ext = null)
    {
        if ($needle_ext) {
            $ext_array[] = $needle_ext;
        } else {
            $ext_array = array('xml', 'zip', 'gz');
        }

        $search_ext = pathinfo($filepath, PATHINFO_EXTENSION);

        if (in_array($search_ext, $ext_array)) {
            return true;
        } else {
            return false;
        }
    }

}
