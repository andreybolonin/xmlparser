<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TourParserCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('tour:parser')
            ->setDescription('Start parser process');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tour_manager = $this->getContainer()->get('tour_manager');
        $tour_manager->parser();
    }

}
