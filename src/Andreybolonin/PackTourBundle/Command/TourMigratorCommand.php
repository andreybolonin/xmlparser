<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TourMigratorCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('tour:migrator')
            ->setDescription('Start migrator process');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tour_manager = $this->getContainer()->get('tour_manager');
        $tour_manager->migrator();
    }

}
