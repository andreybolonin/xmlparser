<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OperatorAddCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('operator:add')
            ->setDescription('Command for operator add. Create all need tables in datebase.')
            ->addArgument(
                'operator_id',
                InputArgument::REQUIRED,
                'Operator_id option is required'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operator = $input->getArgument('operator_id');
        $pdo_master = $this->getContainer()->get('pdo_master');
        $pdo_backend = $this->getContainer()->get('pdo_backend');

        $xml_tour = "CREATE TABLE IF NOT EXISTS xml_tour.xml_tour_" . $operator . " (
          `id` bigint(11) NOT NULL AUTO_INCREMENT,
          `tour` double NOT NULL,
          `tourOperator` varchar(255) NOT NULL,
          `spo` mediumint(11) NOT NULL,
          `cityDepatured` int(11) DEFAULT NULL,
          `room` int(11) NOT NULL,
          `hotel` int(11) NOT NULL,
          `star` int(10) NOT NULL,
          `city` int(11) NOT NULL,
          `country` int(11) NOT NULL,
          `arrivalRegion` int(11) DEFAULT NULL,
          `departuredRegion` int(11) DEFAULT NULL,
          `dateStart` date NOT NULL,
          `dateEnd` date NOT NULL,
          `nightCount` smallint(6) NOT NULL,
          `pansion` int(11) NOT NULL,
          `stayType` int(11) NOT NULL,
          `adult` smallint(6) NOT NULL,
          `children` smallint(6) DEFAULT NULL,
          `infant` smallint(6) DEFAULT NULL,
          `price` decimal(11,0) NOT NULL,
          `update` tinyint(4) NOT NULL DEFAULT '0',
          `moved` tinyint(4) NOT NULL DEFAULT '0',
          `currency` enum('USD','EUR','UAH','RUB') DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `tour` (`spo`,`tour`),
          KEY `dateStart` (`dateStart`),
          KEY `moved` (`moved`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $pdo_backend->exec($xml_tour);


        $city = "CREATE TABLE IF NOT EXISTS pack_tours.city_" . $operator . " (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(55) NOT NULL,
          `country_id` int(11) DEFAULT '0',
          PRIMARY KEY (`id`),
          UNIQUE KEY `name` (`name`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $pdo_master->exec($city);

        $country = "CREATE TABLE IF NOT EXISTS pack_tours.country_" . $operator . " (
          `id` int(3) NOT NULL AUTO_INCREMENT,
          `code` varchar(55) NOT NULL,
          `name` varchar(55) NOT NULL,
          `url` varchar(255) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `name` (`name`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

        $pdo_master->exec($country);

        $hotel = "CREATE TABLE IF NOT EXISTS pack_tours.hotel_" . $operator . " (
          `id` int(11) NOT NULL,
          `name` varchar(55) NOT NULL,
          `star` varchar(10) NOT NULL,
          `city` int(11) NOT NULL,
          `country` int(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $pdo_master->exec($hotel);

        $pansion = "CREATE TABLE IF NOT EXISTS pack_tours.pansion_" . $operator . " (
          `id` int(11) NOT NULL,
          `name` varchar(55) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $pdo_master->exec($pansion);

        $room = "CREATE TABLE IF NOT EXISTS pack_tours.room_" . $operator . " (
          `id` int(11) NOT NULL,
          `name` varchar(55) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $pdo_master->exec($room);

        $room_ids = "CREATE TABLE IF NOT EXISTS pack_tours.room_ids_" . $operator . " (
          `id` int(11) NOT NULL,
          `roomType` int(11) NOT NULL,
          `roomDesc` int(11) NOT NULL,
          `roomAccom` int(11) NOT NULL,
          UNIQUE KEY `id` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $pdo_master->exec($room_ids);

        $room_accomodations = "CREATE TABLE IF NOT EXISTS pack_tours.room_accomodations_" . $operator . " (
          `id` int(11) NOT NULL,
          `name` varchar(55) NOT NULL,
          `code` varchar(55) NOT NULL,
          `adult` int(11) DEFAULT NULL,
          `child` int(11) DEFAULT NULL,
          UNIQUE KEY `id` (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $pdo_master->exec($room_accomodations);

        $star = "CREATE TABLE IF NOT EXISTS pack_tours.star_" . $operator . " (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `star` varchar(20) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `star` (`star`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;";

        $pdo_master->exec($star);

        $stay_type = "CREATE TABLE IF NOT EXISTS pack_tours.stay_type_" . $operator . " (
          `id` int(11) NOT NULL,
          `name` varchar(55) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $pdo_master->exec($stay_type);

        $tour_type = "CREATE TABLE IF NOT EXISTS pack_tours.tourtype_" . $operator . " (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(55) NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;";

        $pdo_master->exec($tour_type);


        $pack_tour = "CREATE TABLE IF NOT EXISTS pack_tours.tour_" . $operator . " (
          `id` bigint(11) NOT NULL AUTO_INCREMENT,
          `cityFrom` int(5) NOT NULL DEFAULT '0',
          `room` tinyint(2) NOT NULL,
          `hotel` int(11) NOT NULL,
          `star` tinyint(2) NOT NULL,
          `resort_id` int(5) NOT NULL,
          `country_id` int(5) NOT NULL,
          `ration` tinyint(3) NOT NULL,
          `start` int(11) NOT NULL,
          `nights` tinyint(3) NOT NULL,
          `adults` tinyint(3) NOT NULL,
          `childs` tinyint(3) NOT NULL DEFAULT '0',
          `price` int(6) NOT NULL,
          `currency` enum('USD','EUR','RUB','UAH') COLLATE utf8_bin NOT NULL,
          `at_all` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `uni` (`hotel`,`star`,`ration`,`room`,`start`,`nights`,`adults`,`childs`),
          KEY `start_date` (`start`),
          KEY `cnt_str` (`country_id`,`start`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='utf8_bin' AUTO_INCREMENT=1 ;";

        $pdo_master->exec($pack_tour);
    }
}
