<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TourStatCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('tour:stat')
            ->setDescription('Update statistic of parse and packet tours');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tour_manager = $this->getContainer()->get('tour_manager');
        $tour_manager->saveStatistic();
    }

}
