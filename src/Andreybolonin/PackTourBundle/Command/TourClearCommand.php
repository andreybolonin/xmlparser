<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TourClearCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('tour:clear')
            ->setDescription('Clear parse and pack tours by date');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tour_manager = $this->getContainer()->get('tour_manager');
        $tour_manager->deleteByDate();
        $tour_manager->deletePackToursByDate();
    }

}
