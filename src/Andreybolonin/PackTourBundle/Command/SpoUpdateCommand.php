<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SpoUpdateCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('spo:update')
            ->setDescription('Update spo list');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $spo_manager = $this->getContainer()->get('spo_manager');
        $spo_manager->getOperatorList();
    }

}
