<?php

namespace Andreybolonin\PackTourBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OperatorDebugCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('operator:debug')
            ->setDescription('Display all active operators');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $operators = $this->getContainer()->getParameter('operators');
        foreach ($operators as $operator) {
            $output->writeln('<info>'.$operator.'</info>');
        }
    }
}
