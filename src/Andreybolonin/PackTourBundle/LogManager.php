<?php

namespace Andreybolonin\PackTourBundle;

class LogManager
{
    //const EMERG   = 0;  // Emergency: system is unusable
    //const ALERT   = 1;  // Alert: action must be taken immediately
    //const CRIT    = 2;  // Critical: critical conditions
    const ERR     = 'error';  // Error: error conditions
    const WARN    = 'warning';  // Warning: warning conditions
    const NOTICE  = 'notice';  // Notice: normal but significant condition
    //const INFO    = 6;  // Informational: informational messages
    //const DEBUG   = 7;  // Debug: debug messages

    // Типовые ошибки XML Parser
    const SPO_LIST = 1;
    const NOT_LOAD_LIST = 2;
    const FIRST_TIME_NOT_LOAD = 3;
    const UNZIP_ERROR = 4;
    const ERROR_XML = 5;
    const NO_SUCH_COUNTRY = 6;
    const EMPTY_COUNTRY_TAG = 7;
    const EMPTY_CITY = 8;
    const EMPTY_HOTEL = 9;
    const EMPTY_ROOM = 10;
    const EMPTY_STAY_TYPE = 11;
    const EMPTY_PANSION = 12;
    const NOT_LOAD = 13;
    const NOT_NAME = 14;
    const NO_HOTEL_ROOM = 15;
    const NO_ROOM = 16;
    const NO_REGION = 17;
    const NO_STAR = 20;
    const NO_HOTEL_PANSION = 21;
    const NO_GROUP_TYPE_AGE = 22;
    const CRON_WORK_MORE_THEN = 23;
    const NOT_LOAD_MANY_TIMES = 24;
    const NO_ID = 25;
    const NO_ACCOMODATION = 26;
    const NO_SPACE_ON_DISK = 27;
    const NO_ROOM_KEY = 28;
    const NO_BUILDING_KEY = 29;

    protected $table = 'xml_tour.xml_tour_log';
    public $logs = array();

    public function __construct(\PDO $pdo_master, \PDO $pdo_backend)
    {
        $this->pdo_master = $pdo_master;
        $this->pdo_backend = $pdo_backend;
    }

    /**
     * Логирование ошибок XML Parser. Делает запись в xml_tour.log
     *
     * @param null $type     - тип записи(error, warning, notice)
     * @param null $operator - id оператора
     * @param null $code     - id ошибки
     * @param null $about    - дополнительное описание
     * @internal param null $group - этап на котором произошла ошибка
     */
    public function log($type = null, $operator = null, $code = null, $about = null)
    {
        $XmlTourLog = new XmlTourLog();
        $XmlTourLog->setType($type);
//        $XmlTourLog->setGroup($group);
        $XmlTourLog->setOperator($operator);
        $XmlTourLog->setCode($code);
        $XmlTourLog->setAbout($about);
        $XmlTourLog->setDate(new \DateTime());

        $this->logs[] = $XmlTourLog;
    }

    /**
     * Сохраняет ранее накопленные ошибки в БД
     */
    public function saveLogs()
    {
        if (isset($this->logs) && count($this->logs)) {
            Db::PdoMultiInsertOrUpdate($this->pdo_backend, 'xml_tour.xml_tour_log', $this->logs);
            $this->logs = array();
        }
    }

    /**
     * Логирование ошибок XML Migrator
     *
     * @param $operator
     * @param $error_type
     * @param $catalog_id
     */
    public function migrationSaveLog($operator, $error_type, $catalog_id)
    {
        $this->logs[] = array(
            'operator' => $operator,
            'catalog_id' => $catalog_id,//entity_operator_id
            'error_type' => $error_type,
        );
    }

    /**
     * Делает инсерт в mysql relation.log с общим кол-вом,
     * которое считает из накопленного кол-ва данных в кеше
     *
     * @return bool
     */
    public function migrationSaveLogs()
    {
        if ($this->logs && count($this->logs) > 0) {
            $container = $this->groupMigrationErrors($this->logs);
            Db::PdoMultiInsertOrUpdate($this->pdo_master, 'pack_tours.relation_log', $container);
        }
    }

    /**
     * Группирует накопленные ошибки в процессе миграции по оператору, типу ошибки и ID
     *
     * @param $errors
     * @return mixed
     */
    public function groupMigrationErrors($errors)
    {
        if (!count($errors) > 0) {
            return false;
        }
        $container = array();

        foreach ($errors as $error) {
            $hash = md5($error['operator'] . $error['catalog_id'] . $error['error_type']);
            $error['hash'] = $hash;
            $error['updated_at'] = date("Y-m-d H:i:s", time());
            $error['count_errors'] = 1;
            if (isset($container[$hash])) {
                $error = $container[$hash];
                $error['count_errors']++;
                $container[$hash] = $error;
                continue;
            }
            $container[$hash] = $error;
        }

        return $container;
    }

    /**
     * Возвращает логи отсортированные по периоду: все, текщий месяц, текущая неделя, сегодняшний день
     *
     * @param $period
     * @return array
     */
    public function getRelationLogs($period)
    {
        $d = new \DateTime();

        // month
        $first_day = $d->modify('first day of this month')->format('Y-m-d');
        $last_day = $d->modify('last day of this month')->format('Y-m-d');

        // week
        $first_monday = $d->modify('monday this week')->format('Y-m-d');
        $last_monday = $d->modify('monday next week')->format('Y-m-d');

        if ($period == 'all') {
            $log = $this->pdo_master->query('SELECT user_id, COUNT(id) AS count FROM pack_tours.relation_log GROUP BY user_id')->fetchAll();
        } elseif ($period == 'month') {
            $stmt = $this->pdo_master->prepare('SELECT user_id, COUNT(id) AS count FROM pack_tours.relation_log WHERE created_at > :first AND created_at < :last GROUP BY user_id');
            $stmt->execute(array(':first' => $first_day, ':last' => $last_day));
            $log = $stmt->fetchAll();
        } elseif ($period == 'week') {
            $stmt = $this->pdo_master->prepare('SELECT user_id, COUNT(id) AS count FROM pack_tours.relation_log WHERE created_at > :first AND created_at < :last GROUP BY user_id');
            $stmt->execute(array(':first' => $first_monday, ':last' => $last_monday));
            $log = $stmt->fetchAll();
        } elseif ($period == 'day') {
            $log = $this->pdo_master->query('SELECT user_id, COUNT(id) AS count FROM pack_tours.relation_log WHERE DATE(created_at) = DATE(NOW()) GROUP BY user_id')->fetchAll();
        }

        return $log;
    }

}
