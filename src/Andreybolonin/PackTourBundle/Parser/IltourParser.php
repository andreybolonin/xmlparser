<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;
use Symfony\Component\DomCrawler\Crawler;

class IltourParser extends Xmlpullreader
{

    private $url = 'http://iltour.com.ua/xml/';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $crawler = new Crawler($html, $this->url);
        $files = $crawler->filter('a');

        foreach ($files as $file) {
            if (!Xmlpullreader::checkExtension($file->getAttribute('href'))) {
                continue;
            }

            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $this->url . $file->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $this->url . $file->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * Инициализация справочных данных
     * Для обновления или загрузки одного справочника $allCatalog = FALSE , $curentCatalog =  название каталога
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('ALF');
//
////        $this->initCountry($allCatalog, $curentCatalog, $this, self::$table);
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
////            $aCountry = Spo::getIsoCountry(array('id', 'shortName'));
//            $aCountry = IsoCountry::getIsoCountries();
//            foreach ($aCountry as $val) {
//                $this->country[$val['shortName']] = array(
//                    'id' => $val['id'],
//                    'name' => 'countryName'
//                );
//            }
//
////            $aCountrySelf = Spo::get_type_list(array('id', 'code'), $table . 'country');
//            $aCountrySelf = Country::findAllByOperator($operator_id);
//            foreach ($aCountrySelf as $val) {
//                $this->countrySelf[$val['code']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = Hotel::findAllByOperator($operator_id);
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = Star::findAllByOperator($operator_id);
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = City::findAllByOperator($operator_id);
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRoom') {
//            $aRoom = Room::findAllByOperator($operator_id);
//            foreach ($aRoom as $val) {
//                $this->roomIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = StayType::findAllByOperator($operator_id);
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//
////        $this->initHotel($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initHotelStar($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initCity($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initRegion($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initRoom($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initStayType($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initPansion($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initTourType($allCatalog, $curentCatalog, $this, self::$table);
//    }

    /**
     * Отбор туров
     *
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $node_attributes = $node['node']['attributes'];
        $operator_id = Operator::getOperatorId('ALF');

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            if (isset($node_attributes['SPO'])) {
                $this->spoName = $node_attributes['SPO'];
                $this->currentSpo = $node_attributes['SPO'];
            }

            // В теге одна страна
            if (strpos($node_attributes['COUNTRY'], ',') === false) {
                if (!isset($this->countrySelf[$node_attributes['COUNTRY']])) {
                    // Проверяем есть ли в общем каталоге
                    if (strlen($node_attributes['COUNTRY']) > 0) {
                        if (!isset($this->country[$node_attributes['COUNTRY']])) {
                            Country::addCountry(
                                array(
                                    'code' => trim($node_attributes['COUNTRY']),
                                    'name' => trim($node_attributes['COUNTRY']),
                                    'url' => 'https://www.google.com.ua/search?q=' . trim($node_attributes['COUNTRY']) . '+iso+country+code&oq=' . trim($node_attributes['COUNTRY']) . '+iso+country+code'
                                ),
                                $operator_id
                            );
                        } else {
                            Country::addCountry(
                                array(
                                    'code' => trim($node_attributes['COUNTRY']),
                                    'name' => $this->country[$node_attributes['COUNTRY']]['name']
                                ),
                                $operator_id
                            );
                        }
                        $this->initializeArray(false, 'initCountry');
                    } else {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, $operator_id, Log::EMPTY_COUNTRY_TAG, $this->currentSpoId);
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$node_attributes['COUNTRY']]) ? $this->countrySelf[$node_attributes['COUNTRY']] : null;
            } else {
                $countryArray = explode(',', $node_attributes['COUNTRY']);
                if (!isset($this->countrySelf[$countryArray[1]])) {
                    if (!isset($this->country[$node_attributes['COUNTRY']])) {
                        Country::addCountry(
                            array(
                                'code' => trim($countryArray[1]),
                                'name' => trim($countryArray[1]),
                            ),
                            $operator_id
                        );
                    } else {
                        Country::addCountry(
                            array(
                                'code' => trim($countryArray[1]),
                                'name' => $this->country[$countryArray[1]]['name']
                            ),
                            $operator_id
                        );
                    }
                    $this->initializeArray(false, 'initCountry');
                }
                $this->eachTour['country'] = isset($this->countrySelf[$countryArray[1]]) ? $this->countrySelf[$countryArray[1]] : null;
            }

            if (!isset($this->tourtype[$node_attributes['TOURTYPE']])) {
                TourType::addTourType(
                    array(
                        'name' => trim($node_attributes['TOURTYPE'])
                    ),
                    $operator_id
                );
                $this->initializeArray(true, 'initTourType');
            }

            $this->tour_type = $this->tourtype[$node_attributes['TOURTYPE']] ? $this->tourtype[$node_attributes['TOURTYPE']] : 0;
            $this->eachTour['adult'] = $node_attributes['ADL'];
            $this->eachTour['children'] = $node_attributes['CHD'];
            $this->eachTour['infant'] = $node_attributes['INF'];
            $this->eachTour['currency'] = $node_attributes['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node_attributes['STAR'])) {
                $hotelStar = strlen($node_attributes['STAR']);
                if (!isset($this->hotelStar[$hotelStar])) {
                    Star::addStar(
                        array('star' => $hotelStar),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node_attributes['CITY']]) && $this->errorTour === false) {
                if (strlen($node_attributes['CITY']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node_attributes['CITY']),
                            'country_id' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, $operator_id, Log::EMPTY_CITY, $this->currentSpoId);
                }
            }
            $this->eachTour['city'] = $this->region[$node_attributes['CITY']];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node_attributes['HTC']]) && $this->errorTour === false) {
                if (strlen($node_attributes['HTC'] > 0)) {
                    Hotel::addHotel(
                        array(
                            'id'      => $node_attributes['HTC'],
                            'name'    => $node_attributes['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[$node_attributes['CITY']],
                            'country' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, $operator_id, Log::EMPTY_COUNTRY_TAG, $this->currentSpoId);
                }
            }
            $this->eachTour['hotel'] = $node_attributes['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node_attributes['RMC']])) {
                if (strlen($node_attributes['RMC'] > 0)) {
                    Room::addRoom(
                        array(
                            'id' => trim($node_attributes['RMC']),
                            'name' => trim($node_attributes['ROOM'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, $operator_id, Log::EMPTY_ROOM, $this->currentSpoId);
                }
            }
            $this->eachTour['room'] = $node_attributes['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node_attributes['PLC']])) {
                if (strlen($node_attributes['PLC'] > 0)) {
                    StayType::addStayType(
                        array(
                            'id' => trim($node_attributes['PLC']),
                            'name' => trim($node_attributes['PLACE'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, $operator_id, Log::EMPTY_STAY_TYPE, $this->currentSpoId);
                }
            }
            $this->eachTour['stayType'] = $node_attributes['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node_attributes['MLC']])) {
                if (strlen($node_attributes['MLC']) > 0 && strlen($node_attributes['MEAL']) > 0) {
                    Pansion::addPansion(array(
                            'id' => trim($node_attributes['MLC']),
                            'name' => trim($node_attributes['MEAL'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, $operator_id, Log::EMPTY_PANSION, $this->currentSpoId);
                }
            }
            $this->eachTour['pansion'] = $node_attributes['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node_attributes['NAME'] == 'FLIGHT') {

                if (isset($this->region[$node_attributes['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node_attributes['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node_attributes['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['CITYTO']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node_attributes['CITYTO']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

            } elseif ($node_attributes['NAME'] == 'BACK FLIGHT') {

                if (isset($this->region[$node_attributes['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node_attributes['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['departuredRegion'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // Сортировка по дате и актуальности туров
            if (strtotime($node_attributes['DATE']) > time() &&
                strtotime($node_attributes['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node_attributes['DATE'];
                $this->eachTour['price'] = $node_attributes['VAL'];
                $this->eachTour['nightCount'] = $node_attributes['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart'])+86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = $operator_id;
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, $operator_id, Log::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

}
