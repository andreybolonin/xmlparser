<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\Finder\Finder;

class TuristclubParser extends Xmlpullreader
{

    private $ftp_server = "46.164.131.210";
    private $ftp_user = 'tourobzor';
    private $ftp_password = 'TourObzor';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их в таблицу list
     */
    public function getAndSaveSpoList($operator_id)
    {
        $finder = new Finder();
        $files = $finder->files()
            ->name('*.xml')
            ->name('*.zip')
            ->name('*.rar')
            ->name('*.gzip')
            ->in('ftp://' . $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . '/');

        $spo_array = array();

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getPathname() . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $file->getPathname(),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * Инициализация справочных данных
     * Для обновления или загрузки одного справочника $allCatalog = FALSE , $curentCatalog = 	название каталога
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('TURISTCLUB');
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
//            $aCountry = Country::findAllByOperator($operator_id);
//            foreach ($aCountry as $val) {
//                $this->country[$val['id']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = Hotel::findAllByOperator($operator_id);
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = Star::findAllByOperator($operator_id);
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = City::findAllByOperator($operator_id);
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRoom') {
//            $aRoom = Room::findAllByOperator($operator_id);
//            foreach ($aRoom as $val) {
//                $this->roomIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = StayType::findAllByOperator($operator_id);
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//    }

    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('TURISTCLUB');
        $node_attributes = $node['node']['attributes'];

        if (isset($node['name']) && $node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            $this->spoName = $node_attributes['SPO'];
            $this->currentSpo = $node_attributes['SPO'];

            // В теге одна страна
            if (strpos($node_attributes['COUNTRY'], ',') === false) {
                if (!isset($this->country[(int) $node_attributes['COUNTRY']])) {
                    $this->errorTour = true;
                    if (strlen($node_attributes['COUNTRY']) > 0) {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::NO_SUCH_COUNTRY, $this->currentSpoId);
                    } else {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::EMPTY_COUNTRY_TAG, $this->currentSpoId);
                    }
                }
                $this->eachTour['country'] = isset($this->country[(int) $node_attributes['COUNTRY']]) ? $this->country[(int) $node_attributes['COUNTRY']] : null;
                // В теге несколько стран
            } else {
                $countryArray = explode(',', $node_attributes['COUNTRY']);
                $cntrId_0 = (int) $countryArray[0];
                $cntrId_1 = (int) $countryArray[1];

                if (!isset($this->country[$cntrId_0]) && !isset($this->country[$cntrId_1])) {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::NO_SUCH_COUNTRY, $this->currentSpoId);
                } else {
                    if (isset($this->country[$cntrId_0])) {
                        $cntrId = $cntrId_0;
                    }
                    if (isset($this->country[$cntrId_1])) {
                        $cntrId = $cntrId_1;
                    }
                }
                $this->eachTour['country'] = isset($cntrId) ? $cntrId : null;
            }

            if (!isset($this->tourtype[$node_attributes['TOURTYPE']])) {
                TourType::addTourType(
                    array('name' => trim($node_attributes['TOURTYPE'])),
                    $operator_id
                );
                $this->initializeArray(true, 'initTourType');
            }

            $this->tour_type = $this->tourtype[$node_attributes['TOURTYPE']] ? $this->tourtype[$node_attributes['TOURTYPE']] : 0;
            $this->eachTour['adult'] = $node_attributes['ADL'];
            $this->eachTour['children'] = $node_attributes['CHD'];
            $this->eachTour['infant'] = $node_attributes['INF'];
            $this->eachTour['currency'] = $node_attributes['CURRENCY'];
        }

        if (isset($node['name']) && $node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node_attributes['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node_attributes['STAR']));
                if (strlen($hotelStar) == 0) {
                    $hotelStar = 'no star';
                }
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    Star::addStar(array('star' => $hotelStar), $operator_id);
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[trim($node_attributes['CITY'])]) && $this->errorTour === false) {
                if (strlen($node_attributes['CITY']) > 0 ) {
                    City::addCity(
                        array('name' => trim($node_attributes['CITY']), 'country_id' => $this->eachTour['country']),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::EMPTY_CITY, $this->currentSpoId);
                }
            }
            $this->eachTour['city'] = $this->region[trim($node_attributes['CITY'])];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node_attributes['HTC']]) && $this->errorTour === false) {
                if (strlen($node_attributes['HTC'] > 0)) {
                    Hotel::addHotel(
                        array(
                            'id'      => $node_attributes['HTC'],
                            'name'    => $node_attributes['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[trim($node_attributes['CITY'])],
                            'country' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::EMPTY_HOTEL, $this->currentSpoId);
                }
            }
            $this->eachTour['hotel'] = $node_attributes['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node_attributes['RMC']])) {
                if (strlen($node_attributes['RMC'] > 0)) {
                    Room::addRoom(
                        array('id' => trim($node_attributes['RMC']), 'name' => trim($node_attributes['ROOM'])),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::EMPTY_ROOM, $this->currentSpoId);
                }
            }
            $this->eachTour['room'] = $node_attributes['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node_attributes['PLC']])) {
                if (strlen($node_attributes['PLC'] > 0)) {
                    StayType::addStayType(
                        array('id' => trim($node_attributes['PLC']), 'name' => trim($node_attributes['PLACE'])),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::EMPTY_STAY_TYPE, $this->currentSpoId);
                }
            }
            $this->eachTour['stayType'] = $node_attributes['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node_attributes['MLC']])) {
                if (strlen($node_attributes['MLC']) > 0 && strlen($node_attributes['MEAL']) > 0) {
                    Pansion::addPansion(
                        array('id' => trim($node_attributes['MLC']), 'name' => trim($node_attributes['MEAL'])),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::EMPTY_PANSION, $this->currentSpoId);
                }
            }
            $this->eachTour['pansion'] = strlen($node_attributes['MLC']) == 0 ? 'no cat' : $node_attributes['MLC'];
        }

        if (isset($node['name']) && $node['name'] == 'TRANSPORT') {
            if ($node_attributes['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node_attributes['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) > 0) {
                    City::addCity(
                        array('name' => trim($node['node']['attributes']['CITYFR']), 'country_id' => null),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node_attributes['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['CITYTO']) > 0) {
                    City::addCity(
                        array('name' => trim($node['node']['attributes']['CITYTO']), 'country_id' => null),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node_attributes['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node_attributes['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) > 0) {
                    City::addCity(
                        array('name' => trim($node['node']['attributes']['CITYFR']), 'country_id' => null),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['departuredRegion'] = $this->region[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if (isset($node['name']) && $node['name'] == 'PRICE') {
            // Cортировка по дате и актуальности туров
            if (/*strtotime($node_attributes['DATE']) > time() &&
                strtotime($node_attributes['DATE']) < Registry::get('parser_start_date') &&*/
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node_attributes['DATE'];
                $this->eachTour['price'] = $node_attributes['VAL'];
                $this->eachTour['nightCount'] = $node_attributes['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('TURISTCLUB');
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('TURISTCLUB'), Log::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

}
