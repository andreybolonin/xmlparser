<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\Finder\Finder;

class PanukraineParser extends Xmlpullreader
{
    private $ftp_server = 'panukraine.kiev.ua';
    private $ftp_user = 'greatwall';
    private $ftp_password = 'ecnfytimeuflsdfnm';
    // ftp://greatwall:ecnfytimeuflsdfnm@panukraine.kiev.ua

    public $xml_read_keys = array(
        'CITY_FROM','CITY_FROM_ID',
        'CITY_TO','CITY_TO_ID',
        'CITY','CITY_ID',
        'HOTEL','HOTEL_ID',
        'STAR',
        'ROOM','ROOM_ID',
        'MEAL','MEAL_ID',
        'ALLOCATION','ALLOCATION_ID',
        'ADL','CHD1','CHD2',
        'DATE','DURATION',
        'PRICE','CURRENCY','PRICE_ID',
        'FLIGHT_FROM','FLIGHT_FROM_ID',
        'FLIGHT_TO','FLIGHT_TO_ID',
        'COUNTRY_TO','COUNTRY_TO_ID',
    );

    /**
     * Получает список SPO и сохраняет их
     * Warning: is_dir(): connect() failed: Connection timed out in
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
//        $spo = array();
//        $this->errorExit = false;
//        $link = ftp_connect($this->ftp_server);
////        $operator_id = Operator::getOperatorId('PANUKRAINE');
//
//        if (ftp_login($link, $this->ftp_user, $this->ftp_password) != true) {
//            $this->allOperators[XML_PANUKRAINE]['work'] = false;
//            $this->allOperators[XML_PANUKRAINE]['time'] = time();
//            $this->operatorListUpdate($this->allOperators);
//            $this->errorExit = true;
//
//            return false;
//        }
//
//        ftp_pasv($link, true);
//        $urlFtp =  "ftp://$this->ftp_user:$this->ftp_password@$this->ftp_server";
//        $dir = '';
//        $file_list = ftp_rawlist($link, $dir);

        $spo = array();
        $finder = new Finder();
        $files = $finder->files()
            ->name('*.xml')
            ->name('*.zip')
            ->name('*.rar')
            ->name('*.gzip')
            ->in('ftp://' . $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . '/');

        foreach ($files as $file) {
            var_dump($file->getPathname());exit;

            // Разбиваем строку по пробельным символам
            //list($acc, $bloks, $group, $user, $size, $month, $day, $year, $file) = preg_split('/[\s]+/', $file);
            $arr = preg_split('/[\s]+/', $file);

            if (count($arr) > 8) {
                foreach ($arr as $key => $elsement) {
                    if ($key > 8) {
                        $arr[8] .= ' ' . $arr[$key];
                        unset($arr[$key]);
                    }
                }
            }

            // Если директория то открываем
            if (substr($arr[0], 0, 1) == 'd' && $arr[8] != '.' && $arr[8] != '..') {
                if ( substr($dir, -1) != '/') {
                    $dir = $dir . '/';
                }
                $this->spoListOperatorFtp($link, $dir  . $arr[8] . '/');
            }

            // Если файл записываем в массив
            if (substr($arr[0], 0, 1) == '-' && strtolower(substr($arr[8], strrpos($arr[8], '.') + 1)) == 'zip') {
                //if (isset(strtolower(substr($arr[8], strrpos($arr[8], '.') + 1)))) {
                //iconv('utf-8', 'utf-8//IGNORE', $dir . $arr[8])
                $spo[@iconv('KOI8-U', 'utf-8//IGNORE', $dir . $arr[8])] =  array(
                    'id' => @iconv('KOI8-U', 'utf-8//IGNORE', $dir . $arr[8]),
                    'status' => 'nottouched',
                    'date' => date('Y-m-d H:i:s')
                );
                //}
            }
        }

        if ($this->errorExit === false) {
            $this->ftpWorkWithList(XML_PANUKRAINE, $urlFtp);
        }
        ftp_close($link);

        $this->saveSpo($spo);
    }

    /**
     * @param  string     $param
     * @return mixed|void
     */
//    protected function initializeArray($param = 'all')
//    {
//        $operator_id = Operator::getOperatorId('PANUKRAINE');
//
//        if ($param == 'all' || $param == 'initCountry') {
//            $this->country = Country::initCountry($operator_id);
//        }
//
//        if ($param == 'all' || $param == 'initHotel') {
//            $this->hotel = Hotel::initHotel($operator_id);
//        }
//
//        if ($param == 'all' || $param == 'initHotelStar') {
//            $this->hotelStar = Star::initStar($operator_id);
//        }
//
//        if ($param == 'all' || $param == 'initRegion') {
//            $this->city = City::initCity($operator_id);
//        }
//
//        if ($param == 'all' || $param == 'initRoom') {
//            $this->roomIds = Room::initRoom($operator_id);
//        }
//
//        if ($param == 'all' || $param == 'initStayType') {
//            $this->stayTypeIds = StayType::initStayType($operator_id);
//        }
//
//        if ($param == 'all' || $param == 'initPansion') {
//            $this->pansionIds = Pansion::initPansion($operator_id);
//        }
//
////        if ($allCatalog === true || $curentCatalog == 'initCountry') {
////            $aCountry = $this->tblTour->selectFields(array('id') ,'xml_panukraine_country');
////            foreach ($aCountry as $val) {
////                $this->country[$val['id']] = '';
////            }
////            unset($aCountry);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'initHotel') {
////            $aHotel = $this->tblTour->selectFields(array('id'), 'xml_panukraine_hotel');
////            foreach ($aHotel as $val) {
////                $this->hotel[$val['id']] = '';
////            }
////            unset($aHotel);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
////            $aHotelStar = $this->tblTour->selectFields(array(), 'xml_panukraine_star');
////            foreach ($aHotelStar as $val) {
////                $this->hotelStar[$val['star']] = $val['id'];
////            }
////            unset($aHotelStar);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'initRegion') {
////            $aRegion = $this->tblTour->selectFields(array(), 'xml_panukraine_region');
////            foreach ($aRegion as $val) {
////                $this->city[$val['id']] = $val['country_id'];
////            }
////            unset($aRegion);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'initRoom') {
////            $aRoom = $this->tblTour->selectFields(array('id'), 'xml_panukraine_room');
////            foreach ($aRoom as $val) {
////                $this->roomIds[$val['id']] = '';
////            }
////            unset($aRoom);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'initPansion') {
////            $aPansion = $this->tblTour->selectFields(array('id'), 'xml_panukraine_pansion');
////            foreach ($aPansion as $val) {
////                $this->pansionIds[$val['id']] = '';
////            }
////            unset($aPansion);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'initStayType') {
////            $aStayType = $this->tblTour->selectFields(array('id'), 'xml_panukraine_stay_type');
////            foreach ($aStayType as $val) {
////                $this->stayTypeIds[$val['id']] = '';
////            }
////            unset($aStayType);
////        }
//    }

    /**
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('PANUKRAINE');
        $this->eachTour['spo'] = $this->currentSpoAuto;

        if ($node['name'] == 'CITY_FROM' || $node['name'] == 'CITY_FROM_ID') {

            if ($node['name'] == 'CITY_FROM') {
                $this->stackUkr['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'CITY_FROM_ID') {
                $this->stackUkr['id'] = $node['node']['body'];
                if (!isset($this->city[$node['node']['body']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_region', array($this->stackUkr), true);
                    $this->initializeArray('initRegion');
                }
                $this->stackUkr = array();
//        $this->eachTour['cityDepatured'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'CITY_TO' || $node['name'] == 'CITY_TO_ID') {
            if ($node['name'] == 'CITY_TO') {
                $this->stackCityTo['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'CITY_TO_ID') {
                $this->stackCityTo['id'] = $node['node']['body'];
//        $this->eachTour['arrivalRegion'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'CITY' || $node['name'] == 'CITY_ID') {
            if ($node['name'] == 'CITY') {
                $this->stackCity['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'CITY_ID') {
                $this->stackCity['id'] = $node['node']['body'];
                $this->eachTour['city'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'HOTEL' || $node['name'] == 'HOTEL_ID' || $node['name'] == 'STAR') {
            if ($node['name'] == 'HOTEL') {
                $this->stackHotel['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'HOTEL_ID') {
                $this->stackHotel['id'] = $node['node']['body'];
                $this->eachTour['hotel'] = $node['node']['body'];
                $this->stackHotel['city'] = $this->stackCity['id'];
            } elseif ($node['name'] == 'STAR') {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['body']));
                if (!isset($this->hotelStar[$hotelStar])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_star', array(array('star' => $hotelStar)), true);
                    $this->initializeArray('initHotelStar');
                }
                if (!isset($this->hotelStar[$hotelStar])) {
                    $this->errorTour = true;
                }
                $this->stackHotel['star'] = $this->hotelStar[$hotelStar];
                $this->eachTour['star'] = $this->hotelStar[$hotelStar];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'ROOM' || $node['name'] == 'ROOM_ID') {

            if ($node['name'] == 'ROOM') {
                $this->stackRoom['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'ROOM_ID') {
                $this->stackRoom['id'] = $node['node']['body'];
                if (!isset($this->roomIds[$node['node']['body']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_room', array($this->stackRoom), true);
                    $this->initializeArray('initRoom');
                }
                if (!isset($this->roomIds[$node['node']['body']])) {
                    $this->errorTour = true;
                }
                $this->stackRoom = array();
                $this->eachTour['room'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'MEAL' || $node['name'] == 'MEAL_ID') {

            if ($node['name'] == 'MEAL') {
                $this->stackPansion['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'MEAL_ID') {
                $this->stackPansion['id'] = $node['node']['body'];
                if (!isset($this->pansionIds[$node['node']['body']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_pansion', array($this->stackPansion), true);
                    $this->initializeArray('initPansion');
                }
                if (!isset($this->pansionIds[$node['node']['body']])) {
                    $this->errorTour = true;
                }
                $this->stackPansion = array();
                $this->eachTour['pansion'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'ALLOCATION' || $node['name'] == 'ALLOCATION_ID') {
            if ($node['name'] == 'ALLOCATION') {
                $this->stackstayType['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'ALLOCATION_ID') {
                $this->stackstayType['id'] = $node['node']['body'];
                if (!isset($this->stayTypeIds[$node['node']['body']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_stay_type', array($this->stackstayType), true);
                    $this->initializeArray('initStayType');
                }
                if (!isset($this->stayTypeIds[$node['node']['body']])) {
                    $this->errorTour = true;
                }

                $this->stackstayType = array();
                $this->eachTour['stayType'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'ADL') {
            $this->eachTour['adult'] = $node['node']['body'];
        }
        if ($node['name'] == 'CHD1') {
            $this->eachTour['children'] = $node['node']['body'];
        }
        if ($node['name'] == 'CHD2') {
            $this->eachTour['infant'] = $node['node']['body'];
        }

        if ($node['name'] == 'DATE') {
            $dateStart = substr($node['node']['body'],0,10);
            $this->eachTour['dateStart'] = $dateStart;
            $this->eachTour['dateStartIndex'] = $this->eachTour['dateStart'];
        }

        if ($node['name'] == 'DURATION') {
            $this->eachTour['nightCount'] = $node['node']['body'];
            $this->eachTour['dateEnd'] = date("Y-m-d",strtotime($this->eachTour['dateStart'])+60*60*24*$this->eachTour["nightCount"]);
        }

        if ($node['name'] == 'PRICE') {
            $this->eachTour['price'] = $node['node']['body'];
        }

        if ($node['name'] == 'CURRENCY') {
            $this->eachTour['currency'] = $node['node']['body'];
        }

        if ($node['name'] == 'PRICE_ID') {
            $this->eachTour['tour'] = $node['node']['body'];
        }

        if ($node['name'] == 'FLIGHT_FROM' || $node['name'] == 'FLIGHT_FROM_ID') {
            if ($node['name'] == 'FLIGHT_FROM' && $node['node']['body'] != 'NO') {
                $this->stackUkr['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'FLIGHT_FROM_ID' && $node['node']['body'] != 0) {
                $this->stackFlightFromUkr['id'] = $node['node']['body'];
                if (!isset($this->city[$node['node']['body']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_region', array($this->stackFlightFromUkr), true);
                    $this->initializeArray('initRegion');
                }
                $this->stackFlightFromUkr = array();
                $this->eachTour['cityDepatured'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'FLIGHT_TO' || $node['name'] == 'FLIGHT_TO_ID') {
            if ($node['name'] == 'FLIGHT_TO' && $node['node']['body'] != 'NO') {
                $this->stackFlightTo['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'FLIGHT_TO_ID' && $node['node']['body'] != 0) {
                $this->stackFlightTo['id'] = $node['node']['body'];
                $this->eachTour['arrivalRegion'] = $node['node']['body'];
            }
            unset($node['node']['body']);
        }

        if ($node['name'] == 'COUNTRY_TO' || $node['name'] == 'COUNTRY_TO_ID') {
            if ($node['name'] == 'COUNTRY_TO') {
                $this->stackCountryTo['name'] = $node['node']['body'];
            } elseif ($node['name'] == 'COUNTRY_TO_ID') {
                $this->stackCityTo['country_id'] = $node['node']['body'];
                $this->stackCity['country_id'] = $node['node']['body'];
                $this->stackCountryTo['id'] = $node['node']['body'];
                $this->stackHotel['country'] = $node['node']['body'];

                $this->eachTour['country'] = $node['node']['body'];
                if (!isset($this->country[$node['node']['body']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_country', array($this->stackCountryTo), true);
                    $this->initializeArray('initCountry');
                }
                if (!isset($this->country[$node['node']['body']])) {
                    $this->errorTour = true;
                }
                $this->stackCountryTo = array();

                if (!isset($this->city[$this->stackCityTo['id']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_region', array($this->stackCityTo), true);
                    $this->initializeArray('initRegion');
                }
                $this->stackCityTo = array();

                if (!empty($this->stackFlightTo)) {
                    if (!isset($this->city[$this->stackFlightTo['id']])) {
//                        $this->tblTour->insertOrUpdate('xml_panukraine_region', array($this->stackFlightTo), true);
                        $this->initializeArray('initRegion');
                    }
                }
                $this->stackFlightTo = array();

                if (!isset($this->city[$this->stackCity['id']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_region', array($this->stackCity), true);
                    $this->initializeArray('initRegion');
                }
                if (!isset($this->city[$this->stackCity['id']])) {
                    $this->errorTour = true;
                }
                $this->stackCity = array();

                if (!isset($this->hotel[$this->stackHotel['id']])) {
//                    $this->tblTour->insertOrUpdate('xml_panukraine_hotel', array($this->stackHotel), true);
                    $this->initializeArray('initHotel');
                }
                if (!isset($this->hotel[$this->stackHotel['id']])) {
                    $this->errorTour = true;
                }
                $this->stackHotel = array();

                if (strtotime($this->eachTour['dateStart']) > strtotime(date("Y-m-d")) && $this->errorTour === false) {
                    $this->eachTour['update'] = 1;
                    $this->eachTour['tourOperator'] = XML_PANUKRAINE;
                    $this->eachFiftiesTour[] = $this->eachTour;

                    if (count($this->eachFiftiesTour) == 50) {
                        $this->actualTour += 50;
//                        $this->tblTour->insertOrUpdate('xml_tour',$this->eachFiftiesTour, true);
                        $this->eachFiftiesTour = array();
                    }
                    $this->countTour++;
                }
            }
        }

        // Запись оставшихся туров
        if ($node['name'] == 'eof') {
            if (!empty($this->eachFiftiesTour)) {
                $this->actualTour += count($this->eachFiftiesTour);
//                $this->tblTour->insertOrUpdate('xml_tour',$this->eachFiftiesTour, true);
            }
        }

        // Файл не докачан
        if (isset($node['error'])) {
//      $this->insertLog('error', CRON_PARSE, XML_PANUKRAINE, 13, $this->currentSpoId);
            #$this->insertLog('xml_idrisko_error', 'not load', null ,null, 'xml_log', $this->currentSpoId , null, null);
            $this->notLoad = true;
        }
    }
}
