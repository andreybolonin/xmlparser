<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\Finder\Finder;

class JoinupParser extends Xmlpullreader
{

    private $ftp_server = "ftp.joinup.ua";
    private $ftp_user = 'turobzor';
    private $ftp_password = 'ZyTaWn77in32mNWo';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их в таблицу list
     * Warning: is_dir(): php_network_getaddresses: getaddrinfo failed: System error
     */
    public function getAndSaveSpoList($operator_id)
    {
        $finder = new Finder();
        $files = $finder->files()
            ->name('*.xml')
            ->name('*.zip')
            ->name('*.rar')
            ->name('*.gzip')
            ->ignoreUnreadableDirs()
            ->in('ftp://' . $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . '/');

        $stmt = $this->pdo_backend->prepare("INSERT INTO xml_tour.spo (operator, file_name, status, url) VALUES (:operator, :file_name, :status, :url)");
        $this->pdo_backend->beginTransaction();

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getPathname() . '"')->fetch();
            if (!$spo) {
                $stmt->bindValue(':file_name', $file->getFilename());
                $stmt->bindValue(':operator', $operator_id);
                $stmt->bindValue(':url', $file->getPathname());
                $stmt->bindValue(':status', 'nottouched');
                $stmt->execute();
            }
        }

        $this->pdo_backend->commit();
    }

    /**
     * @param  array      $spo
     * @return mixed|void
     */
    public function params($spo = array())
    {
        $this->initializeArray();
        $this->intializeParams($spo);

        if ($this->readXmlByUrl($spo['path']) === false) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::ERROR_XML, $spo['url']);
            $spo['status'] = 'error';
            Spo::insertOrUpdateList($spo);

            return false;
        }

        $this->setReadBanch(array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE'));
        $this->setReadBanchOnlyAttr(array('OFFER' => true, 'HOTEL' => true, 'TRANSPORT' => true, 'PRICE' => true));
        $this->setFunctionHandler(array($this, 'getAllTourFromSpo'));
        $this->getXmlAssoc();

        return $this->postXmlParsing($spo, $this, false);
    }

    /**
     * Инициализация справочных данных
     * Для обновления или загрузки одного справочника $allCatalog = FALSE , $curentCatalog =  название каталога
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('JOINUP');
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
////            $aCountry = Spo::getIsoCountry(array('id', 'shortName', 'shortName2'));
//            $aCountry = IsoCountry::getIsoCountries();
//            foreach ($aCountry as $val) {
//                $this->country[$val['shortName']] = array(
//                    'id' => $val['id'],
//                    'name' => 'countryName'
//                );
//                $this->country[$val['shortName2']] = $val['id'];
//            }
//
//            $aCountrySelf =  $aCountrySelf = Country::findAllByOperator($operator_id);
//            foreach ($aCountrySelf as $val) {
//                $this->countrySelf[$val['code']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = Hotel::findAllByOperator($operator_id);
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = Star::findAllByOperator($operator_id);
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = City::findAllByOperator($operator_id);
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog == 'all' || $curentCatalog == 'initRoom') {
//            if ($allCatalog === true || $curentCatalog == 'initRoom') {
//                $aRoom = Room::findAllByOperator($operator_id);
//                foreach ($aRoom as $val) {
//                    $this->roomIds[$val['id']] = '';
//                }
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = StayType::findAllByOperator($operator_id);
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//
////        $this->initHotel($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initHotelStar($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initCity($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initRegion($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initRoom($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initStayType($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initPansion($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initTourType($allCatalog, $curentCatalog, $this, self::$table);
//    }

    /**
     *  Отбор туров
     *
     * @param $node
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('JOINUP');

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            $this->spoName = $node['node']['attributes']['SPO'];

            // В теге одна страна
            if (strpos($node['node']['attributes']['COUNTRY'], ',') === false) {
                if (!isset($this->countrySelf[$node['node']['attributes']['COUNTRY']])) {
                    // не пустая проверяем есть ли в ощем каталоге
                    if (strlen($node['node']['attributes']['COUNTRY']) > 0) {
                        if (!isset($this->country[$node['node']['attributes']['COUNTRY']])) {
                            Country::addCountry(
                                array(
                                    'code' => trim($node['node']['attributes']['COUNTRY']),
                                    'name' => trim($node['node']['attributes']['COUNTRY'])
                                ),
                                $operator_id
                            );
                        } else {
                            if (isset($this->country[$node['node']['attributes']['COUNTRY']]['name'])) {
                                Country::addCountry(
                                    array(
                                        'code' => trim($node['node']['attributes']['COUNTRY']),
                                        'name' => $this->country[$node['node']['attributes']['COUNTRY']]['name']
                                    ),
                                    $operator_id
                                );
                            }
                        }
                        $this->initializeArray(false, 'initCountry');
                    } else {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::NO_SUCH_COUNTRY, $this->currentSpoId);
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$node['node']['attributes']['COUNTRY']]) ? $this->countrySelf[$node['node']['attributes']['COUNTRY']] : null;
            } else {
                $countryArray = explode(',',$node['node']['attributes']['COUNTRY']);
                if (!isset($this->countrySelf[$countryArray[1]])) {
                    if (!isset($this->country[$node['node']['attributes']['COUNTRY']])) {
                        Country::addCountry(
                            array(
                                'code' => trim($countryArray[1]),'name' => trim($countryArray[1]),
                                'url' => ''
                            ),
                            $operator_id
                        );
                    } else {
                        Country::addCountry(
                            array(
                                'code' => trim($countryArray[1]),
                                'name' => $this->country[$countryArray[1]]['name']
                            ),
                            $operator_id
                        );
                    }
                    $this->initializeArray(false, 'initCountry');
                }
                $this->eachTour['country'] = isset($this->countrySelf[$countryArray[1]]) ? $this->countrySelf[$countryArray[1]] : null;
            }

            if (!isset($this->tourtype[$node['node']['attributes']['TOURTYPE']])) {
                TourType::addTourType(
                    array('name' => trim($node['node']['attributes']['TOURTYPE'])),
                    $operator_id
                );
                $this->initializeArray(true, 'initTourType');
            }
            $this->tour_type = $this->tourtype[$node['node']['attributes']['TOURTYPE']] ? $this->tourtype[$node['node']['attributes']['TOURTYPE']] : 0;
            $this->eachTour['adult'] = $node['node']['attributes']['ADL'];
            $this->eachTour['children'] = $node['node']['attributes']['CHD'];
            $this->eachTour['infant'] = $node['node']['attributes']['INF'];
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    Star::addStar(
                        array('star' => $hotelStar),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node['node']['attributes']['CITY']]) && $this->errorTour === false) {
                if ( strlen($node['node']['attributes']['CITY']) > 0 ) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITY']),
                            'country_id' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::EMPTY_CITY, $this->currentSpoId);
                }
            }
            $this->eachTour['city'] = $this->region[$node['node']['attributes']['CITY']];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {
                    Hotel::addHotel(
                        array(
                            'id'      => $node['node']['attributes']['HTC'],
                            'name'    => $node['node']['attributes']['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[$node['node']['attributes']['CITY']],
                            'country' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::EMPTY_HOTEL, $this->currentSpoId);
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC'] > 0)) {
                    Room::addRoom(
                        array(
                            'id' => trim($node['node']['attributes']['RMC']),
                            'name' => trim($node['node']['attributes']['ROOM'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::EMPTY_ROOM, $this->currentSpoId);
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере

            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC'] > 0)) {
                    StayType::addStayType(
                        array(
                            'id' => trim($node['node']['attributes']['PLC']),
                            'name' => trim($node['node']['attributes']['PLACE'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::EMPTY_STAY_TYPE, $this->currentSpoId);
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    Pansion::addPansion(
                        array(
                            'id' => trim($node['node']['attributes']['MLC']),
                            'name' => trim($node['node']['attributes']['MEAL'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::EMPTY_PANSION, $this->currentSpoId);
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node['node']['attributes']['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node['node']['attributes']['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYTO']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node['node']['attributes']['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // Сортировка по дате и актуальности туров
            if (strtotime($node['node']['attributes']['DATE']) > time() &&
                strtotime($node['node']['attributes']['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node['node']['attributes']['DATE'];
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('JOINUP');
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек по 50 туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('JOINUP'), Log::EMPTY_HOTEL, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

}
