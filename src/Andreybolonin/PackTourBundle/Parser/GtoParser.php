<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;

class GtoParser extends Xmlpullreader
{

    protected $url = 'http://gate.gto.com.ua/gate.php/pricelist';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $browser->getClient()->setTimeout(1500);
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $dom = new \DOMDocument();
        $dom->loadHTML($html);
        $files = $dom->getElementsByTagName('a');

        foreach ($files as $file) {
            if (!Xmlpullreader::checkExtension($file->getAttribute('href'))) {
                continue;
            }

            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $file->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     *  Отбор туров
     *
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = $this->spo['operator'];
        $node_attributes = $node['node']['attributes'];

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            $this->spoName = $node_attributes['SPO'];

            // В теге одна страна
            if (!isset($this->country[$node_attributes['COUNTRY']])) {
                if ( strlen($node_attributes['COUNTRY']) > 0 ) {
                    Country::addCountry(array(
                            'code' => trim($node_attributes['COUNTRY']),
                            'name' => trim($node_attributes['COUNTRY']),
//                        'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initCountry');
                } else {
                    $this->errorCountry();
                }
            }
            $this->eachTour['country'] = $this->country[$node_attributes['COUNTRY']];

            if ($node_attributes['TOURTYPE']) {
                if (!isset($this->tourtype[$node_attributes['TOURTYPE']])) {
                    TourType::addTourType(array(
                            'name' => trim($node_attributes['TOURTYPE']),
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initTourType');
                }
                $this->tour_type = $this->tourtype[$node_attributes['TOURTYPE']] ? $this->tourtype[$node_attributes['TOURTYPE']] : 0;
            }

            $this->eachTour['adult'] = $node_attributes['ADL'];
            $this->eachTour['children'] = $node_attributes['CHD'];
            $this->eachTour['infant'] = $node_attributes['INF'];
            $this->eachTour['currency'] = $node_attributes['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node_attributes['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node_attributes['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    Star::addStar(array(
                            'star' => $hotelStar,
//                        'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->city[$node_attributes['CITY']]) && $this->errorTour === false) {
                if ( strlen($node_attributes['CITY']) > 0 ) {
                    City::addCity(array(
                            'name' => trim($node_attributes['CITY']),
                            'country_id' => $this->eachTour['country'],
//                        'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initRegion');
                } else {
                    $this->errorCity();
                }
            }
            $this->eachTour['city'] = $this->city[$node_attributes['CITY']];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node_attributes['HTC']]) && $this->errorTour === false) {
                if (strlen($node_attributes['HTC'] > 0)) {
                    Hotel::addHotel(array(
                            'id'      => $node_attributes['HTC'],
                            'name'    => $node_attributes['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->city[$node_attributes['CITY']],
                            'country' => $this->eachTour['country'],
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initHotel');
                } else {
                    $this->errorHotel();
                }
            }
            $this->eachTour['hotel'] = $node_attributes['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node_attributes['RMC']])) {
                if (strlen($node_attributes['RMC'] > 0)) {
                    Room::addRoom(array(
                            'id' => trim($node_attributes['RMC']),
                            'name' => trim($node_attributes['ROOM']),
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initRoom');
                } else {
                    $this->errorRoom();
                }
            }
            $this->eachTour['room'] = $node_attributes['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node_attributes['PLC']])) {
                if (strlen($node_attributes['PLC'] > 0)) {
                    StayType::addStayType(array(
                            'id' => trim($node_attributes['PLC']),
                            'name' => trim($node_attributes['PLACE']),
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initStayType');
                } else {
                    $this->errorStayType();
                }
            }
            $this->eachTour['stayType'] = $node_attributes['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node_attributes['MLC']])) {
                if (strlen($node_attributes['MLC']) > 0 && strlen($node_attributes['MEAL']) > 0) {
                    Pansion::addPansion(array(
                            'id' => trim($node_attributes['MLC']),
                            'name' => trim($node_attributes['MEAL']),
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initPansion');
                } else {
                    $this->errorPansion();
                }
            }
            $this->eachTour['pansion'] = $node_attributes['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node_attributes['NAME'] == 'FLIGHT') {
                if (isset($this->city[$node_attributes['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->city[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) > 0) {
                    City::addCity(array(
                            'name' => trim($node_attributes['CITYFR']),
                            'country_id' => null,
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initRegion');
                    $this->eachTour['cityDepatured'] = $this->city[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->city[$node_attributes['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->city[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['CITYTO']) > 0) {
                    City::addCity(array(
                            'name' => trim($node_attributes['CITYTO']),
                            'country_id' => null,
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->city[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node_attributes['NAME'] == 'BACK FLIGHT') {
                if (isset($this->city[$node_attributes['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->city[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) > 0) {
                    City::addCity(array(
                            'name' => trim($node_attributes['CITYFR']),
                            'country_id' => null,
//                            'operator' => $operator_id
                        ),
                        $operator_id
                    );
                    $this->initializeArray('initRegion');
//                    $this->eachTour['departuredRegion'] = $this->city[$node_attributes['CITYFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // Сортировка по дате и актуальности туров
            if (strtotime($node_attributes['DATE']) > time() &&
//                strtotime($node_attributes['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = substr($node_attributes['DATE'], 0 , 4) . '-' . substr($node_attributes['DATE'], -4 , 2) . '-' . substr($node_attributes['DATE'], -2 , 2);
                $this->eachTour['price'] = $node_attributes['VAL'];
                $this->eachTour['nightCount'] = $node_attributes['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart'])+86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = $operator_id;
                $this->eachFiftiesTour[] = $this->eachTour;

                $this->saveTours($operator_id);
            }
            $this->countTour++;
        }

        $this->saveLastTours($node, $operator_id);
    }

}
