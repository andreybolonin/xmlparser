<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;
use Symfony\Component\DomCrawler\Crawler;

class TridenttravelParser extends Xmlpullreader
{

    private $url_1 = 'http://trident.travel/data/xmlspo/';
    private $url_2 = 'http://trident.travel/data/xmlspo/TIND/';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'PRICE');

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $html_1 = $browser->get($this->url_1)->getContent();
            $html_2 = $browser->get($this->url_2)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $dom_1 = new Crawler($html_1, $this->url_1);
        $dom_2 = new Crawler($html_2, $this->url_2);

        $this->arrayLoop($dom_1, $operator_id, $this->url_1);
        $this->arrayLoop($dom_2, $operator_id, $this->url_2);
    }

    /**
     * @param $dom
     * @param $operator_id
     * @param $url
     * @return array
     */
    public function arrayLoop($dom, $operator_id, $url)
    {
        $files = $dom->filter('a');

        foreach ($files as $file) {
            if (!Xmlpullreader::checkExtension($file->getAttribute('href'))) {
                continue;
            }
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $url . $file->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $url . $file->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * Инициализация справочных данных
     * Для обновления или загрузки одного справочника $allCatalog = FALSE , $curentCatalog =  название каталога
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('TRIDENTTRAVEL');
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
//            $this->country = Country::initCountryShortNameId($operator_id);
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = Hotel::findAllByOperator($operator_id);
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = Star::findAllByOperator($operator_id);
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = City::findAllByOperator($operator_id);
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog == 'all' || $curentCatalog == 'initRoom') {
//            if ($allCatalog === true || $curentCatalog == 'initRoom') {
//                $aRoom = Room::findAllByOperator($operator_id);
//                foreach ($aRoom as $val) {
//                    $this->roomIds[$val['id']] = '';
//                }
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = StayType::findAllByOperator($operator_id);
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//    }

    /**
     *  Отбор туров
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('TRIDENTTRAVEL');

        if (isset($node['name']) && $node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            $this->spoName = $node['node']['attributes']['SPO'];
            $this->currentSpo = $node['node']['attributes']['SPO'];

            // В теге одна страна
            if (isset($node['node']['attributes']['COUNTRY']) && !isset($this->country[$node['node']['attributes']['COUNTRY']])) {
                if (isset($node['node']['attributes']['COUNTRY']) && strlen($node['node']['attributes']['COUNTRY']) > 0) {
                    Country::addCountry(
                        array(
                            'name' => trim($node['node']['attributes']['COUNTRY'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initCountry');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::EMPTY_COUNTRY_TAG, $this->currentSpoId);
                }
            }
            if (isset($node['node']['attributes']['COUNTRY'])) {
                $this->eachTour['country'] = $this->country[$node['node']['attributes']['COUNTRY']];
            }

            $region = trim($node['node']['attributes']['CityFR']);
            if (isset($this->region[$region])) {
                $this->eachTour['cityDepatured'] = $this->region[$region];
            } elseif (strlen($node['node']['attributes']['CityFR']) > 0) {
                City::addCity(
                    array('name' => trim($region), 'country_id' => null),
                    $operator_id
                );
                $this->initializeArray(false, 'initRegion');
                $this->eachTour['cityDepatured'] = $this->region[$region];
            }
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if (isset($node['name']) && $node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    Star::addStar(array('star' => $hotelStar), $operator_id);
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node['node']['attributes']['RESORT']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['RESORT']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['RESORT']),
                            'country_id' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::EMPTY_CITY, $this->currentSpoId);
                }
            }
            $this->eachTour['city'] = $this->region[$node['node']['attributes']['RESORT']];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {
                    Hotel::addHotel(
                        array(
                            'id'      => $node['node']['attributes']['HTC'],
                            'name'    => $node['node']['attributes']['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[$node['node']['attributes']['RESORT']],
                            'country' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::EMPTY_HOTEL, $this->currentSpoId);
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC'] > 0)) {
                    Room::addRoom(
                        array(
                            'id' => trim($node['node']['attributes']['RMC']),
                            'name' => trim($node['node']['attributes']['ROOM'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::EMPTY_ROOM, $this->currentSpoId);
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC'] > 0)) {
                    StayType::addStayType(
                        array(
                            'id' => trim($node['node']['attributes']['PLC']),
                            'name' => trim($node['node']['attributes']['PLACE'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::EMPTY_STAY_TYPE, $this->currentSpoId);
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            $people = explode('+', $node['node']['attributes']['PLACE']);
            $this->eachTour['children'] = 0;
            foreach ($people as $peopleOne) {
                //$peopleOne = 0;
                if (strpos($peopleOne, 'AD') !== false) {
                    $this->eachTour['adult'] = substr($peopleOne, 0 , 1);
                }
                if (strpos($peopleOne, 'CHLD') !== false) {
                    $this->eachTour['children'] += substr($peopleOne, 0 , 1);
                }
            }
            unset($people);

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    Pansion::addPansion(array(
                            'id' => trim($node['node']['attributes']['MLC']),
                            'name' => trim($node['node']['attributes']['MEAL'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::EMPTY_PANSION, $this->currentSpoId);
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if ($node['name'] == 'PRICE') {
            // Cортировка по дате и актуальности туров
            if (strtotime($node['node']['attributes']['DATE']) > time() &&
                strtotime($node['node']['attributes']['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node['node']['attributes']['DATE'];//substr($node['node']['attributes']['DATE'], 0 , 4) . '-' . substr($node['node']['attributes']['DATE'], -4 , 2) . '-' . substr($node['node']['attributes']['DATE'], -2 , 2);
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 0;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('TRIDENTTRAVEL');
                $this->eachFiftiesTour[] = $this->eachTour;

                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TRIDENTTRAVEL'), Log::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

}
