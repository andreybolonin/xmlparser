<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\Db;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;
use Symfony\Component\DomCrawler\Crawler;

class TurtessParser extends Xmlpullreader
{

    private $url = 'http://www2.turtess-online.com.ua/export/prices/';
    public $xml_read_keys = array('element', 'spo');

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $browser->getClient()->setTimeout(1500);
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $crawler = new Crawler($html, $this->url);
        $files = $crawler->filter('spo');

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getAttribute('url') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $file->getAttribute('url'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('TURTESS');

        if ($node['name'] == 'element') {
            $this->eachTour['spo'] = $this->spoId;
            $this->eachTour['price'] = $node['node']['attributes']['P'];
            $this->eachTour['dateStart'] = date("Y-m-d", strtotime($node['node']['attributes']['D']));

            // Город вылета
            if (!isset($this->city[$node['node']['attributes']['aFCK']]) && !isset($this->dublicateId[$node['node']['attributes']['aFCK']])) {
                $this->getCatalog('fromcities');
            }
            if (!isset($this->city[$node['node']['attributes']['aFCK']]) && !isset($this->dublicateId[$node['node']['attributes']['aFCK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['aFCK']] = 'fromcities';
            }
            $this->eachTour['cityDepatured'] = isset($this->city[$node['node']['attributes']['aFCK']]) ? $node['node']['attributes']['aFCK'] : null ;

            // Город прилета
            if (!isset($this->city[$node['node']['attributes']['aTCK']]) && !isset($this->dublicateId[$node['node']['attributes']['aTCK']])) {
                $this->getCatalog('tocities');
            }
            if (!isset($this->city[$node['node']['attributes']['aTCK']]) && !isset($this->dublicateId[$node['node']['attributes']['aTCK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['aTCK']] = 'tocities';
            }
//            $this->eachTour['arrivalRegion'] = isset($this->city[$node['node']['attributes']['aTCK']]) ? $node['node']['attributes']['aTCK'] : $this->errorTour = true ;
//            $this->eachTour['departuredRegion'] = isset($this->city[$node['node']['attributes']['aTCK']]) ? $node['node']['attributes']['aTCK'] : null;

            // Тип питания
            if (!isset($this->pansion[$node['node']['attributes']['htlBK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlBK']])) {
                $this->getCatalog('pansion');
            }
            if (!isset($this->pansion[$node['node']['attributes']['htlBK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlBK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['htlBK']] = 'pansion';
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::EMPTY_PANSION, $this->currentSpoId);
            }
            $this->eachTour['pansion'] = isset($this->pansion[$node['node']['attributes']['htlBK']]) ? $node['node']['attributes']['htlBK'] : $this->errorTour = true ;

            // Отель
            if (!isset($this->hotel[$node['node']['attributes']['htlK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlK']])) {
                $this->getCatalog('hotel');
            }
            if (!isset($this->hotel[$node['node']['attributes']['htlK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['htlK']] = 'hotel';
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::EMPTY_HOTEL, $this->currentSpoId);
            }
            $this->eachTour['hotel'] = isset($this->hotel[$node['node']['attributes']['htlK']]) ? $node['node']['attributes']['htlK'] : $this->errorTour = true ;

            // Страна
            if (!isset($this->country[$node['node']['attributes']['htlCoK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlCoK']])) {
                $this->getCatalog('countries');
            }
            if (!isset($this->country[$node['node']['attributes']['htlCoK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlCoK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['htlCoK']] = 'countries';
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::NO_SUCH_COUNTRY, 'СПО: ' . $this->currentSpoId . ' ID' . $node['node']['attributes']['htlCoK']);
            }
            $this->eachTour['country'] = isset($this->country[$node['node']['attributes']['htlCoK']]) ? $node['node']['attributes']['htlCoK'] : $this->errorTour = true ;

            // Регион проживания
            if (!isset($this->region[$node['node']['attributes']['htlRK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlRK']])) {
                $this->getCatalog('region');
            }
            if (!isset($this->region[$node['node']['attributes']['htlRK']]) && !isset($this->dublicateId[$node['node']['attributes']['htlRK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['htlRK']] = 'region';
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::NO_REGION, 'СПО: ' . $this->currentSpoId . ' ID' . $node['node']['attributes']['htlRK']);
            }
            $this->eachTour['city'] = isset($this->region[$node['node']['attributes']['htlRK']]) ? $node['node']['attributes']['htlRK'] : $this->errorTour = true ;

            // Звездность отеля
            $star = str_replace('*', '', $node['node']['attributes']['htlCN']);
            $starToInsert = $star == '' ? 1 : $this->star[$star];

            if (!isset($this->star[$star]) && $star != '') {
                $this->noIdInCatalog = true;
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::NO_STAR, 'СПO: ' . $this->currentSpoId . ' ID' . $star);
            }
            $this->eachTour['star'] = isset($this->star[$star]) || $star == '' ? $starToInsert : $this->errorTour = true ;

            // Количество ночей
            $this->eachTour['nightCount'] = $node['node']['attributes']['htlN'];
            $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);

            // Тип номера
            if (!isset($this->roomType[$node['node']['attributes']['roomTK']]) && !isset($this->dublicateId[$node['node']['attributes']['roomTK']])) {
                $this->getCatalog('room_types');
            }
            if (!isset($this->roomType[$node['node']['attributes']['roomTK']]) && !isset($this->dublicateId[$node['node']['attributes']['roomTK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['roomTK']] = 'room_types';
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::EMPTY_STAY_TYPE, 'СПО: ' . $this->currentSpoId . ' ID' . $node['node']['attributes']['roomTK']);
            }
            $this->eachTour['stayType'] = isset($this->roomType[$node['node']['attributes']['roomTK']]) ? $node['node']['attributes']['roomTK'] : $this->errorTour = true ;

            // Количество людей в отеле
            $this->eachTour['adult'] = $node['node']['attributes']['roomNA'];
            $this->eachTour['children'] = $node['node']['attributes']['roomNC'];
            $this->eachTour['infant'] = 0;

            // Категория номера
            if (!isset($this->roomCat[$node['node']['attributes']['roomCK']]) && !isset($this->dublicateId[$node['node']['attributes']['roomCK']])) {
                $this->getCatalog('room');
            }
            if (!isset($this->roomCat[$node['node']['attributes']['roomCK']]) && !isset($this->dublicateId[$node['node']['attributes']['roomCK']])) {
                $this->noIdInCatalog = true;
                $this->dublicateId[$node['node']['attributes']['roomCK']] = 'room';
                Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::NO_ROOM, 'СПО: ' . $this->currentSpoId . ' ID' . $node['node']['attributes']['roomCK']);
            }

            $this->eachTour['room'] = isset($this->roomCat[$node['node']['attributes']['roomCK']]) ? $node['node']['attributes']['roomCK'] : $this->errorTour = true ;
            $this->eachTour['tour'] = $node['node']['attributes']['key'];
            $this->eachTour['currency'] = $node['node']['attributes']['curr'];
            $this->eachTour['update'] = 1;
            $this->eachTour['tourOperator'] = Operator::getOperatorId('TURTESS');
        }

        // Подсчет всех туров
        if (!empty($this->eachTour)) {
            $this->countTour++;

            // Сортировка по дате
            if ($this->eachTour['dateStart'] < date("Y-m-d") ||
                $this->eachTour['dateStart'] > date("Y-m-d", Registry::get('parser_start_date'))
            ) {
                $this->errorTour = true;
            }

            // Cтек только с полным набором данных о туре
            if ($this->errorTour ===  false) {
                $this->eachFiftiesTour[] = $this->eachTour;
                $this->eachTour = array();
            }
            // Обновляем статус ошибки
            $this->errorTour = false;

            // Стек туров
            $this->saveTours($operator_id);
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл недокачан
        if (isset($node['error'])) {
            $this->notLoad = true;
        }
    }

    /**
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('TURTESS');
//
//        if ($allCatalog === true || $curentCatalog == 'countries') {
//            $country = Country::findAllByOperator($operator_id);
//            foreach ($country as $key => $val) {
//                $this->country[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'hotel') {
//            $hotel = Hotel::findAllByOperator($operator_id);
//            foreach ($hotel as $key => $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'pansion') {
//            $pansion = Pansion::findAllByOperator($operator_id);
//            foreach ($pansion as $key => $val) {
//                $this->pansion[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'region') {
//            $region = City::findAllByOperator($operator_id);
//            foreach ($region as $key => $val) {
//                $this->region[$val['region']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'fromcities') {
//            $fromcity = City::findAllByOperator($operator_id);
//            foreach ($fromcity as $key => $val) {
//                $this->city[$val['city']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'tocities') {
//            $tocity = City::findAllByOperator($operator_id);
//            foreach ($tocity as $key => $val) {
//                $this->city[$val['city']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'room_accomodations') {
//            $accomodation = RoomAccomodations::findAllByOperator($operator_id);
//            foreach ($accomodation as $key => $val) {
//                $this->accomodation[$val['id']] = $val['nameFull'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'room') {
//            $roomCat = Room::findAllByOperator($operator_id);
//            foreach ($roomCat as $key => $val) {
//                $this->roomCat[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'room_types') {
//            $roomType = StayType::findAllByOperator($operator_id);
//            foreach ($roomType as $key => $val) {
//                $this->roomType[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'star') {
//            $star = Star::findAllByOperator($operator_id);
//            foreach ($star as $key => $val) {
//                $this->star[$val['star']] = $val['id'];
//            };
//        }
//    }

    /**
     * Обновление справочника
     * Очищает и загружает справочник
     *
     * @param null $type
     */
    private function getCatalog($type = null)
    {
        $this->eachElement = array();
        $this->catalogArray = array();

        switch ($type) {
            case 'countries':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/countries/';
                $this->listType = 'countries';
                break;
            case 'fromcities':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/cities/from/';
                $this->listType = 'fromcities';
                break;
            case 'tocities':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/cities/to/';
                $this->listType = 'tocities';
                break;
            case 'region':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/resorts/';
                $this->listType = 'region';
                break;
            case 'hotel':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/hotels/';
                $this->listType = 'hotel';
                break;
            case 'pansion':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/boardings/';
                $this->listType = 'pansion';
                break;
            case 'room_types':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/room_types/';
                $this->listType = 'room_types';
                break;
            case 'room':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/room_categories/';
                $this->listType = 'room';
                break;
            case 'room_accomodations':
                $this->fileType = 'http://www2.turtess-online.com.ua/export/dictionary/room_accomodations/';
                $this->listType = 'room_accomodations';
                break;
        }

        $tmpList = new TurtessParserList();
        $tmpList->listType = $this->listType;
        if ($tmpList->readXmlByUrl($this->fileType) === false) {
            Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::ERROR_XML, $this->listType);

            return;
        }

        $tmpList->setReadBanch(array('element'));
        $tmpList->setFunctionHandler(array($tmpList, 'parseList'));
        $tmpList->setReadBanchOnlyAttr(array('element' => true));
        $tmpList->getXmlAssoc();

        if ($this->listType == 'hotel') {

            $allStars = db::rows('SELECT star FROM ' . self::$base . 'hotel GROUP BY star');
            if (!empty($allStars))
                db::insertOrUpdateMulty($allStars, self::$base . 'star');
        }

        $this->initializeArray(false, $this->listType);
        unset($this->eachElement);
        unset($this->catalogArray);
        unset($this->table);
        unset($this->fileType);
        unset($this->listType);
        unset($tmpList);
    }
}

class TurtessParserList extends TurtessParser
{
    public $listType = null;
    private static $base = 'turtess.';

    /**
     * Запись в базу справочника
     *
     * @param $node
     * @return void
     */
    public function parseList($node)
    {
        if (isset($this->listType) && $node['name'] != 'eof') {
            switch ($this->listType) {
                case 'countries':
                    $this->table = 'country';
                    $this->eachElement['id'] = $node['node']['attributes']['countryKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['countryName'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'fromcities':
                    $this->table = 'city';
                    $this->eachElement['city'] = $node['node']['attributes']['cityKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['cityName'];
                    $this->eachElement['country'] = $node['node']['attributes']['cityCountryKey'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'tocities':
                    $this->table = 'city';
                    $this->eachElement['city'] = $node['node']['attributes']['cityKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['cityName'];
                    $this->eachElement['country'] = $node['node']['attributes']['cityCountryKey'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'region':
                    $this->table = 'city';
                    $this->eachElement['region'] = $node['node']['attributes']['resortKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['resortName'];
                    $this->eachElement['country'] = $node['node']['attributes']['resortCountryKey'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'hotel':
                    $this->table = 'hotel';
                    $this->eachElement['id'] = $node['node']['attributes']['hotelKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['hotelName'];
                    $this->eachElement['country'] = $node['node']['attributes']['hotelCountryKey'];
                    $this->eachElement['city'] = $node['node']['attributes']['hotelCityKey'];
                    $this->eachElement['region'] = $node['node']['attributes']['hotelresortKey'];
                    $this->eachElement['desctiption'] = $node['node']['attributes']['hotel_description_id'];
                    $this->eachElement['star'] = str_replace('*', '', $node['node']['attributes']['hotelCategory']);
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'pansion':
                    $this->table = 'pansion';
                    $this->eachElement['id'] = $node['node']['attributes']['boardingKey'];
                    $this->eachElement['code'] = $node['node']['attributes']['boardingCode'];
                    $this->eachElement['name'] = $node['node']['attributes']['boardingName'];
                    $this->eachElement['group'] = $node['node']['attributes']['boarding_group'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'room_types':
                    $this->table = 'room_types';
                    $this->eachElement['id'] = $node['node']['attributes']['roomTypeKey'];
                    $this->eachElement['code'] = $node['node']['attributes']['roomType_code'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'room':
                    $this->table = 'room';
                    $this->eachElement['id'] = $node['node']['attributes']['roomCategoryKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['roomCategoryName'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
                case 'room_accomodations':
                    $this->table = 'room_accomodations';
                    $this->eachElement['id'] = $node['node']['attributes']['roomAccomodationKey'];
                    $this->eachElement['name'] = $node['node']['attributes']['roomAccomodationName'];
                    $this->eachElement['nameFull'] = $node['node']['attributes']['roomAccomodationNameFull'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
            }
        }
        db::insertOrUpdate($this->eachElement, self::$base . $this->table);

        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('TURTESS'), Log::ERROR_XML, $this->listType);
        }
    }
}
