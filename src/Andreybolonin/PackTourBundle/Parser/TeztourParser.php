<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\XmlPullReader;

class TeztourParser extends Xmlpullreader
{

    private $url = 'http://book.tez-tour.com/xmlgate/spo/spo_country_list.xml';
    public $xml_read_keys = array('id', 'hotelRoom', 'arrivalRegionId', 'departureRegionId', 'date', 'nightCount', 'hotelPansion', 'price', 'message');
    //$this->setReadBanchOnlyAttr(array('hotelRoom' => true, 'date' => true , 'nightCount' => true, 'hotelPansion' => true));

    /**
     * Получает список SPO и сохраняет их в таблицу list
     */
    public function getAndSaveSpoList($operator_id)
    {
        $this->aSpoList = array();
        $this->errorExit = false;
        $lodedSpoList = null;
        $this->tblObjectForOerator = null;
        $lodedSpoList = $this->getSpoListForOperator($this->url, XML_TEZTOUR);

        if ($this->readXmlByUrl($lodedSpoList) === false) {
            $this->errorExit = true;
        }

        $this->setReadBanch(array('S'));
        $this->setFunctionHandler(array($this, 'getAllTeztourSpo'));
        $this->getXmlAssoc();

        if ($this->errorExit === false) {
            // необходимые данные про оператора
            $this->tblObjectForOerator = new tbl_teztour();

            $spoDateList = $this->tblObjectForOerator->getAllSpoList();
            // Если список спо по оператору не пуст
            if (!empty($spoDateList)) {
                $arrayToUpdate = $this->updateSpoArrayTezTour($spoDateList);

                // Удаление со списка, туров, файлов
                if (isset($arrayToUpdate['delete']) && count($arrayToUpdate['delete']) > 0) {
                    $deleteFile = array();
                    $this->deleteForOperator($arrayToUpdate['delete'], XML_TEZTOUR);
                    unset($arrayToUpdate['delete']);
                    unset($deleteFile);
                }

                /*
                 * $arrayToUpdate имеет вид array('add' => array(массив добавлениия), 'update' => array(массив обновления))
                 * $keyAction == add || update
                 */
                foreach ($arrayToUpdate as $keyAction => $arrayAction) {
                    if (isset($arrayToUpdate[$keyAction]) && !empty($arrayToUpdate[$keyAction]) && $keyAction == 'add') {
                        foreach ($arrayAction as $item) {
                            if ($item['version'] == 0) {
                                $item['version'] = '"' . $item['version'] . '"';
                            }
                            $spoFiles[] = array(
                                'operator'   => XML_TEZTOUR,
                                'id'         => $item['id'],
                                'version'    => $item['version'],
                                'url'        => 'http://book.tez-tour.com/book/vc/xml/spo_xml?spo=' . $item['id'],
                                'date'       => date("Y-m-d H:i:s")
                            );
                        }
                        $this->tblObjectForOerator->insertOrUpdate('xml_files_teztour', $spoFiles, true);
                        $this->tblObjectForOerator->insertOrUpdate('xml_teztour_spo_id_date', $arrayToUpdate['add'], true);
                        unset($spoFiles);
                    } elseif (isset($arrayToUpdate[$keyAction]) && !empty($arrayToUpdate[$keyAction]) && $keyAction == 'update') {
                        foreach ($arrayAction as $item) {
                            if ($item['preversion'] < $item['version']) {
                                for ($countVersion = $item['preversion'] + 1; $countVersion <= $item['version']; $countVersion ++) {
                                    $spoFilesUpdate[] = array(
                                        'operator' => XML_TEZTOUR,
                                        'id'       => $item['id'],
                                        'version'  => $countVersion,
                                        'url'      => 'http://xml.tez-tour.com/xmlgate/spo/' . $item['id'] . '/diffs/' . $countVersion . '.xml',
                                        'date'     => date("Y-m-d H:i:s")
                                    );
                                }
                            }
                        }
                        $this->tblObjectForOerator->insertOrUpdate('xml_files_teztour', $spoFilesUpdate, true);
                        $this->tblObjectForOerator->insertOrUpdate('xml_teztour_spo_id_date', $arrayToUpdate['update'], true);
                    }
                }
                // Если спо нет в бд
            } else {
                foreach ($this->aSpoList as $item) {
                    $spoFiles[] = array(
                        'operator'   => XML_TEZTOUR,
                        'id'         => $item['id'],
                        'version'    => $item['version'],
                        'url'        => 'http://book.tez-tour.com/book/vc/xml/spo_xml?spo=' . $item['id'],
                        'date'       => date("Y-m-d H:i:s")
                    );
                }
                $this->tblObjectForOerator->insertOrUpdate('xml_files_teztour', $spoFiles, true);
                $this->tblObjectForOerator->insertOrUpdate('xml_teztour_spo_id_date', $this->aSpoList, true);
            }
            unset($this->tblObjectForOerator);
//            $this->insertLog(null, CRON_LIST, XML_TEZTOUR, null, null);
        }
    }

    /**
     * Инициализация справочных данных
     *
     * @param  string     $param
     * @return mixed|void
     */
//    protected function initializeArray($param = 'all')
//    {
//        $operator_id = Operator::getOperatorId('TEZTOUR');
//
////        //room
////        if ($allCatalog === true || $curentCatalog == 'hotelRoom') {
////            // Справочник HotelRoom (связка отель и тип комнаты)
////            $this->aHotelRoom = array();
////            $aHotelRooms = $this->tblTeztur->getAllHotelRoom();
////            foreach ($aHotelRooms as $key =>$val) {
////                $this->aHotelRoom[$val['hotel_room_id']] = $val;
////            }
////            unset($aHotelRooms);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'room') {
////            $this->aRoom = array();
////            // Справочник Room ( комнаты)
////            $aRooms = $this->tblTeztur->getAllRoom();
////            foreach ($aRooms as $key =>$val) {
////                $this->aRoom[$val['room_id']] = '';
////            }
////            unset($aRooms);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'hotels') {
////            $this->aHotel = array();
////            // Справочник HotelRoom (связка отель и тип комнаты)
////            $aHotel = $this->tblTeztur->getAllHotel();
////            foreach ($aHotel as $key =>$val) {
////                $this->aHotel[$val['hotel_id']] = '';
////            }
////            unset($aHotel);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'country') {
////            $this->aCountry = array();
////            // Справочник HotelRoom (связка отель и тип комнаты)
////            $aCountries = $this->tblTeztur->getAllCountry();
////            foreach ($aCountries as $key =>$val) {
////                $this->aCountry[$val['country_id']] = '';
////            }
////            unset($aCountries);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'hotelPansion') {
////            $this->aHotelPansion = array();
////            $this->aPansion = array();
////            // Справочник HotelPansion (связка отель и тип питания)
////            $aHotelPansions = $this->tblTeztur->getPansionById();
////            foreach ($aHotelPansions as $key =>$val) {
////                $this->aHotelPansion[$val['hotel_pansion_id']] = $val;
////                $this->aPansion[$val['pansion_id']] = null;
////            }
////            #p2($this->aHotelPansion, 'HP');
////            #p2($this->aPansion, 'P');
////            unset($aHotelPansions);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'groupTypeAge') {
////            // Справочник GroupTypeAge (количество чел взрослых и детей)
////            $this->aGroupTypeAge = array();
////            $aGroupTypeAges = $this->tblTeztur->getGroupTypeAge();
////            foreach ($aGroupTypeAges as $key =>$val) {
////                $this->aGroupTypeAge[$val['groupTypeAgeId']] = $val;
////            }
////            unset($aGroupTypeAges);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'stayType') {
////            $this->aStayType = array();
////            // Справочник StayType
////            $aStayTypes = $this->tblTeztur->getStayType();
////            foreach ($aStayTypes as $key =>$val) {
////                $this->aStayType[$val['stay_type_id']] = $val;
////            }
////            unset($aStayTypes);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'cities') {
////            $this->aRegion = array();
////            // Справочник регионов
////            $aRegions = $this->tblTeztur->getRegion();
////            foreach ($aRegions as $key =>$val) {
////                $this->aRegion[$val['region_id']] = $val;
////            }
////            unset($aRegions);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'region') {
////            $this->aRegion = array();
////            // Справочник регионов
////            $aRegions = $this->tblTeztur->getRegion();
////            foreach ($aRegions as $key =>$val) {
////                $this->aRegion[$val['region_id']] = $val;
////            }
////            unset($aRegions);
////        }
////
////        if ($allCatalog === true || $curentCatalog == 'hotelStar') {
////            $this->hotelStar = array();
////            $this->hotelStarName = array();
////            // Справочник рейсов
////            $aHotelStar = $this->tblTeztur->getAllHotelStar();
////            foreach ($aHotelStar as $key =>$val) {
////                $this->hotelStar[$val['id']] = $val['id'];
////                $this->hotelStarName[$val['name']] = $val['id'];
////            }
////            unset($aHotelStar);
////        }
//    }

    /**
     * Формирование массива тура и запись(обновление) в базу
     *  if(!array_key_exists(ключ, массив)) - существует ключ в справочнике
     *
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('TEZTOUR');
        $node_attributes = $node['node']['attributes'];

        if (!isset($node['name'])) {
//            $this->insertLog('error', CRON_PARSE, XML_TEZTOUR, 14, $this->currentSpoId);
        }
        if ($node['name'] == 'message') {
            #$this->tblTeztur->insertOneSpoIdDate($item['id'], $item['date'], 'empty',null);
            #$this->tblTeztur->emptySpoFiles($this->currentSpoId, 'empty');
            $this->emptyFile = true;
        }

        if ($node['name'] == 'id') {
            $this->eachSpo['spo'] = $node['node']['body'];
            if (!isset($this->aRegion[$this->corentCityDepatured]) ) {
                $this->getCatalog('cities');
            }
            if (!isset($this->aRegion[$this->corentCityDepatured]) ) {
                #$this->errorTour = true;
                #$this->updatedCatalog = true;
                #$this->insertLog('error', CRON_PARSE, XML_TEZTOUR, 8, $this->currentSpoId . 'id: ' . $node['node'][0]);
                #$this->insertLog('xml_teztour_error', 'no id', $node['node'][0] , null, 'xml_log', $this->currentSpoId, 'cities', null);
            }
            $this->eachSpo['cityDepatured'] = $this->aRegion[$this->corentCityDepatured]['region_id'];
        }

        if ($node['name'] == 'hotelRoom') {
            $this->errorTour = false;
            // В справочнике нет нужного элемента
            if ( !isset($this->aHotelRoom[ $node_attributes['id'] ]) && !isset($this->dublicateId[$node_attributes['id']])) {
                // Обновляем справочник
                $this->getCatalog('hotelRoom');
            }
            if ( !isset($this->aHotelRoom[ $node_attributes['id'] ]) && !isset($this->dublicateId[$node_attributes['id']])) {
                //после обновления нет нужного елемента
                $this->dublicateId[$node_attributes['id']] = 'hotelRoom';
                $this->errorTour = true;
                $this->updatedCatalog = true;
//                $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 15, 'СПО: ' . $this->currentSpoId . ' ID' . $node_attributes['id']);
                #$this->insertLog('xml_teztour_error', 'no id', $node_attributes['id'] ,null , 'xml_log', $this->currentSpoId ,'hotelRoom' , null);
            }

            // Заполняем данные связаные с хотел рум
            if (isset($this->aHotelRoom[ $node_attributes['id'] ])) {
                if (!isset($this->aRoom[$this->aHotelRoom[$node_attributes['id']]['room_id']])) {
                    $this->getCatalog('room');
                }
                if (!isset($this->aRoom[$this->aHotelRoom[$node_attributes['id']]['room_id']])) {
                    //после обновления нет нужного елемента
                    $this->errorTour = true;
                    $this->updatedCatalog = true;
//                    $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 16, 'СПО: ' . $this->currentSpoId . ' ID' . $this->aHotelRoom[$node_attributes['id']]['room_id']);
                    #$this->insertLog('xml_teztour_error', 'no id', $this->aHotelRoom[$node_attributes['id']]['room_id'], null, 'xml_log', $this->currentSpoId, 'room', null);
                }
                $this->eachSpo['room'] = isset($this->aHotelRoom[$node_attributes['id']]) ? $this->aHotelRoom[$node_attributes['id']]['room_id'] : '';

                // Регион
                if (!isset($this->aRegion[$this->aHotelRoom[$node_attributes['id']]['region_id']])) {
                    $this->getCatalog('region');
                }
                if (!isset($this->aRegion[$this->aHotelRoom[$node_attributes['id']]['region_id']])) {
                    //после обновления нет нужного елемента
                    $this->errorTour = true;
                    $this->updatedCatalog = true;
//                    $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 17, 'СПО: ' . $this->currentSpoId . ' ID' . $this->aHotelRoom[$node_attributes['id']]['region_id']);
                    #$this->insertLog('xml_teztour_error', 'no id',  $this->aHotelRoom[$node_attributes['id']]['region_id'], null, 'xml_log', $this->currentSpoId, 'region', null);
                }
                $this->eachSpo['city'] = isset($this->aHotelRoom[$node_attributes['id']]) ? $this->aHotelRoom[$node_attributes['id']]['region_id'] : '';

                if (!isset($this->aCountry[$this->aHotelRoom[$node_attributes['id']]['cntr_id']])) {
                    $this->getCatalog('country');
                }
                if (!isset($this->aCountry[$this->aHotelRoom[$node_attributes['id']]['cntr_id']])) {
                    //после обновления нет нужного елемента
                    $this->errorTour = true;
                    $this->updatedCatalog = true;
//                    $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 6, 'СПО: ' . $this->currentSpoId . ' ID' . $this->aHotelRoom[$node_attributes['id']]['cntr_id']);
                    #$this->insertLog('xml_teztour_error', 'no id', $this->aHotelRoom[$node_attributes['id']]['cntr_id'], null, 'xml_log', $this->currentSpoId, 'country', null);
                }
                $this->eachSpo['country'] = isset($this->aHotelRoom[$node_attributes['id']]) ? $this->aHotelRoom[$node_attributes['id']]['cntr_id'] : '';

                if (!isset($this->aHotel[$this->aHotelRoom[$node_attributes['id']]['hotel_id']])) {
                    $this->getCatalog('hotels');
                }
                if (!isset($this->aHotel[$this->aHotelRoom[$node_attributes['id']]['hotel_id']])) {
                    //после обновления нет нужного елемента
                    $this->errorTour = true;
                    $this->updatedCatalog = true;
//                    $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 9, 'СПО: ' . $this->currentSpoId . ' ID' . $this->aHotelRoom[$node_attributes['id']]['hotel_id']);
                    #$this->insertLog('xml_teztour_error', 'no id', $this->aHotelRoom[$node_attributes['id']]['hotel_id'] , null , 'xml_log', $this->currentSpoId, 'hotels', null);
                }
                $this->eachSpo['hotel'] = isset($this->aHotelRoom[$node_attributes['id']]) ? $this->aHotelRoom[$node_attributes['id']]['hotel_id'] : '';

                if ( !isset($this->hotelStar[$this->aHotelRoom[$node_attributes['id']]['star']])) {
                    $this->getCatalog('hotelStar');
                }
                if (!isset($this->hotelStar[$this->aHotelRoom[$node_attributes['id']]['star']])) {
                    //после обновления нет нужного елемента
                    $this->errorTour = true;
                    $this->updatedCatalog = true;
//                    $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 20, 'СПО: ' . $this->currentSpoId . ' ID' . $this->aHotelRoom[$node_attributes['id']]['star']);
                    #$this->insertLog('xml_teztour_error', 'no id' , $this->aHotelRoom[$node_attributes['id']]['star'] , null , 'xml_log', $this->currentSpoId, 'hotelStar', null);
                }
                $this->eachSpo['star'] = isset($this->hotelStar[$this->aHotelRoom[$node_attributes['id']]['star']]) ? $this->hotelStar[$this->aHotelRoom[$node_attributes['id']]['star']] : '';
            } else {
                $this->errorTour = true;
                $this->eachSpo['room'] = null;
                $this->eachSpo['city'] = null;
                $this->eachSpo['country'] = null;
                $this->eachSpo['hotel'] = null;
                $this->eachSpo['star'] = null;
            }
        }

        if ($node['name'] == 'arrivalRegionId') {
            // Сверяем существование региона
            if (!array_key_exists( $node['node']['body'], $this->aRegion) && !isset($this->dublicateId[$node['node']['body']])) {
                $this->getCatalog('region');
            }
            if (!array_key_exists( $node['node']['body'],$this->aRegion) && !isset($this->dublicateId[$node['node']['body']])) {
                $this->dublicateId[$node['node']['body']] = 'arrivalRegionId';
                #$this->errorTour = true;
                #$this->updatedCatalog = true;
                #$this->insertLog('xml_teztour_error', 'no id', $node['node']['body'] , $node['node']['body'], 'xml_log', $this->currentSpoId, 'region', null);
            }
            $this->eachSpo['arrivalRegion'] = $this->aRegion[$node['node']['body']]['region_id'];
        }

        if ($node['name'] == 'departureRegionId') {
            if (!array_key_exists( $node['node']['body'],$this->aRegion) && !isset($this->dublicateId[$node['node']['body']])) {
                $this->getCatalog('region');
            }
            if (!array_key_exists( $node['node']['body'],$this->aRegion) && !isset($this->dublicateId[$node['node']['body']])) {
                $this->dublicateId[$node['node']['body']] = 'departureRegionId';
                #$this->errorTour = true;
                #$this->updatedCatalog = true;
                #$this->insertLog('xml_teztour_error', 'no id', $node['node']['body'] , null , 'xml_log', $this->currentSpoId, 'region', null);
            }
//            $this->eachSpo['departuredRegion'] = $this->aRegion[$node['node']['body']]['region_id'];
        }

        if ($node['name'] == 'date') {
            $this->errorTour = false;
            $this->eachSpo['dateStart'] = substr($node_attributes['value'],-4) . '-' . substr($node_attributes['value'], 3 , 2).'-'.substr($node_attributes['value'], 0 , 2);
            $this->eachSpo['dateStartIndex'] = $this->eachSpo['dateStart'];
        }

        if ($node['name'] == 'nightCount') {
            $this->eachSpo['nightCount'] = $node_attributes['value'];
            $this->eachSpo['dateEnd'] = date("Y-m-d",strtotime($this->eachSpo['dateStart'])+60*60*24*$this->eachSpo['nightCount']);
        }

        if ($node['name'] == 'hotelPansion') {
            if (!array_key_exists($node_attributes['value'], $this->aHotelPansion) && !isset($this->dublicateId[$node_attributes['value']])) {
                $this->getCatalog('hotelPansion');
            }
            if (!array_key_exists($node_attributes['value'], $this->aHotelPansion) && !isset($this->dublicateId[$node_attributes['value']])) {
                $this->dublicateId[$node_attributes['value']] = 'hotelPansion';
                $this->errorTour = true;
                $this->updatedCatalog = true;
//                $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 21, 'СПО: ' . $this->currentSpoId . ' ID' . $node_attributes['value']);
                #$this->insertLog('xml_teztour_error', 'no id' ,  $node_attributes['value'] , null , 'xml_log', $this->currentSpoId, 'hotelPansion', null);
            }

            // Данные по хотел пансион
            if ( array_key_exists($node_attributes['value'], $this->aHotelPansion)) {
                if (!array_key_exists($this->aHotelPansion[$node_attributes['value']]['pansion_id'], $this->aPansion)) {
                    $this->getCatalog('pansion');
                }
                if (!array_key_exists($this->aHotelPansion[$node_attributes['value']]['pansion_id'], $this->aPansion)) {
                    $this->errorTour = true;
                    $this->updatedCatalog = true;
//                    $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 12, 'СПО: ' . $this->currentSpoId . ' ID' . $this->aHotelPansion[$node_attributes['value']]['pansion_id']);
                    #$this->insertLog('xml_teztour_error', 'no id', $this->aHotelPansion[$node_attributes['value']]['pansion_id'], null , 'xml_log', $this->currentSpoId, 'pansion', null);
                }
                $this->eachSpo['pansion'] = $this->aHotelPansion[$node_attributes['value']]['pansion_id'];
            } else {
                $this->errorTour = true;
                $this->eachSpo['pansion'] = null;
            }
        }

        if ($node['name'] == 'price') {
            // start stayType
            if (!array_key_exists($node_attributes['stayType'], $this->aStayType) && !isset($this->dublicateId[$node_attributes['stayType']])) {
                $this->getCatalog('stayType');
            }
            if (!array_key_exists($node_attributes['stayType'], $this->aStayType) && !isset($this->dublicateId[$node_attributes['stayType']])) {
                $this->errorTour = true;
                $this->updatedCatalog = true;
                $this->dublicateId[$node_attributes['stayType']] = 'stayType';
//                $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 11, 'СПО: ' . $this->currentSpoId . ' ID' . $node_attributes['stayType']);
                #$this->insertLog('xml_teztour_error', 'no id', $node_attributes['stayType'] , null , 'xml_log', $this->currentSpoId, 'stayType', null);
            }
            $this->eachSpo['stayType'] = isset($this->aStayType[$node_attributes['stayType']]) ? $this->aStayType[$node_attributes['stayType']]['stay_type_id'] : null;
            // end stayType

            // start groupTypeAge
            if (!array_key_exists($node_attributes['groupTypeAge'], $this->aGroupTypeAge) && !isset($this->dublicateId[$node_attributes['groupTypeAge']])) {
                $this->getCatalog('groupTypeAge');
            }
            if (!array_key_exists($node_attributes['groupTypeAge'], $this->aGroupTypeAge) && !isset($this->dublicateId[$node_attributes['groupTypeAge']])) {
                $this->errorTour = true;
                $this->updatedCatalog = true;
                $this->dublicateId[$node_attributes['groupTypeAge']] = 'groupTypeAge';
//                $this->insertLog('warning', CRON_PARSE, XML_TEZTOUR, 22, 'СПО: ' . $this->currentSpoId . ' ID' . $node_attributes['groupTypeAge']);
                #$this->insertLog('xml_teztour_error', 'no id', $node_attributes['groupTypeAge'] , null, 'xml_log', $this->currentSpoId, 'groupTypeAge', null);
            }
            if (array_key_exists($node_attributes['groupTypeAge'], $this->aGroupTypeAge)) {
                $this->eachSpo['adult'] = $this->aGroupTypeAge[$node_attributes['groupTypeAge']]['adultCount'];
                $this->eachSpo['children'] = $this->aGroupTypeAge[$node_attributes['groupTypeAge']]['bigChildCount'];
                $this->eachSpo['infant'] = $this->aGroupTypeAge[$node_attributes['groupTypeAge']]['smallChildCount'];
            } else {
                $this->errorTour = true;
                $this->eachSpo['adult'] = null;
                $this->eachSpo['children'] = null;
                $this->eachSpo['infant'] = null;
            }

            // end groupTypeAge
            $this->eachSpo['tour'] = $node_attributes['id'];
            $this->eachSpo['price'] = $node['node']['body'];
            $this->eachSpo['update'] = 1;
            $this->eachSpo['tourOperator'] = $operator_id;
            $this->eachTour['currency'] = 'USD';

            // Подсчет туров
            $this->CountTour++;

            if (strtotime($this->eachSpo['dateStart']) < time()) {
                $this->errorTour = true;
            }
            // Стек только с полным набором данных о туре
            if ($this->errorTour ===  false) {
                $this->eachFiftiesTour[] = $this->eachSpo;
            }
            $this->saveTours($operator_id);
        }
        $this->saveLastTours($node, $operator_id);
    }

    /**
     * Обновление справочников
     * TODO Переделать через DOMDocument->getElementsByTagName()
     *
     * @param  null $type
     * @return bool
     */
    public function getCatalog($type = null)
    {
        if ($type == 'all' || $type == 'hotelRoom') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/hotelrooms.xml';
//            $tmpTeztourList->setReadBanch(array('id', 'prop'));
        }

        if ($type == 'all' || $type == 'hotels') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/hotels.xml';
            $this->listType = 'hotels';
            $this->initializeArray(false, 'hotelStar');
//            $tmpTeztourList->setReadBanch(array('id', 'name', 'prop'));
        }

        if ($type == 'all' || $type == 'room') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/Rooms.xml';
            $this->listType = 'room';
//            $tmpTeztourList->setReadBanch(array('id', 'name'));
        }

        if ($type == 'all' || $type == 'country') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/country.xml';
            $this->listType = 'country';
//            $tmpTeztourList->setReadBanch(array('id', 'prop'));
        }

        if ($type == 'all' || $type == 'hotelPansion') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/hotelPansions.xml';
            $this->listType = 'hotelPansion';
//            $tmpTeztourList->setReadBanch(array('id', 'prop'));
        }

        if ($type == 'all' || $type == 'pansion') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/pansions.xml';
            $this->listType = 'pansion';
//            $tmpTeztourList->setReadBanch(array('id', 'name'));
        }

        if ($type == 'all' || $type == 'stayType') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/staytype.xml';
            $this->listType = 'stayType';
//            $tmpTeztourList->setReadBanch(array('id', 'name'));
        }

        if ($type == 'all' || $type == 'groupTypeAge') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/groupTypeAges.xml';
            $this->listType = 'groupTypeAge';
//            $tmpTeztourList->setReadBanch(array('id', 'prop'));
        }

        if ($type == 'all' || $type == 'region') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/regions.xml';
            $this->listType = 'region';
//            $tmpTeztourList->setReadBanch(array('id', 'prop'));
        }

        if ($type == 'all' || $type == 'cities') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/cities.xml';
            $this->listType = 'cities';
//            $tmpTeztourList->setReadBanch(array('id', 'prop'));
        }

        if ($type == 'all' || $type == 'hotelStar') {
            $fileHotelRoom = FS_FILES. '/xml_teztour_catalog/teztour/hotelTypes.xml';
            $this->listType = 'hotelStar';
//            $tmpTeztourList->setReadBanch(array('id', 'name'));
        }

//        if ($tmpTeztourList->readXmlByUrl($fileHotelRoom) === false) {
////            $this->insertLog('error', CRON_PARSE, XML_TEZTOUR, 5, $this->listType);
//            return false;
//        }
//        $tmpTeztourList->hotelStarName = $this->hotelStarName;
//        $tmpTeztourList->listType = $this->listType;
//        $tmpTeztourList->setFunctionHandler(array($tmpTeztourList, 'parseList'));
//        $tmpTeztourList->getXmlAssoc();
//        $this->initializeArray(false, $this->listType);
    }
}

//class xml_teztour_list extends TeztourParser
//{
//    public $hotelStarName = array();
//    public $listType;
//
//    public function __construct()
//    {
//        parent::__construct();
//        $this->tblTeztur = new tbl_teztour();
//        $this->catalogArray = array();
//    }
//
//    /**
//     * @param $node
//     */
//    public function parseList($node)
//    {
//        if (isset($this->listType) && $node['name'] != 'eof') {
//            switch ($this->listType) {
//                case 'hotelRoom':
//                    $this->table = 'xml_teztour_hotel_room';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['hotel_room_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Hotel') {
//                        $this->eachElement['hotel_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Room') {
//                        $this->eachElement['room_id'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'hotels':
//                    $this->table = 'xml_teztour_hotel';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['hotel_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['name'] == 'name') {
//                        $this->eachElement['name'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'type') {
//                        $this->eachElement['star'] = $this->hotelStarName[trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['body']))];
//                    }
//                    if ($node['node']['attributes']['name'] == 'Country') {
//                        $this->eachElement['country_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Region') {
//                        $this->eachElement['region_id'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'hotelStar':
//                    $this->table = 'xml_teztour_hotel_star';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['id'] = trim($node['node']['body']);
//                    }
//                    if ($node['name'] == 'name') {
//                        $this->eachElement['name'] = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['body']));
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'room':
//                    $this->table = 'xml_teztour_room';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['room_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['name'] == 'name') {
//                        $this->eachElement['name'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'country':
//                    $this->table = 'xml_teztour_country';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['country_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['name'] == 'prop') {
//                        $this->eachElement['country_name'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'hotelPansion':
//                    $this->table = 'xml_teztour_hotel_pansion';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['hotel_pansion_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Hotel') {
//                        $this->eachElement['hotel_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Pansion') {
//                        $this->eachElement['pansion_id'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'pansion':
//                    $this->table = 'xml_teztour_pansion';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['pansion_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['name'] == 'name') {
//                        $this->eachElement['pansion_name'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'stayType':
//                    $this->table = 'xml_teztour_stay_type';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['stay_type_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['name'] == 'name') {
//                        $this->eachElement['stay_type_name'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'groupTypeAge':
//                    $this->table = 'xml_teztour_group_type_age';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['groupTypeAgeId'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'adultCount') {
//                        $this->eachElement['adultCount'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'adultCount') {
//                        $this->eachElement['smallChildCount'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'bigChildCount') {
//                        $this->eachElement['bigChildCount'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'infantAge') {
//                        $this->eachElement['infantAge'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'smallChildAge') {
//                        $this->eachElement['smallChildAge'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'bigChildAge') {
//                        $this->eachElement['bigChildAge'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'cities':
//                    $this->table = 'xml_teztour_region';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['region_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Country') {
//                        $this->eachElement['cntr_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'russianName') {
//                        $this->eachElement['region_name'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//                case 'region':
//                    $this->table = 'xml_teztour_region';
//                    if ($node['name'] == 'id') {
//                        $this->eachElement['region_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'Country') {
//                        $this->eachElement['cntr_id'] = trim($node['node']['body']);
//                    }
//                    if ($node['node']['attributes']['name'] == 'russianName') {
//                        $this->eachElement['region_name'] = trim($node['node']['body']);
//                        $this->catalogArray[] = $this->eachElement;
//                    }
//                    break;
//            }
//        }
//
//        if (count($this->catalogArray) == 50) {
//            $this->tblTeztur->insertOrUpdate($this->table, $this->catalogArray, true);
//            $this->catalogArray = array();
//            $this->eachElement = array();
//            $this->table = null;
//        }
//        if ($node['name'] == 'eof' && !empty($this->catalogArray)) {
//            $this->tblTeztur->insertOrUpdate($this->table,$this->catalogArray, true);
//            $this->catalogArray = array();
//            $this->eachElement = array();
//            $this->table = null;
//        }
//        if (isset($node['error']))
//        {
//            $this->insertLog('error', CRON_PARSE, XML_TEZTOUR, 5, $this->listType);
//            #$this->insertLog('xml_teztour_error', 'not load', null, null, 'xml_log', $this->listType, null, null);
//        }
//    }
//}
