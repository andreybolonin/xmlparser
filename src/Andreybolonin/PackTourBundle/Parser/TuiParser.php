<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DomCrawler\Crawler;

class TuiParser extends XmlPullReader
{

    private $url = 'http://xml2.voyage.kiev.ua/';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их в бд
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $html = $this->buzz->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $crawler = new Crawler($html, $this->url);
        $files = $crawler->filter('a');

        foreach ($files as $file) {
            if (!$this->checkExtension($file->getAttribute('href'))) {
                continue;
            }
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $this->url . $file->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $this->url . $file->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * @param $spo
     */
    public function domParser2($spo)
    {
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->loadXML(file_get_contents($spo['path']));
        $this->showDOMNode($document);
    }

    /**
     * Экспериментальный метод для перебора бесконечно вложенных XML-деревьев
     *
     * @param \DOMNode $domNode
     */
    public function showDOMNode(\DOMNode $domNode)
    {
        foreach ($domNode->childNodes as $node) {

            if ($node->nodeName == 'OFFER') {
                var_dump('OFFER:');
                var_dump($node->getAttribute('DATE'));
                var_dump($node->getAttribute('COUNTRY'));
                var_dump($node->getAttribute('CURRENCY'));
            }

            if ($node->nodeName == 'HOTEL') {
                var_dump('HOTEL:');
                var_dump($node->getAttribute('NAME'));
                var_dump($node->getAttribute('TOWN'));
                var_dump($node->getAttribute('HTC'));
                var_dump($node->getAttribute('STAR'));
                var_dump($node->getAttribute('ROOM'));
                var_dump($node->getAttribute('RMC'));
                var_dump($node->getAttribute('MEAL'));
            }

            if ($node->nodeName == 'TRANSPORT') {
                var_dump('TRANSPORT:');
                var_dump($node->getAttribute('TOWNFR'));
                var_dump($node->getAttribute('TOWNTO'));
            }

            if ($node->nodeName == 'PRICE') {
                var_dump('PRICE:');
                var_dump($node->getAttribute('DATE'));
                var_dump($node->getAttribute('VAL'));
                exit;
            }

            if ($node->hasChildNodes()) {
//            if ($node->childNodes->length != 1) {
                $this->showDOMNode($node);
            }
        }
    }

    /**
     * Парсинг файла
     *
     * @param $node
     * @return mixed|void
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        if (isset($node['node']['attributes'])) {
            $node_attributes = $node['node']['attributes'];
        }
        $operator_id = $this->spo['operator'];

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            if (isset($node_attributes['SPO'])) {
                $this->spoName = $node_attributes['SPO'];
                $this->currentSpo = $node_attributes['SPO'];
            }

            // country
            if (strpos($node_attributes['COUNTRY'], ',') === false) {
                if (!isset($this->countrySelf[$node_attributes['COUNTRY']])) {
                    // Проверяем есть ли в общем каталоге
                    if (strlen($node_attributes['COUNTRY']) > 0) {
                        if (!isset($this->country[$node_attributes['COUNTRY']])) {
                            $this->addCountry(array(
                                'code' => trim($node_attributes['COUNTRY']),
                                'name' => trim($node_attributes['COUNTRY'])
                            ));
                        } else {
                            $this->addCountry(array(
                                'code' => trim($node_attributes['COUNTRY']),
                                'name' => $this->country[$node_attributes['COUNTRY']]['name']
                            ));
                        }
                    } else {
                        $this->errorCountry();
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$node_attributes['COUNTRY']]) ? $this->countrySelf[$node_attributes['COUNTRY']] : null;
            } else {
                $countryArray = explode(',', $node_attributes['COUNTRY']);
                if (!isset($this->countrySelf[$countryArray[1]])) {
                    if (!isset($this->country[$node_attributes['COUNTRY']])) {
                        $this->addCountry(array(
                            'code' => trim($countryArray[1]),
                            'name' => trim($countryArray[1]),
                        ));
                    } else {
                        $this->addCountry(array(
                            'code' => trim($countryArray[1]),
                            'name' => $this->country[$countryArray[1]]['name']
                        ));
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$countryArray[1]]) ? $this->countrySelf[$countryArray[1]] : null;
            }

//            if (isset($node_attributes['TOURTYPE']) && !isset($this->tourtype[$node_attributes['TOURTYPE']])) {
//                $this->addTourType(array('name' => trim($node_attributes['TOURTYPE'])));
//            }
//
//            $this->tour_type = $this->tourtype[$node_attributes['TOURTYPE']] ? $this->tourtype[$node_attributes['TOURTYPE']] : 0;
            $this->eachTour['adult'] = $node_attributes['ADL'];
            $this->eachTour['children'] = $node_attributes['CHD'];
            $this->eachTour['infant'] = $node_attributes['INF'];
            $this->eachTour['currency'] = $node_attributes['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {

            // star
            if (isset($node_attributes['STAR'])) {
                $hotelStar = strlen($node_attributes['STAR']);
                if (!isset($this->hotelStar[$hotelStar])) {
                    $this->addStar(array('star' => $hotelStar));
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // city
            if (!isset($this->region[$node_attributes['TOWN']]) && $this->errorTour === false) {
                if (strlen($node_attributes['TOWN']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node_attributes['TOWN']),
                        'country_id' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorCity();
                }
            }
            $this->eachTour['city'] = $this->region[$node_attributes['TOWN']];

            // hotel
            if (!isset($this->hotel[ $node_attributes['HTC']]) && $this->errorTour === false) {
                if (strlen($node_attributes['HTC'] > 0)) {
                    $this->addHotel(array(
                        'id'      => $node_attributes['HTC'],
                        'name'    => $node_attributes['NAME'],
                        'star'    => $this->hotelStar[$hotelStar],
                        'city'    => $this->region[$node_attributes['TOWN']],
                        'country' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorHotel();
                }
            }
            $this->eachTour['hotel'] = $node_attributes['HTC'];

            // room
            if (!isset($this->roomIds[$node_attributes['RMC']])) {
                if (strlen($node_attributes['RMC'] > 0)) {
                    $this->addRoom(array(
                        'id' => trim($node_attributes['RMC']),
                        'name' => trim($node_attributes['ROOM'])
                    ));

                } else {
                    $this->errorRoom();
                }
            }
            $this->eachTour['room'] = $node_attributes['RMC'];

            // stay_type
            if (!isset($this->stayTypeIds[$node_attributes['PLC']])) {
                if (strlen($node_attributes['PLC'] > 0)) {
                    $this->addStayType(array(
                        'id' => trim($node_attributes['PLC']),
                        'name' => trim($node_attributes['PLACE'])
                    ));
                } else {
                    $this->errorStayType();
                }
            }
            $this->eachTour['stayType'] = $node_attributes['PLC'];

            // pansion
            if (!isset($this->pansionIds[$node_attributes['MLC']])) {
                if (strlen($node_attributes['MLC']) > 0 && strlen($node_attributes['MEAL']) > 0) {
                    $this->addPansion(array(
                        'id' => trim($node_attributes['MLC']),
                        'name' => trim($node_attributes['MEAL'])
                    ));
                } else {
                    $this->errorPansion();
                }
            }
            $this->eachTour['pansion'] = $node_attributes['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node_attributes['BEG'] == '0') {

                if (isset($this->region[$node_attributes['TOWNFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['TOWNFR']];
                } elseif (strlen($node_attributes['TOWNFR']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node_attributes['TOWNFR']),
                        'country_id' => null
                    ));
                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['TOWNFR']];
//                    $this->eachTour['city'] = $this->region[$node_attributes['TOWNFR']];
                } elseif (strlen($node_attributes['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node_attributes['TOWNTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['TOWNTO']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node_attributes['TOWNTO']),
                        'country_id' => null
                    ));
//                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
                } elseif (strlen($node_attributes['TOWNTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            }
        }

        if ($node['name'] == 'PRICE') {

            // Сортировка по дате и актуальности туров
            if (strtotime($node_attributes['DATE']) > time() &&
//                strtotime($node_attributes['DATE']) < $this->container->getParameter('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node_attributes['DATE'];
                $this->eachTour['price'] = $node_attributes['VAL'];
                $this->eachTour['nightCount'] = $node_attributes['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart'])+86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['updated'] = 1;
                $this->eachTour['tourOperator'] = $operator_id;
                $this->eachFiftiesTour[] = $this->eachTour;

                // Запись туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);
    }

}
