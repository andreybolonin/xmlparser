<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DomCrawler\Crawler;

class AnextourParser extends XmlPullReader
{

    private $url = 'http://online3.anextour.ru/xml.php';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $crawler = new Crawler($html, $this->url);
        $all_spo = $crawler->filter('a');

        foreach ($all_spo as $el) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $el->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $el->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     *  Отбор туров
     *
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = $this->spo['operator'];
        if (isset($node['node']['attributes'])) {
            $node_attributes = $node['node']['attributes'];
        }

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            $this->spoName = $node['node']['attributes']['SPO'];

            // В теге одна страна
            if (strpos($node['node']['attributes']['COUNTRY'], ',') === false) {
                if (!isset($this->country[$node['node']['attributes']['COUNTRY']])) {
                    $this->errorCountry();
                }
                $this->eachTour['country'] = isset($this->country[$node['node']['attributes']['COUNTRY']]['id']) ? $this->country[$node['node']['attributes']['COUNTRY']]['id'] : null;
            } else {
                $countryArray = explode(',',$node['node']['attributes']['COUNTRY']);
                if (!isset($this->country[$countryArray[1]])) {
                    $this->errorCountry();
                }
                $this->eachTour['country'] = isset($countryArray[1]) ? $countryArray[1] : null;
            }

            // tour_type
            if (!isset($this->tourtype[$node['node']['attributes']['TOURTYPE']])) {
                $this->addTourType(array('name' => trim($node_attributes['TOURTYPE'])));
            }

            $this->eachTour['adult'] = $node['node']['attributes']['ADL'];
            $this->eachTour['children'] = $node['node']['attributes']['CHD'];
            $this->eachTour['infant'] = $node['node']['attributes']['INF'];
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    $this->addStar(array('star' => $hotelStar));
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node['node']['attributes']['CITY']]) && $this->errorTour === false) {
                if ( strlen($node['node']['attributes']['CITY']) > 0 ) {
                    $this->addCity(array(
                        'name' => trim($node_attributes['CITY']),
                        'country_id' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorCity();
                }
            }
            $this->eachTour['city'] = $this->region[$node['node']['attributes']['CITY']];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {
                    $this->addHotel(array(
                        'id'      => $node_attributes['HTC'],
                        'name'    => $node_attributes['NAME'],
                        'star'    => $this->hotelStar[$hotelStar],
                        'city'    => $this->region[$node_attributes['CITY']],
                        'country' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorHotel();
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC'] > 0)) {
                    $this->addRoom(array(
                        'id' => trim($node['node']['attributes']['RMC']),
                        'name' => trim($node['node']['attributes']['ROOM'])
                    ));
                } else {
                    $this->errorRoom();
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC'] > 0)) {
                    $this->addStayType(array(
                        'id' => trim($node['node']['attributes']['PLC']),
                        'name' => trim($node['node']['attributes']['PLACE'])
                    ));
                } else {
                    $this->errorStayType();
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    $this->addPansion(array(
                        'id' => trim($node['node']['attributes']['MLC']),
                        'name' => trim($node['node']['attributes']['MEAL'])
                    ));
                } else {
                    $this->errorPansion();
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node['node']['attributes']['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITYFR']),
                        'country_id' => null
                    ));
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node['node']['attributes']['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITYTO']),
                        'country_id' => null
                    ));
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node['node']['attributes']['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITYFR']),
                        'country_id' => null
                    ));
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // Сортировка по дате и актуальности туров
            if (strtotime($node['node']['attributes']['DATE']) > time() &&
//                strtotime($node['node']['attributes']['DATE']) < $this->container->getParameter('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node['node']['attributes']['DATE'];//substr($node['node']['attributes']['DATE'], 0 , 4) . '-' . substr($node['node']['attributes']['DATE'], -4 , 2) . '-' . substr($node['node']['attributes']['DATE'], -2 , 2);
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d",strtotime($this->eachTour['dateStart'])+86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = $operator_id;
                $this->eachFiftiesTour[] = $this->eachTour;

                // Запись туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);
    }

}
