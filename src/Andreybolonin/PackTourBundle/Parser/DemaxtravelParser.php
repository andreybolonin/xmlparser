<?php

namespace Modul\Parser;

use Buzz\Browser;
use app\Entity\City;
use app\Entity\Hotel;
use app\Entity\Pansion;
use app\Entity\Room;
use app\Entity\Star;
use app\Entity\StayType;
use app\Entity\TourType;
use app\Entity\Spo;
use app\Operator;
use app\Xmlpullreader;
use Symfony\Component\DomCrawler\Crawler;

class DemaxtravelParser extends Xmlpullreader
{
    public function __construct()
    {
        parent::__construct();
    }

    private $url = 'http://demaxtravel.com/xml/';

    /**
     * Получает список SPO и сохраняет их в таблицу list
     */
    public function getAndSaveSpoList()
    {
        try {
            $browser = new Browser();
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $crawler = new Crawler($html, $this->url);
        $all_spo = $crawler->filter('a');
        $operator_id = Operator::getOperatorId('DEMAXTRAVEL');

        foreach ($all_spo as $el) {
            $spoInObject[$this->url . $el->getAttribute('href')] = array(
                'file_name'=> $el->getAttribute('href'),
                'operator' => $operator_id,
                'url'      => $this->url . $el->getAttribute('href'),
                'status'   => 'nottouched',
            );
        }

        if (!empty($spoInObject)) {
            Spo::updateListByOperator($spoInObject, $operator_id);
        }
    }

    public function params($spo = array())
    {
        $this->initializeArray();
        $this->intializeParams($spo);

        if ($this->readXmlByUrl($spo['path']) === false) {
            //$this->insertLog('error', CRON_PARSE, XML_DEMAXTRAVEL, 5, $spo['url']);
            #$this->insertLog('xml_gto_error', 'error xml' , null, null, 'xml_log', $spo['url'], null, null);
            //$this->tblTour->insertOneSpoIdDate('xml_demaxtravel_spo_list',$this->currentSpoId, 'broken', $spo['url']);
            $spo['status'] = 'error';
            Spo::insertOrUpdateList($spo);

            return false;
        } else {
            $this->setReadBanch(array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE'));
            $this->setReadBanchOnlyAttr(array('OFFER' => true, 'HOTEL' => true, 'TRANSPORT' => true, 'PRICE' => true));
            $this->setFunctionHandler(array($this, 'getAllTourFromSpo'));
            $this->getXmlAssoc();
        }

        return $this->postXmlParsing($spo, $this);
    }

    /**
     * Инициализация справочных данных
     * Для обновления или загрузки одного справочника $allCatalog = FALSE , $curentCatalog = 	название каталога
     */
    protected function initializeArray($allCatalog = true, $curentCatalog = null)
    {
        if ($allCatalog === true || $curentCatalog == 'initCountry') {
            $aCountry = $this->tblTour->selectFields(array('id', 'shortName', 'shortName2') ,'xml_iso_country');
            foreach ($aCountry as $val) {
                $this->country[$val['shortName2']] = $val['id'];
                $this->country[$val['shortName']] = $val['id'];
            }
        }

        if ($allCatalog === true || $curentCatalog == 'initHotel') {
            $aHotel = Hotel::findAllByOperator($operator_id);
            foreach ($aHotel as $val) {
                $this->hotel[$val['id']] = '';
            }
        }

        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
            $aHotelStar = Star::findAllByOperator($operator_id);
            foreach ($aHotelStar as $val) {
                $this->hotelStar[$val['star']] = $val['id'];
            }
        }

        if ($allCatalog === true || $curentCatalog == 'initRegion') {
            $aRegion = City::findAllByOperator($operator_id);
            foreach ($aRegion as $val) {
                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
            }
        }

        if ($allCatalog == 'all' || $curentCatalog == 'initRoom') {
            if ($allCatalog === true || $curentCatalog == 'initRoom') {
                $aRoom = Room::findAllByOperator($operator_id);
                foreach ($aRoom as $val) {
                    $this->roomIds[$val['id']] = '';
                }
            }
        }

        if ($allCatalog === true || $curentCatalog == 'initStayType') {
            $aStayType = StayType::findAllByOperator($operator_id);
            foreach ($aStayType as $val) {
                $this->stayTypeIds[$val['id']] = '';
            }
        }

        if ($allCatalog === true || $curentCatalog == 'initPansion') {
            $aPansion = Pansion::findAllByOperator($operator_id);
            foreach ($aPansion as $val) {
                $this->pansionIds[$val['id']] = '';
            }
        }

        if ($allCatalog === true || $curentCatalog == 'initTourType') {
            $aTourType = TourType::findAllByOperator($operator_id);
            foreach ($aTourType as $val) {
                $this->tourtype[$val['name']] = $val['id'];
            }
        }

//        $this->initHotel($allCatalog, $curentCatalog, $this, self::$table);
//
//        $this->initHotelStar($allCatalog, $curentCatalog, $this, self::$table);
//
//        $this->initRegion($allCatalog, $curentCatalog, $this, self::$table);
//
//        $this->initRoom($allCatalog, $curentCatalog, $this, self::$table);
//
//        $this->initStayType($allCatalog, $curentCatalog, $this, self::$table);
//
//        $this->initPansion($allCatalog, $curentCatalog, $this, self::$table);
//
//        $this->initTourType($allCatalog, $curentCatalog, $this, self::$table);
    }

    /*
     *  Отбор туров
     */
    public function getAllTourFromSpo($node)
    {
        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;

            // В теге одна страна
            if (!isset($this->country[$node['node']['attributes']['COUNTRY']])) {
                $this->errorTour = true;
                if (strlen($node['node']['attributes']['COUNTRY']) > 0) {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 6, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id', null, null, 'xml_log', $this->currentSpoId , 'No such country', $node['node']['attributes']['COUNTRY']);
                } else {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 7, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id', null, null, 'xml_log', $this->currentSpoId , 'empty country tag', null);
                }
            }
            $this->eachTour['country'] = isset($this->country[$node['node']['attributes']['COUNTRY']]) ? $this->country[$node['node']['attributes']['COUNTRY']] : null;

            if (!isset($this->tourtype[$node['node']['attributes']['TOURTYPE']])) {
                $this->tblTour->insertOrUpdate('xml_demaxtravel_tourtype', array(array('name' => trim($node['node']['attributes']['TOURTYPE']))), true);
                $this->initializeArray(true, 'initTourType');
            }
            $this->eachTour['tourtype'] = $this->tourtype[$node['node']['attributes']['TOURTYPE']] ? $this->tourtype[$node['node']['attributes']['TOURTYPE']] : 0;

            $this->eachTour['adult'] = $node['node']['attributes']['ADL'];
            $this->eachTour['children'] = $node['node']['attributes']['CHD'];
            $this->eachTour['infant'] = $node['node']['attributes']['INF'];
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {

            // Определяем и записываем звездность отелей
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_star', array(array('star' => $hotelStar)), true);
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node['node']['attributes']['CITY']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['CITY']) > 0 ) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_region', array(array('name' => trim($node['node']['attributes']['CITY']), 'country_id' => $this->eachTour['country'])), true);
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 8, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id', null, null, 'xml_log', $this->currentSpoId , 'empty city', null);
                }
            }
            $this->eachTour['city'] = isset( $this->region[$node['node']['attributes']['CITY']]) ?  $this->region[$node['node']['attributes']['CITY']] : null;

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {

                    $this->tblTour->insertOrUpdate('xml_demaxtravel_hotel', array(array(
                            'id'      => $node['node']['attributes']['HTC'],
                            'name'    => $node['node']['attributes']['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[$node['node']['attributes']['CITY']],
                            'country' => $this->eachTour['country']
                        )
                        ), true
                    );

                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 9, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id', null, null, 'xml_log', $this->currentSpoId, 'empty hotel', null);
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC'] > 0)) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_room', array(array('id' => trim($node['node']['attributes']['RMC']), 'name' => trim($node['node']['attributes']['ROOM']))), true);
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 10, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id' , null, null, 'xml_log', $this->currentSpoId, 'empty room', null);
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC'] > 0)) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_stay_type', array(array('id' => trim($node['node']['attributes']['PLC']), 'name' => trim($node['node']['attributes']['PLACE']))), true);
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 11, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id' , null, null, 'xml_log', $this->currentSpoId, 'empty stayType', null);
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_pansion', array(array('id' => trim($node['node']['attributes']['MLC']), 'name' => trim($node['node']['attributes']['MEAL']))), true);
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    //$this->insertLog('warning', CRON_PARSE, XML_DEMAXTRAVEL, 12, $this->currentSpoId);
                    #$this->insertLog('xml_pegas_error', 'no id', $node['node']['attributes']['MLC'], null, 'xml_log', $this->currentSpoId, 'empty pansion', null);
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node['node']['attributes']['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_region', array(array('name' => trim($node['node']['attributes']['CITYFR']), 'country_id' => null)), true);
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    #$this->insertLog('xml_idrisko_error', 'no transport', null, null, 'xml_log', $this->currentSpoId, 'No such city from', 'tour count ' . $this->countTour);
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node['node']['attributes']['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) > 0) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_region', array(array('name' => trim($node['node']['attributes']['CITYTO']), 'country_id' => null)), true);
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) == 0) {
                    #$this->insertLog('xml_idrisko_error', 'no transport', null, null, 'xml_log', $this->currentSpoId, 'No such city to', 'tour count ' . $this->countTour);
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node['node']['attributes']['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    $this->tblTour->insertOrUpdate('xml_demaxtravel_region', array(array('name' => trim($node['node']['attributes']['CITYFR']), 'country_id' => null)), true);
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    #$this->insertLog('xml_idrisko_error', 'no transport', null, null, 'xml_log', $this->currentSpoId , 'No such city depatured', 'tour count ' . $this->countTour);
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // сортировка по дате и актуальности туров
            if (strtotime($node['node']['attributes']['DATE']) > time() && $this->errorTour === false) {

                $this->eachTour['dateStart'] = substr($node['node']['attributes']['DATE'], 0 , 4) . '-' . substr($node['node']['attributes']['DATE'], -4 , 2) . '-' . substr($node['node']['attributes']['DATE'], -2 , 2);
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d",strtotime($this->eachTour['dateStart'])+86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('DEMAXTRAVEL');
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек по 50 туров
                $this->saveTours();
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node);

        // Файл не докачан
        if (isset($node['error'])) {
            //$this->insertLog('error', CRON_PARSE, XML_DEMAXTRAVEL, 13, $this->currentSpoId);
            #$this->insertLog('xml_pegas_error', 'not load', null ,null, 'xml_log', $this->currentSpoId , null, null);
            $this->notLoad = true;
        }
    }
}
