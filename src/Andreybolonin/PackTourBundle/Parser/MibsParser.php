<?php
/*
 * TourML
 */

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\Db;
use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;
use Symfony\Component\DomCrawler\Crawler;

class MibsParser extends Xmlpullreader
{

    protected $url = 'http://online.mibstravel.ua/PriceList/files/';
    protected $fileDom = 'http://online.mibstravel.ua';
    public $xml_read_keys = array(
        'flight',
        'variant',
        'hotelService',
        'flightService',
        'transferService',
        'extraService',
        'serviceSet',
        'price',
        'date',
        'tour',
        'spo'
    );

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $crawler = new Crawler($html, $this->url);
        $files = $crawler->filter('a');

        foreach ($files as $el) {
            if (!Xmlpullreader::checkExtension($el->getAttribute('href'))) {
                continue;
            }
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $this->fileDom . $el->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $this->fileDom . $el->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * Считывание справочников в переменные класса
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('MIBS');
//
//        if ($allCatalog === true || $curentCatalog == 'room') {
//            $this->roomIDs = array();
////            $room = Spo::get_type_list(array(), self::$table . 'room_ids');
//            $room = RoomIds::findAllByOperator($operator_id);
//            foreach ($room as $val) {
//                $this->roomIDs[$val['id']] = $val;
//            }
//
//            $this->roomAccomodation = array();
////            $roomAccomod = Spo::get_type_list(array(), self::$table . 'room_accomodations');
//            $roomAccomod = RoomAccomodations::findAllByOperator($operator_id);
//            foreach ($roomAccomod as $val) {
//                $this->roomAccomodation[$val['id']] = $val;
//            }
//
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'country') {
//            $this->country = array();
////            $country = Spo::get_type_list(array('id', 'name'), self::$table . 'country');
//            $country = Country::findAllByOperator($operator_id);
//            foreach ($country as $valCntr) {
//                $this->country[$valCntr['id']] = $valCntr['name'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'city') {
//            $this->city = array();
////            $city = Spo::get_type_list(array('id', 'name'), self::$table . 'city');
//            $city = City::findAllByOperator($operator_id);
//            foreach ($city as $valCity) {
//                $this->city[$valCity['id']] = $valCity['name'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'building' || $curentCatalog == 'hotel') {
//            $this->hotels = array();
////            $hotel = Spo::get_type_list(array(), self::$table . 'hotel');
//            $hotel = Hotel::findAllByOperator($operator_id);
//            foreach ($hotel as $val) {
//                $this->hotels[$val['id']] = $val;
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'pansion') {
//            $this->pansions = array();
////            $pansions = Spo::get_type_list(array(), self::$table . 'pansion');
//            $pansions = Pansion::findAllByOperator($operator_id);
//            foreach ($pansions as $val) {
//                $this->pansions[$val['id']] = $val;
//            }
//        }
//    }

    /**
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('MIBS');

        if ($node['name'] == 'flight') {
            $this->flight[$node['node']['attributes']['key']] = array(
                'from' => $node['node']['attributes']['fromCityKey'],
                'to'   => $node['node']['attributes']['toCityKey']
            );
        }

        if ($node['name'] == 'tour') {
            $this->spoName = $node['node']['attributes']['name'];
        }

        if ($node['name'] == 'spo') {
            $currency = $this->selectCurrency($node);
            $this->tempTourOne['currency'] = $currency;
            $this->currentSpo = $node['node']['attributes']['key'];
        }

        if ($node['name'] == 'hotelService') {
            $this->hotelService = array();
            $this->hotelService['pansion'] = $node['node']['attributes']['mealKey'];
            $this->hotelService['roomKey'] = $node['node']['attributes']['roomKey'];
            $this->hotelService['buildingKey'] = $node['node']['attributes']['buildingKey'];
        }

        if ($node['name'] == 'variant' && !empty($this->hotelService)) {
            $variantId = trim(str_replace('_', '', $node['node']['attributes']['id']));
            $this->hotelServiceVariant[$variantId]['nightCount'] = $node['node']['attributes']['nights'];
            $this->hotelServiceVariant[$variantId]['pansion'] = $this->hotelService['pansion'];
            $this->hotelServiceVariant[$variantId]['roomKey'] = $this->hotelService['roomKey'];
            $this->hotelServiceVariant[$variantId]['buildingKey'] = $this->hotelService['buildingKey'];
            unset($variantId);
        } elseif ($node['name'] == 'flightService') {
            $this->hotelService = array();
        }

        if ($node['name'] == 'flightService') {
            $this->flightService = array();
            $this->flightService['flightKey'] = $node['node']['attributes']['flightKey'];
        }

        if ($node['name'] == 'variant' && !empty($this->flightService)) {
            $variantId = trim(str_replace('_', '', $node['node']['attributes']['id']));
            if ($node['node']['attributes']['dayBeg'] == 1) {
                $this->flightServiceVariant[$variantId]['flightKeyFrom'] = $this->flightService['flightKey'];
            } else {
                $this->flightServiceVariant[$variantId]['flightKeyTo'] = $this->flightService['flightKey'];
            }
            unset($variantId);
        } elseif ($node['name'] == 'transferService') {
            $this->flightService = array();
            $this->hotelService = array();
        }

        if ($node['name'] == 'serviceSet') {
            $ids = explode(' ', trim(str_replace('_', '', $node['node']['attributes']['ids'])));

            foreach ($ids as $idService) {
                if (isset($this->hotelServiceVariant[$idService])) {
                    $this->tempTourOne['nightCount'] = $this->hotelServiceVariant[$idService]['nightCount'];

                    // pansion ID
                    if (!isset($this->pansions[$this->hotelServiceVariant[$idService]['pansion']])) {
                        $this->getCatalog('boarding');
                    }
                    if (!isset($this->pansions[$this->hotelServiceVariant[$idService]['pansion']])) {
                        $this->noIdInCatalog = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::NO_HOTEL_PANSION, $this->currentSpoId);
                    } else {
                        $this->tempTourOne['pansion'] = $this->hotelServiceVariant[$idService]['pansion'];
                    }

                    // room ID
                    if (!isset($this->roomIDs[$this->hotelServiceVariant[$idService]['roomKey']])) {
                        $this->getCatalog('room');
                    }
                    if (!isset($this->roomIDs[$this->hotelServiceVariant[$idService]['roomKey']])) {
                        $this->noIdInCatalog = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::NO_ROOM_KEY, $this->currentSpoId);
                    } else {
                        $this->tempTourOne['stayType'] = $this->roomIDs[$this->hotelServiceVariant[$idService]['roomKey']]['roomType'];
                        $this->tempTourOne['room'] = $this->roomIDs[$this->hotelServiceVariant[$idService]['roomKey']]['roomDesc'];
                        $this->tempTourOne['adult'] = $this->roomAccomodation[$this->roomIDs[$this->hotelServiceVariant[$idService]['roomKey']]['roomAccom']]['adult'];
                        $this->tempTourOne['children'] = $this->roomAccomodation[$this->roomIDs[$this->hotelServiceVariant[$idService]['roomKey']]['roomAccom']]['child'];
                    }

                    // hotel
                    if (!isset($this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']])) {
                        $this->getCatalog('hotel');
                        $this->getCatalog('building');
                        $this->getCatalog('category');
                    }
                    if (!isset($this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']])) {
                        $this->noIdInCatalog = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::NO_BUILDING_KEY, $this->currentSpoId);
                    } else {
                        $this->tempTourOne['star'] = $this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['star'];
                        $this->tempTourOne['hotel'] = $this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['id'];

                        if (!$this->country[$this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['country']]) {
                            $this->getCatalog('country');
                        }
                        if (!$this->country[$this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['country']]) {
                            $this->noIdInCatalog = true;
                            Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::ERROR_XML, $this->currentSpoId);
                        }

                        $this->tempTourOne['country'] = $this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['country'];

                        if (!$this->city[$this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['city']]) {
                            $this->getCatalog('city');
                        }
                        if (!$this->city[$this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['city']]) {
                            $this->noIdInCatalog = true;
                            Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::ERROR_XML, $this->currentSpoId);
                        }
                        $this->tempTourOne['city'] = $this->hotels[$this->hotelServiceVariant[$idService]['buildingKey']]['city'];
                    }
                }

                // Города и регионы вылетов и прилетов
                if (isset($this->flightServiceVariant[$idService])) {
                    // Город вылета и прилета
                    if (array_key_exists('flightKeyFrom', $this->flightServiceVariant[$idService])) {
                        $this->tempTourOne['cityDepatured'] = $this->flight[$this->flightServiceVariant[$idService]['flightKeyFrom']]['from'];
//                        $this->tempTourOne['arrivalRegion'] = $this->flight[$this->flightServiceVariant[$idService]['flightKeyFrom']]['to'];
                    }

                    if (array_key_exists('flightKeyTo', $this->flightServiceVariant[$idService])) {
//                        $this->tempTourOne['departuredRegion'] = $this->flight[$this->flightServiceVariant[$idService]['flightKeyTo']]['from'];
                    }
                }
            }
            unset($ids);
        }

        if ($node['name'] == 'price') {
            $this->tempTourOne['price'] = $node['node']['attributes']['gross'];
        }

        if ($node['name'] == 'date') {
            if (strtotime($node['node']['attributes']['from']) > time() &&
                strtotime($node['node']['attributes']['from']) < Registry::get('parser_start_date') &&
                $this->noIdInCatalog === false
            ) {
                $this->tempTourOne['dateStart'] = $node['node']['attributes']['from'];
                $this->tempTourOne['dateEnd'] = date("Y-m-d", strtotime($this->tempTourOne['dateStart']) + 86400*$this->tempTourOne["nightCount"]);
                $this->tempTourOne['tour'] = $node['node']['attributes']['key'];
                $this->tempTourOne['update'] = 1;
                $this->tempTourOne['tourOperator'] = Operator::getOperatorId('MIBS');
                $this->tempTourOne['infant'] = null;
                $this->tempTourOne['spo'] = $this->currentSpoAuto;
                $this->eachFiftiesTour[$this->countIdService] =  $this->tempTourOne;
                $this->countIdService++;

                //Стек туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

    /**
     * Добавляет несуществующую в справочнике позицию - в hotel, room, city, country ...
     *
     * @param  null $type
     * @return bool
     */
    public function getCatalog($type = null)
    {
        $tmpList = new MibsParserList();
        $tmpList->listType = $type;

        if ($tmpList->readXmlByUrl($this->curentSpoFile) === false) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::ERROR_XML, $this->listType);

            return false;
        }

        if ($type == 'room') {
            $arraySetReadBanch = array('room', 'roomType', 'roomDescription', 'roomCategory', 'roomAccomodation');
            $arraysetReadBanchOnlyAttr = array('room' => true, 'roomType' => true, 'roomDescription' => true, 'roomCategory' => true);
            $functionHandler = 'parseListRoom';
        } else {
            $arraySetReadBanch = array($type);
            $arraysetReadBanchOnlyAttr = array($type => true);
            $functionHandler = 'parseList';
        }

        $tmpList->setReadBanch($arraySetReadBanch);
        $tmpList->setFunctionHandler(array($tmpList, $functionHandler));
        $tmpList->setReadBanchOnlyAttr($arraysetReadBanchOnlyAttr);
        $tmpList->getXmlAssoc();
        $this->initializeArray(false, $type);
    }

    /**
     * Выбор валюты
     *
     * @param $node
     * @return null|string
     */
    public function selectCurrency($node)
    {
        $currency = null;
        switch ($node['node']['attributes']['currencyKey']) {
            case 1:
                $currency = 'USD';
                break;
            case 2:
                $currency = 'RUB';
                break;
            case 3:
                $currency = 'EUR';
                break;
            case 113:
                $currency = 'UAH';
                break;
            case 115:
                $currency = 'CHF';
                break;
        }

        return $currency;
    }
}

/*
 *  Класс для парсинга справочников
 */
class MibsParserList extends MibsParser
{
    public $listType = null;

    /**
     * @param $node
     */
    public function parseList($node)
    {
        if (isset($this->listType) && $node['name'] != 'eof') {
            switch ($this->listType) {
                // Страна
                case 'country':
                    if (!isset($node['node']['attributes']['fake'])) {
                        $this->table = self::$table . 'country';
                        $this->eachElement['id'] = $node['node']['attributes']['key'];
                        $this->eachElement['name'] = $node['node']['attributes']['name'];
                        $this->catalogArray[] = $this->eachElement;
                    }
                    break;
                // Город
                case 'city':
                    if (!isset($node['node']['attributes']['fake'])) {
                        $this->table = self::$table . 'city';
                        $this->eachElement['id'] = $node['node']['attributes']['key'];
                        $this->eachElement['name'] = $node['node']['attributes']['name'];
                        //$this->eachElement['code'] = $node['node']['attributes']['code'];
                        $this->eachElement['country_id'] = $node['node']['attributes']['countryKey'];
                        $this->catalogArray[] = $this->eachElement;
                    }
                    break;
                // Звездность
                case 'category':
                    $this->table = self::$table . 'star';
                    $this->eachElement['id'] = $node['node']['attributes']['key'];
                    $this->eachElement['name'] = trim(str_replace('*', '', $node['node']['attributes']['name']));
                    $this->catalogArray[] = $this->eachElement;
                    break;
                // Отель
                case 'hotel':
                    $this->table = self::$table . 'hotel';
                    if ($node['name'] == 'hotel') {
                        $this->eachElement['id'] = $node['node']['attributes']['key'];
                        $this->eachElement['name'] = addslashes($node['node']['attributes']['name']);
                        $this->eachElement['country'] = $node['node']['attributes']['countryKey'];
                        $this->eachElement['city'] = $node['node']['attributes']['cityKey'];
                        $this->catalogArray[] = $this->eachElement;
                    }
                    break;
                case 'building':
                    $this->table = self::$table . 'hotel';
                    if ($node['name'] == 'building') {
                        $this->eachElement['id'] = $node['node']['attributes']['hotelKey'];
                        $this->eachElement['star'] = $node['node']['attributes']['categoryKey'];
                        $this->catalogArray[] = $this->eachElement;
                    }
                    break;
                // Питание
                case 'boarding':
                    $this->table = self::$table . 'pansion';
                    $this->eachElement['id'] = $node['node']['attributes']['key'];
                    $this->eachElement['code'] = $node['node']['attributes']['code'];
                    $this->eachElement['name'] = $node['node']['attributes']['name'];
                    $this->catalogArray[] = $this->eachElement;
                    break;
            }
        }

        if (count($this->catalogArray) == 50) {
            db::insertOrUpdateMulty($this->catalogArray, $this->table);
            $this->catalogArray = array();
        }

        if ($node['name'] == 'eof' && !empty($this->catalogArray)) {
            db::insertOrUpdateMulty($this->catalogArray, $this->table);
            unset($this->listType);
            unset($this->catalogArray);
            unset($this->table);
            unset($this->eachElement);
        }

        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::ERROR_XML, $this->currentSpoId);
        }
    }

    /**
     * @param $node
     */
    public function parseListRoom($node)
    {
        if (isset($this->listType) && $node['name'] != 'eof') {
            // Размещение в номере
            if ($node['name'] == 'roomType') {
                $this->roomTypeId = $node['node']['attributes']['key'];
                $this->room['stay_type'][$this->roomTypeId] = array(
                    'id'   => $this->roomTypeId,
                    'name' => $node['node']['attributes']['name'],
                );
            }

            // Тип комнаты
            if ($node['name'] == 'roomCategory') {
                $this->roomCategoryId = $node['node']['attributes']['key'];
                $this->room['room'][$this->roomCategoryId] = array(
                    'id'   => $this->roomCategoryId,
                    'name' => $node['node']['attributes']['name'],
                );
            }

            // Вспомогательная инфа
            if ($node['name'] == 'roomDescription') {
                $this->roomDescriptionId = $node['node']['attributes']['catKey'];
                $this->room['description'][$this->roomDescriptionId] = array(
                    'id'     => $this->roomDescriptionId,
                    'catKey' => $node['node']['attributes']['catKey'],
                );
            }

            // Количество взрослых детей в номере
            if ($node['name'] == 'roomAccomodation') {
                $this->roomAccomodationId = $node['node']['attributes']['key'];
                $men = explode('+', str_replace(' ', '', $node['node']['attributes']['name']));

                foreach ($men as $one) {
                    if (strpos ($one, 'Adult') !== false) {
                        $adult = $one{0};
                    }
                    if (strpos ($one, 'Child') !== false) {
                        $child = $one{0};
                    }
                }
                $this->room['room_accomodations'][$this->roomAccomodationId] = array(
                    'id'    => $this->roomAccomodationId,
                    'name'  => $node['node']['attributes']['name'],
                    'code'  => $node['node']['attributes']['code'],
                    'adult' => isset($adult) ? $adult : null,
                    'child' => isset($child) ? $child : null
                );
            }

            // Вспомогательная инфа
            if ($node['name'] == 'room') {
                $this->roomId = $node['node']['attributes']['key'];
                $this->room['room_ids'][$this->roomId] = array(
                    'id'        => $node['node']['attributes']['key'],
                    'roomType'  => $node['node']['attributes']['typeKey'],
                    'roomDesc'  => $node['node']['attributes']['roomDescKey'],
                    'roomAccom' => $node['node']['attributes']['accomodationKey']
                );
            }
        }

        if ($node['name'] == 'eof') {
            // Проверяем наличие елементов в справочнике
            foreach ($this->room['room_ids'] as $room) {
                if (!isset($this->room['stay_type'][$room['roomType']])) {
                    // to log
//                    $this->insertLog('warning', CRON_PARSE, XML_MIBS, 11, $room['roomType'] . ' нет в справочнике');
                }
                if (!isset($this->room['description'][$room['roomDesc']])) {
                    // to log
//                    $this->insertLog('warning', CRON_PARSE, XML_MIBS, 25, $room['roomDesc'] . ' нет в справочнике');
                }
                if (!isset($this->room['room_accomodations'][$room['roomAccom']])) {
                    // to log
//                    $this->insertLog('warning', CRON_PARSE, XML_MIBS, 26, $room['roomAccom'] . ' нет в справочнике');
                }
            }

            foreach ($this->room['description'] as $description) {
                $this->room['room'][$description['catKey']]['description'] = $description['id'];
            }
            unset($this->room['description']);

            foreach ($this->room as $table => $value) {
                db::insertOrUpdateMulty($value, self::$table . $table);
            }

            unset($this->listType);
            unset($this->roomTypeId);
            unset($this->roomCategoryId);
            unset($this->roomDescriptionId);
            unset($this->roomAccomodationId);
            unset($this->roomId);
            unset($this->room);
        }

        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('MIBS'), Log::ERROR_XML, $this->currentSpoId);
        }
    }
}
