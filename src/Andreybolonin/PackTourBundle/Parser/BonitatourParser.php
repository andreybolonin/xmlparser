<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Finder\Finder;

class BonitatourParser extends XmlPullReader
{

    private $ftp_server = '91.222.36.123';
    private $ftp_user = 'turneua';
    private $ftp_password = 'Bonita12345';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');
    // Справочники - ftp://91.222.36.123/guides.xml

    /**
     * Получает список SPO и сохраняет их в бд
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        $finder = new Finder();
        $files = $finder->files()
            ->name('*.xml')
            ->name('*.zip')
            ->name('*.rar')
            ->name('*.gzip')
            ->in('ftp://' . $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . '/');
        $spo_array = array();

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getPathname() . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $file->getPathname(),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * Парсинг файла
     *
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        if (isset($node['node']['attributes'])) {
            $node_attributes = $node['node']['attributes'];
        }
        $operator_id = $this->spo['operator'];

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            if (isset($node_attributes['SPO'])) {
                $this->spoName = $node_attributes['SPO'];
                $this->currentSpo = $node_attributes['SPO'];
            }

            // country
            if (strpos($node_attributes['COUNTRY'], ',') === false) {
                if (!isset($this->countrySelf[$node_attributes['COUNTRY']])) {
                    // Проверяем есть ли в общем каталоге
                    if (strlen($node_attributes['COUNTRY']) > 0) {
                        if (!isset($this->country[$node_attributes['COUNTRY']])) {
                            $this->addCountry(array(
                                'code' => trim($node_attributes['COUNTRY']),
                                'name' => trim($node_attributes['COUNTRY'])
                            ));
                        } else {
                            $this->addCountry(array(
                                'code' => trim($node_attributes['COUNTRY']),
                                'name' => $this->country[$node_attributes['COUNTRY']]['name']
                            ));
                        }
                    } else {
                        $this->errorCountry();
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$node_attributes['COUNTRY']]) ? $this->countrySelf[$node_attributes['COUNTRY']] : null;
            } else {
                $countryArray = explode(',', $node_attributes['COUNTRY']);
                if (!isset($this->countrySelf[$countryArray[1]])) {
                    if (!isset($this->country[$node_attributes['COUNTRY']])) {
                        $this->addCountry(array(
                            'code' => trim($countryArray[1]),
                            'name' => trim($countryArray[1]),
                        ));
                    } else {
                        $this->addCountry(array(
                            'code' => trim($countryArray[1]),
                            'name' => $this->country[$countryArray[1]]['name']
                        ));
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$countryArray[1]]) ? $this->countrySelf[$countryArray[1]] : null;
            }

            if (!isset($this->tourtype[$node_attributes['TOURTYPE']])) {
                $this->addTourType(array('name' => trim($node_attributes['TOURTYPE'])));
            }

            $this->tour_type = $this->tourtype[$node_attributes['TOURTYPE']] ? $this->tourtype[$node_attributes['TOURTYPE']] : 0;
            $this->eachTour['adult'] = $node_attributes['ADL'];
            $this->eachTour['children'] = $node_attributes['CHD'];
            $this->eachTour['infant'] = $node_attributes['INF'];
            $this->eachTour['currency'] = $node_attributes['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {

            // star
            if (isset($node_attributes['STAR'])) {
                $hotelStar = strlen($node_attributes['STAR']);
                if (!isset($this->hotelStar[$hotelStar])) {
                    $this->addStar(array('star' => $hotelStar));
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // city
            if (!isset($this->region[$node_attributes['CITY']]) && $this->errorTour === false) {
                if (strlen($node_attributes['CITY']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node_attributes['CITY']),
                        'country_id' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorCity();
                }
            }
            $this->eachTour['city'] = $this->region[$node_attributes['CITY']];

            // hotel
            if (!isset($this->hotel[ $node_attributes['HTC']]) && $this->errorTour === false) {
                if (strlen($node_attributes['HTC'] > 0)) {
                    $this->addHotel(array(
                        'id'      => $node_attributes['HTC'],
                        'name'    => $node_attributes['NAME'],
                        'star'    => $this->hotelStar[$hotelStar],
                        'city'    => $this->region[$node_attributes['CITY']],
                        'country' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorHotel();
                }
            }
            $this->eachTour['hotel'] = $node_attributes['HTC'];

            // room
            if (!isset($this->roomIds[$node_attributes['RMC']])) {
                if (strlen($node_attributes['RMC'] > 0)) {
                    $this->addRoom(array(
                        'id' => trim($node_attributes['RMC']),
                        'name' => trim($node_attributes['ROOM'])
                    ));

                } else {
                    $this->errorRoom();
                }
            }
            $this->eachTour['room'] = $node_attributes['RMC'];

            // stay_type
            if (!isset($this->stayTypeIds[$node_attributes['PLC']])) {
                if (strlen($node_attributes['PLC'] > 0)) {
                    $this->addStayType(array(
                        'id' => trim($node_attributes['PLC']),
                        'name' => trim($node_attributes['PLACE'])
                    ));
                } else {
                    $this->errorStayType();
                }
            }
            $this->eachTour['stayType'] = $node_attributes['PLC'];

            // pansion
            if (!isset($this->pansionIds[$node_attributes['MLC']])) {
                if (strlen($node_attributes['MLC']) > 0 && strlen($node_attributes['MEAL']) > 0) {
                    $this->addPansion(array(
                        'id' => trim($node_attributes['MLC']),
                        'name' => trim($node_attributes['MEAL'])
                    ));
                } else {
                    $this->errorPansion();
                }
            }
            $this->eachTour['pansion'] = $node_attributes['MLC'];
        }

//        if ($node['name'] == 'TRANSPORT') {
//            if ($node_attributes['NAME'] == 'FLIGHT') {
//
//                if (isset($this->region[$node_attributes['CITYFR']])) {
//                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['CITYFR']];
//                } elseif (strlen($node_attributes['CITYFR']) > 0) {
//                    City::addCity(
//                        array(
//                            'name' => trim($node_attributes['CITYFR']),
//                            'country_id' => null
//                        ),
//                        $operator_id
//                    );
//                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['cityDepatured'] = $this->region[$node_attributes['CITYFR']];
//                } elseif (strlen($node_attributes['CITYFR']) == 0) {
//                    $this->eachTour['cityDepatured'] = null;
//                }
//
//                if (isset($this->region[$node_attributes['CITYTO']])) {
////                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
//                } elseif (strlen($node_attributes['CITYTO']) > 0) {
//                    City::addCity(
//                        array(
//                            'name' => trim($node_attributes['CITYTO']),
//                            'country_id' => null
//                        ),
//                        $operator_id
//                    );
//                    $this->initializeArray(false, 'initRegion');
////                    $this->eachTour['arrivalRegion'] = $this->region[$node_attributes['CITYTO']];
//                } elseif (strlen($node_attributes['CITYTO']) == 0) {
//                    $this->eachTour['cityDepatured'] = null;
//                }
//
//            } elseif ($node_attributes['NAME'] == 'BACK FLIGHT') {
//
//                if (isset($this->region[$node_attributes['CITYFR']])) {
////                    $this->eachTour['departuredRegion'] = $this->region[$node_attributes['CITYFR']];
//                } elseif (strlen($node_attributes['CITYFR']) > 0) {
//                    City::addCity(
//                        array(
//                            'name' => trim($node_attributes['CITYFR']),
//                            'country_id' => null
//                        ),
//                        $operator_id
//                    );
//                    $this->initializeArray(false, 'initRegion');
////                    $this->eachTour['departuredRegion'] = $this->region[$node_attributes['CITYFR']];
//                } elseif (strlen($node_attributes['CITYFR']) == 0) {
////                    $this->eachTour['departuredRegion'] = null;
//                }
//            }
//        } else {
//            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
////            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
////            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
//        }

        if ($node['name'] == 'PRICE') {

            // Сортировка по дате и актуальности туров
            if (strtotime($node_attributes['DATE']) > time() &&
//                strtotime($node_attributes['DATE']) < $this->container->getParameter('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node_attributes['DATE'];
                $this->eachTour['price'] = $node_attributes['VAL'];
                $this->eachTour['nightCount'] = $node_attributes['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart'])+86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['updated'] = 1;
                $this->eachTour['tourOperator'] = $operator_id;
                $this->eachFiftiesTour[] = $this->eachTour;

                // Запись туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);
    }

}
