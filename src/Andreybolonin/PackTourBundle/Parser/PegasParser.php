<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\XmlPullReader;
use Buzz\Browser;
use Symfony\Component\DomCrawler\Crawler;

class PegasParser extends Xmlpullreader
{

    private $url = 'http://www.pegast.com.ua/xml/';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return bool
     */
    public function getAndSaveSpoList($operator_id)
    {
        try {
            $browser = new Browser();
            $html = $browser->get($this->url)->getContent();
        } catch (\Exception $e) {
            echo 'Выброшено исключение: ', $e->getMessage(), "\n";

            return false;
        }

        $spo_array = array();
        $crawler = new Crawler($html, $this->url);
        $files = $crawler->filter('a');

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $this->url . $file->getAttribute('href') . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $this->url . $file->getAttribute('href'),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('PEGAS');

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->spoId;
            $this->currentSpo = $node['node']['attributes']['SPO'];

            // В теге одна страна
            if (strpos($node['node']['attributes']['COUNTRY'], ',') === false) {
                if (!isset($this->countrySelf[$node['node']['attributes']['COUNTRY']])) {
                    // не пустая проверяем есть ли в ощем каталоге
                    if (strlen($node['node']['attributes']['COUNTRY']) > 0) {
                        Country::addCountry(
                            array(
                                'code' => trim($node['node']['attributes']['COUNTRY']),
                                'name' => trim($node['node']['attributes']['COUNTRY'])
                            ),
                            $operator_id
                        );
                        $this->initializeArray(false, 'initCountry');
                    } else {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::EMPTY_COUNTRY_TAG, $this->currentSpoId);
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$node['node']['attributes']['COUNTRY']]) ? $this->countrySelf[$node['node']['attributes']['COUNTRY']] : null;
            } else {
                $countryArray = explode(',', $node['node']['attributes']['COUNTRY']);
                if (!isset($this->countrySelf[$countryArray[1]])) {
                    Country::addCountry(
                        array(
                            'code' => trim($countryArray[1]),
                            'name' => trim($countryArray[1]),
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initCountry');
                }
                $this->eachTour['country'] = isset($this->countrySelf[$countryArray[1]]) ? $this->countrySelf[$countryArray[1]] : null;
            }

            if (!isset($this->tourtype[$node['node']['attributes']['TOURTYPE']])) {
                TourType::addTourType(
                    array('name' => trim($node['node']['attributes']['TOURTYPE'])),
                    $operator_id
                );
                $this->initializeArray(true, 'initTourType');
            }

            $this->eachTour['adult'] = $node['node']['attributes']['ADL'];
            $this->eachTour['children'] = $node['node']['attributes']['CHD'];
            $this->eachTour['infant'] = $node['node']['attributes']['INF'];
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    Star::addStar(
                        array('star' => $hotelStar),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node['node']['attributes']['CITY']]) && $this->errorTour === false) {
                if ( strlen($node['node']['attributes']['CITY']) > 0 ) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITY']),
                            'country_id' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::EMPTY_CITY, $this->currentSpoId);
                }
            }
            $this->eachTour['city'] = isset( $this->region[$node['node']['attributes']['CITY']]) ?  $this->region[$node['node']['attributes']['CITY']] : null;

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {
                    Hotel::addHotel(
                        array(
                            'id'      => $node['node']['attributes']['HTC'],
                            'name'    => $node['node']['attributes']['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[$node['node']['attributes']['CITY']],
                            'country' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::EMPTY_HOTEL, $this->currentSpoId);
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC'] > 0)) {
                    Room::addRoom(
                        array(
                            'id' => trim($node['node']['attributes']['RMC']),
                            'name' => trim($node['node']['attributes']['ROOM'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::EMPTY_ROOM, $this->currentSpoId);
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC'] > 0)) {
                    StayType::addStayType(
                        array(
                            'id' => trim($node['node']['attributes']['PLC']),
                            'name' => trim($node['node']['attributes']['PLACE'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::EMPTY_STAY_TYPE, $this->currentSpoId);
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    Pansion::addPansion(
                        array(
                            'id' => trim($node['node']['attributes']['MLC']),
                            'name' => trim($node['node']['attributes']['MEAL'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::EMPTY_PANSION, $this->currentSpoId);
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node['node']['attributes']['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node['node']['attributes']['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYTO']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node['node']['attributes']['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // Cортировка по дате и актуальности туров
            if (strtotime($node['node']['attributes']['DATE']) > time() &&
                strtotime($node['node']['attributes']['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {

                $this->eachTour['dateStart'] = $node['node']['attributes']['DATE'];//substr($node['node']['attributes']['DATE'], 0 , 4) . '-' . substr($node['node']['attributes']['DATE'], -4 , 2) . '-' . substr($node['node']['attributes']['DATE'], -2 , 2);
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('PEGAS');
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('PEGAS'), Log::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

    /**
     * Наполняет глобальные переменные данными из XML файла
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('PEGAS');
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
////            $aCountry = Spo::getIsoCountry(array('id', 'shortName'));
//            $aCountry = IsoCountry::getIsoCountries();
//            foreach ($aCountry as $val) {
//                $this->country[$val['shortName']] = $val['id'];
//            }
//
////            $aCountrySelf = Spo::get_type_list(array('id', 'code') , self::$table . 'country');
//            $aCountrySelf = Country::findAllByOperator($operator_id);
//            foreach ($aCountrySelf as $val) {
//                $this->countrySelf[$val['code']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = Hotel::findAllByOperator($operator_id);
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = Star::findAllByOperator($operator_id);
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = City::findAllByOperator($operator_id);
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog == 'all' || $curentCatalog == 'initRoom') {
//            if ($allCatalog === true || $curentCatalog == 'initRoom') {
//                $aRoom = Room::findAllByOperator($operator_id);
//                foreach ($aRoom as $val) {
//                    $this->roomIds[$val['id']] = '';
//                }
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = StayType::findAllByOperator($operator_id);
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//    }

}
