<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\Finder\Finder;

class CamParser extends XmlPullReader
{

    private $ftp_server = '77.90.196.58';
    private $ftp_user = 'anonymous';
    private $ftp_password = '';
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их
     *
     * @param $operator_id
     * @return mixed|void
     */
    public function getAndSaveSpoList($operator_id)
    {
        $finder = new Finder();
        $files = $finder->files()
            ->name('*.xml')
            ->name('*.zip')
            ->name('*.rar')
            ->name('*.gzip')
            ->in('ftp://' . $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . '/');

        $spo_array = array();

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getPathname() . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $file->getPathname(),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

    /**
     * Получаем из БД список параметра тура(country, hotel, hotelstar) и заменяем значение из XML на ключ в БД если он есть, если его нет то добавляем.
     * Либо не заменяем если у значения есть какой-то ключ оператора.
     *
     * @param  bool       $allCatalog
     * @param  null       $curentCatalog
     * @return mixed|void
     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = $this->spo['operator'];
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
//            $aCountry = $this->pdo_backend->query('SELECT * FROM xml_tour.iso_country')->fetchAll();
//            foreach ($aCountry as $val) {
//                $this->country[$val['shortName']] = $val['id'];
//            }
//
//            $aCountrySelf = $this->pdo_master->query('SELECT * FROM pack_tours.country_' . $operator_id)->fetchAll();
//            foreach ($aCountrySelf as $val) {
//                $this->countrySelf[$val['shortName']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = $this->pdo_master->query('SELECT * FROM pack_tours.hotel_' . $operator_id)->fetchAll();
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = $this->pdo_master->query('SELECT * FROM pack_tours.star_' . $operator_id)->fetchAll();
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = $this->pdo_master->query('SELECT * FROM pack_tours.city_' . $operator_id)->fetchAll();
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRoom') {
////            $aRoom = Room::findAllByOperator($operator_id);
//            $aRoom = $this->pdo_master->query('SELECT * FROM pack_tours.room_' . $operator_id)->fetchAll();
//            foreach ($aRoom as $val) {
//                $this->roomIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = $this->pdo_master->query('SELECT * FROM pack_tours.stay_type_' . $operator_id)->fetchAll();
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//    }

    /**
     * Парсинг XML файла - разбор значений туров: OFFER, HOTEL, TRANSPORT
     *
     * @param $node
     * @return mixed|void
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = $this->spo['operator'];

        if (isset($node['name']) && $node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;

            // country
            if (strpos($node['node']['attributes']['COUNTRY'], ',') === false) {
                if (!isset($this->countrySelf[$node['node']['attributes']['COUNTRY']])) {
                    // не пустая проверяем есть ли в ощем каталоге
                    if (strlen($node['node']['attributes']['COUNTRY']) > 0) {
                        $this->addCountry(array(
                            'shortName' => trim($node['node']['attributes']['COUNTRY']),
                            'name' => trim($node['node']['attributes']['COUNTRY'])
                        ));
                    } else {
                        $this->errorCountry();
                    }
                }
                $this->eachTour['country'] = isset($this->countrySelf[$node['node']['attributes']['COUNTRY']]) ? $this->countrySelf[$node['node']['attributes']['COUNTRY']] : null;
            } else {
                $countryArray = explode(',', $node['node']['attributes']['COUNTRY']);
                if (!isset($this->countrySelf[$countryArray[1]])) {
                    $this->addCountry(array(
                        'shortName' => trim($countryArray[1]),
                        'name' => trim($countryArray[1]),
                    ));
                }
                $this->eachTour['country'] = isset($this->countrySelf[$countryArray[1]]) ? $this->countrySelf[$countryArray[1]] : null;
            }

            // tour_type
            if ($node['node']['attributes']['TOURTYPE']) {
                if (!isset($this->tourtype[$node['node']['attributes']['TOURTYPE']])) {
                    $this->addTourType(array('name' => trim($node['node']['attributes']['TOURTYPE'])));
                }
                $this->tour_type = $this->tourtype[$node['node']['attributes']['TOURTYPE']] ? $this->tourtype[$node['node']['attributes']['TOURTYPE']] : 0;
            }

            $this->eachTour['adult'] = $node['node']['attributes']['ADL'];
            $this->eachTour['children'] = $node['node']['attributes']['CHD'];
            $this->eachTour['infant'] = $node['node']['attributes']['INF'];
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if (isset($node['name']) && $node['name'] == 'HOTEL') {

            // star
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (!isset($this->hotelStar[$hotelStar])) {
                    $this->addStar(array('star' => $hotelStar));
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // city
            if (!isset($this->region[$node['node']['attributes']['CITY']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['CITY']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITY']),
                        'country_id' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorCity();
                }
            }
            $this->eachTour['city'] = $this->region[$node['node']['attributes']['CITY']]; //- notice

            // hotel
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {
                    $this->addHotel(array(
                        'id'      => $node['node']['attributes']['HTC'],
                        'name'    => $node['node']['attributes']['NAME'],
                        'star'    => $this->hotelStar[$hotelStar],
                        'city'    => $this->region[$node['node']['attributes']['CITY']],
                        'country' => $this->eachTour['country']
                    ));
                } else {
                    $this->errorHotel();
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // room
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC']) > 0 && strlen($node['node']['attributes']['ROOM']) > 0) {
                    $this->addRoom(array(
                        'id' => trim($node['node']['attributes']['RMC']),
                        'name' => trim($node['node']['attributes']['ROOM'])
                    ));
                } else {
                    $this->errorRoom();
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC']) > 0 && strlen($node['node']['attributes']['PLACE']) > 0) {
                    $this->addStayType(array(
                        'id' => trim($node['node']['attributes']['PLC']),
                        'name' => trim($node['node']['attributes']['PLACE'])
                    ));
                } else {
                    $this->errorStayType();
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    $this->addPansion(array(
                        'id' => trim($node['node']['attributes']['MLC']),
                        'name' => trim($node['node']['attributes']['MEAL'])
                    ));
                } else {
                    $this->errorPansion();
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if (isset($node['name']) && $node['name'] == 'TRANSPORT') {
            if ($node['node']['attributes']['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITYFR']),
                        'country_id' => null
                    ));
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node['node']['attributes']['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITYTO']),
                        'country_id' => null
                    ));
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node['node']['attributes']['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    $this->addCity(array(
                        'name' => trim($node['node']['attributes']['CITYFR']),
                        'country_id' => null
                    ));
//                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
//                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
//            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if (isset($node['name']) && $node['name'] == 'PRICE') {
            if (isset($node['node']['attributes']['DATE']) &&
                strtotime($node['node']['attributes']['DATE']) > time() &&
//                strtotime($node['node']['attributes']['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {
                $this->eachTour['dateStart'] = $node['node']['attributes']['DATE'];
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                if (isset($node['node']['attributes']['N'])) {
                    $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                }
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('CAM');
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);
    }

}
