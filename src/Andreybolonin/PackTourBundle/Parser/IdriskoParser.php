<?php

namespace Andreybolonin\PackTourBundle\Parser;

use Andreybolonin\PackTourBundle\PdoHelper;
use Andreybolonin\PackTourBundle\XmlPullReader;
use Symfony\Component\Finder\Finder;

class IdriskoParser extends XmlPullReader
{

    protected $ftp_server = "77.120.113.97";
    protected $ftp_user = 'sposread';
    protected $ftp_password = "Pc2RjNJhzpcu";
    public $xml_read_keys = array('OFFER', 'HOTEL', 'TRANSPORT', 'PRICE');

    /**
     * Получает список SPO и сохраняет их в таблицу list
     */
    public function getAndSaveSpoList($operator_id)
    {
        $finder = new Finder();
        $files = $finder->files()
            ->name('*.xml')
            ->name('*.zip')
            ->name('*.rar')
            ->name('*.gzip')
            ->in('ftp://' . $this->ftp_user . ':' . $this->ftp_password . '@' . $this->ftp_server . '/');

        $spo_array = array();

        foreach ($files as $file) {
            $spo = $this->pdo_backend->query('SELECT * FROM xml_tour.spo WHERE url = "' . $file->getPathname() . '"')->fetch();
            if (!$spo) {
                $spo_array[] = array(
                    'operator' => $operator_id,
                    'url' => $file->getPathname(),
                    'status' => 'nottouched'
                );
            }
        }

        PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.spo', $spo_array);
    }

//    /**
//     * Инициализация справочных данных
//     * Для обновления или загрузки одного справочника $allCatalog = FALSE , $curentCatalog = 	название каталога
//     *
//     * @param  bool       $allCatalog
//     * @param  null       $curentCatalog
//     * @return mixed|void
//     */
//    protected function initializeArray($allCatalog = true, $curentCatalog = null)
//    {
//        $operator_id = Operator::getOperatorId('IDRISKO');
//
//        if ($allCatalog === true || $curentCatalog == 'initCountry') {
////            $aCountry = Spo::getIsoCountry(array('id', 'shortName', 'shortName2'));
//            $aCountry = IsoCountry::getIsoCountries();
//            foreach ($aCountry as $val) {
//                $this->country[$val['shortName']] = $val['id'];
//                $this->country[$val['shortName2']] = $val['id'];
//            }
//            $this->country['EK'] = 147;
//            $this->country['EP'] = 67;
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotel') {
//            $aHotel = Hotel::findAllByOperator($operator_id);
//            foreach ($aHotel as $val) {
//                $this->hotel[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initHotelStar') {
//            $aHotelStar = Star::findAllByOperator($operator_id);
//            foreach ($aHotelStar as $val) {
//                $this->hotelStar[$val['star']] = $val['id'];
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initRegion') {
//            $aRegion = City::findAllByOperator($operator_id);
//            foreach ($aRegion as $val) {
//                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
//            }
//        }
//
//        if ($allCatalog == 'all' || $curentCatalog == 'initRoom') {
//            if ($allCatalog === true || $curentCatalog == 'initRoom') {
//                $aRoom = Room::findAllByOperator($operator_id);
//                foreach ($aRoom as $val) {
//                    $this->roomIds[$val['id']] = '';
//                }
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initStayType') {
//            $aStayType = StayType::findAllByOperator($operator_id);
//            foreach ($aStayType as $val) {
//                $this->stayTypeIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initPansion') {
//            $aPansion = Pansion::findAllByOperator($operator_id);
//            foreach ($aPansion as $val) {
//                $this->pansionIds[$val['id']] = '';
//            }
//        }
//
//        if ($allCatalog === true || $curentCatalog == 'initTourType') {
//            $aTourType = TourType::findAllByOperator($operator_id);
//            foreach ($aTourType as $val) {
//                $this->tourtype[$val['name']] = $val['id'];
//            }
//        }
//
////        $this->initHotel($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initHotelStar($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initCity($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initRegion($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initRoom($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initStayType($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initPansion($allCatalog, $curentCatalog, $this, self::$table);
////
////        $this->initTourType($allCatalog, $curentCatalog, $this, self::$table);
//    }

    /**
     *  Отбор туров
     *
     * @param $node
     */
    public function getAllTourFromSpo($node)
    {
        $operator_id = Operator::getOperatorId('IDRISKO');

        if ($node['name'] == 'OFFER') {
            $this->errorTour = false;
            $this->eachTour['spo'] = $this->currentSpoAuto;
            $this->spoName = $node['node']['attributes']['SPO'];

            // В теге одна страна
            if (strpos($node['node']['attributes']['COUNTRY'], ',') === false) {
                if (!isset($this->country[$node['node']['attributes']['COUNTRY']])) {
                    $this->errorTour = true;
                    if (strlen($node['node']['attributes']['COUNTRY']) > 0) {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::NO_SUCH_COUNTRY, $this->currentSpoId);
                    } else {
                        $this->errorTour = true;
                        Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::EMPTY_COUNTRY_TAG, $this->currentSpoId);
                    }
                }
                $this->eachTour['country'] = isset($this->country[$node['node']['attributes']['COUNTRY']]) ? $this->country[$node['node']['attributes']['COUNTRY']] : null;
            } else {
                $countryArray = explode(',',$node['node']['attributes']['COUNTRY']);
                if (!isset($this->country[$countryArray[1]])) {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::NO_SUCH_COUNTRY, $this->currentSpoId);
                }
                $this->eachTour['country'] = isset($countryArray[1]) ? $countryArray[1] : null;//implode(',',$aCountry);
            }

            if (!isset($this->tourtype[$node['node']['attributes']['TOURTYPE']])) {
                TourType::addTourType(
                    array('name' => trim($node['node']['attributes']['TOURTYPE'])),
                    $operator_id
                );
                $this->initializeArray(true, 'initTourType');
            }

            $this->tour_type = $this->tourtype[$node['node']['attributes']['TOURTYPE']] ? $this->tourtype[$node['node']['attributes']['TOURTYPE']] : null;
            $this->eachTour['adult'] = $node['node']['attributes']['ADL'];
            $this->eachTour['children'] = $node['node']['attributes']['CHD'];
            $this->eachTour['infant'] = $node['node']['attributes']['INF'];
            $this->eachTour['currency'] = $node['node']['attributes']['CURRENCY'];
        }

        if ($node['name'] == 'HOTEL') {
            // Определяем и записываем звездность отелей
            if (isset($node['node']['attributes']['STAR'])) {
                $hotelStar = trim(str_replace(array ('*****', '****', '***', '**', '*'), '', $node['node']['attributes']['STAR']));
                if (isset($this->hotelStar[$hotelStar])) {
                } else {
                    Star::addStar(
                        array('star' => $hotelStar),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotelStar');
                }
            }
            $this->eachTour['star'] = $this->hotelStar[$hotelStar];

            // Определяем и записываем город
            if (!isset($this->region[$node['node']['attributes']['CITY']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['CITY']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITY']),
                            'country_id' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::EMPTY_CITY, $this->currentSpoId);
                }
            }
            $this->eachTour['city'] = $this->region[$node['node']['attributes']['CITY']];

            // Определяем и записываем отель(id: звездность, город, страна)
            if (!isset($this->hotel[ $node['node']['attributes']['HTC']]) && $this->errorTour === false) {
                if (strlen($node['node']['attributes']['HTC'] > 0)) {
                    Hotel::addHotel(
                        array(
                            'id'      => $node['node']['attributes']['HTC'],
                            'name'    => $node['node']['attributes']['NAME'],
                            'star'    => $this->hotelStar[$hotelStar],
                            'city'    => $this->region[$node['node']['attributes']['CITY']],
                            'country' => $this->eachTour['country']
                        ),
                        $operator_id
                    );
                    $this->initializeArray(true, 'initHotel');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::EMPTY_HOTEL, $this->currentSpoId);
                }
            }
            $this->eachTour['hotel'] = $node['node']['attributes']['HTC'];

            // Тип комнаты
            if (!isset($this->roomIds[$node['node']['attributes']['RMC']])) {
                if (strlen($node['node']['attributes']['RMC'] > 0)) {
                    Room::addRoom(
                        array(
                            'id' => trim($node['node']['attributes']['RMC']),
                            'name' => trim($node['node']['attributes']['ROOM'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRoom');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::EMPTY_ROOM, $this->currentSpoId);
                }
            }
            $this->eachTour['room'] = $node['node']['attributes']['RMC'];

            // Размещение в номере
            if (!isset($this->stayTypeIds[$node['node']['attributes']['PLC']])) {
                if (strlen($node['node']['attributes']['PLC'] > 0)) {
                    StayType::addStayType(
                        array(
                            'id' => trim($node['node']['attributes']['PLC']),
                            'name' => trim($node['node']['attributes']['PLACE'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initStayType');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::EMPTY_STAY_TYPE, $this->currentSpoId);
                }
            }
            $this->eachTour['stayType'] = $node['node']['attributes']['PLC'];

            // Тип питания
            if (!isset($this->pansionIds[$node['node']['attributes']['MLC']])) {
                if (strlen($node['node']['attributes']['MLC']) > 0 && strlen($node['node']['attributes']['MEAL']) > 0) {
                    Pansion::addPansion(
                        array(
                            'id' => trim($node['node']['attributes']['MLC']),
                            'name' => trim($node['node']['attributes']['MEAL'])
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initPansion');
                } else {
                    $this->errorTour = true;
                    Log::log(Log::WARN, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::EMPTY_PANSION, $this->currentSpoId);
                }
            }
            $this->eachTour['pansion'] = $node['node']['attributes']['MLC'];
        }

        if ($node['name'] == 'TRANSPORT') {
            if ($node['node']['attributes']['NAME'] == 'FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['cityDepatured'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }

                if (isset($this->region[$node['node']['attributes']['CITYTO']])) {
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYTO']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
//                    $this->eachTour['arrivalRegion'] = $this->region[$node['node']['attributes']['CITYTO']];
                } elseif (strlen($node['node']['attributes']['CITYTO']) == 0) {
                    $this->eachTour['cityDepatured'] = null;
                }
            } elseif ($node['node']['attributes']['NAME'] == 'BACK FLIGHT') {
                if (isset($this->region[$node['node']['attributes']['CITYFR']])) {
                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) > 0) {
                    City::addCity(
                        array(
                            'name' => trim($node['node']['attributes']['CITYFR']),
                            'country_id' => null
                        ),
                        $operator_id
                    );
                    $this->initializeArray(false, 'initRegion');
                    $this->eachTour['departuredRegion'] = $this->region[$node['node']['attributes']['CITYFR']];
                } elseif (strlen($node['node']['attributes']['CITYFR']) == 0) {
                    $this->eachTour['departuredRegion'] = null;
                }
            }
        } else {
            $this->eachTour['cityDepatured'] = isset($this->eachTour['cityDepatured']) ? $this->eachTour['cityDepatured'] : null;
//            $this->eachTour['arrivalRegion'] =  isset($this->eachTour['arrivalRegion']) ? $this->eachTour['arrivalRegion'] : null;
            $this->eachTour['departuredRegion'] = isset($this->eachTour['departuredRegion']) ? $this->eachTour['departuredRegion'] : null;
        }

        if ($node['name'] == 'PRICE') {
            // сортировка по дате и актуальности туров
            if (strtotime($node['node']['attributes']['DATE']) > time() &&
                strtotime($node['node']['attributes']['DATE']) < Registry::get('parser_start_date') &&
                $this->errorTour === false
            ) {

                $this->eachTour['dateStart'] = $node['node']['attributes']['DATE'];//substr($node['node']['attributes']['DATE'], 0 , 4) . '-' . substr($node['node']['attributes']['DATE'], -4 , 2) . '-' . substr($node['node']['attributes']['DATE'], -2 , 2);
                $this->eachTour['price'] = $node['node']['attributes']['VAL'];
                $this->eachTour['nightCount'] = $node['node']['attributes']['N'];
                $this->eachTour['dateEnd'] = date("Y-m-d", strtotime($this->eachTour['dateStart']) + 86400*$this->eachTour["nightCount"]);
                $this->eachTour['tour'] = $this->countTour;
                $this->eachTour['update'] = 1;
                $this->eachTour['tourOperator'] = Operator::getOperatorId('IDRISKO');
                $this->eachFiftiesTour[] = $this->eachTour;

                // Стек по 50 туров
                $this->saveTours($operator_id);
            }
            // Подсчет всех туров
            $this->countTour++;
        }

        // Запись оставшихся туров
        $this->saveLastTours($node, $operator_id);

        // Файл не докачан
        if (isset($node['error'])) {
            Log::log(Log::ERR, CRON_PARSE, Operator::getOperatorId('IDRISKO'), Log::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

}
