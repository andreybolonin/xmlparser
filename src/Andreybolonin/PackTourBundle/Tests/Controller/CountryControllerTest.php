<?php

namespace Andreybolonin\PackTourBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CountryControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testAjax()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ajax');
    }

}
