<?php

namespace Andreybolonin\PackTourBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HotelControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testAjax()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ajax');
    }

    public function testBooking()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/booking');
    }

    public function testSavehotel()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/saveHotel');
    }

}
