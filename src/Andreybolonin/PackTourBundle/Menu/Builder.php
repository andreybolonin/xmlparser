<?php

namespace Andreybolonin\PackTourBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $security = $this->container->get('security.context');
        $tour_manager = $this->container->get('tour_manager');

        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(array(
            'class' => 'menu',
            'id' => 'main_navigation'
        ));

        if (!$security->getToken()) {
            return $menu;
        }

//        $menu->addChild('Главная', array('route' => 'default'))
//            ->setLinkAttributes(array('class' => 'main'));

        $menu->addChild('Города ' . $tour_manager->relationCount('city'), array('route' => 'city'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Страны ' . $tour_manager->relationCount('country'), array('route' => 'country'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Отели ' . $tour_manager->relationCount('hotel'), array('route' => 'hotel'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Питание ' . $tour_manager->relationCount('pansion'), array('route' => 'pansion'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Комнаты ' . $tour_manager->relationCount('room'), array('route' => 'room'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Звездность ' . $tour_manager->relationCount('star'), array('route' => 'star'))
            ->setLinkAttributes(array('class' => 'main'));

        $menu->addChild('Лог связей', array('route' => 'relation_log'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Лог мигратора', array('route' => 'migrator_errors'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Операторы', array('route' => 'operators'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Spo', array('route' => 'spo'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Спаршенные туры', array('route' => 'parse_tours'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Пакетные туры', array('route' => 'pack_tours'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Параметры импорта', array('route' => 'tour_params'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Статистика отелей', array('route' => 'hotel_stat'))
            ->setLinkAttributes(array('class' => 'main'));
        $menu->addChild('Выход', array('route' => 'logout'))
            ->setLinkAttributes(array('class' => 'main'));

        $menu->setCurrentUri($this->container->get('request')->getRequestUri());

        return $menu;

    }
}
