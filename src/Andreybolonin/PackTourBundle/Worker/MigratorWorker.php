<?php

namespace Andreybolonin\PackTourBundle\Worker;

use GearmanJob;
use Laelaps\GearmanBundle\Annotation as Gearman;
use Laelaps\GearmanBundle\Worker;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Выполняет сохранение в БД отмигрированных "на лету" туров
 */
class MigratorWorker extends Worker
{
    /**
     * Запуск воркера - nohup php app/console gearman:worker:run src/Andreybolonin/PackTourBundle/Worker/ParserWorker.php &
     * Монитор - app/console gearman:status
     *
     * @Gearman\PointOfEntry(name="migrator")
     * @param  GearmanJob                                        $job
     * @param  \Symfony\Component\Console\Output\OutputInterface $output
     * @return boolean                                           returning false means job failure
     */
    public function doJob(GearmanJob $job, OutputInterface $output)
    {
        $tour_manager = $this->get('tour_manager');
        $data = unserialize($job->workload());

        if (count($data['pack_tours']) > 0) {
            $tour_manager->saveTours($data['pack_tours'], $data['operator_id']);
        }
        if (count($data['moved_tours']) > 0) {
            $tour_manager->deleteMigratedTours($data['moved_tours']);
            //Tour::changeStatusMigratedTours($moved_tours, 1);
        }
        if (count($data['error_tours']) > 0) {
            $tour_manager->changeStatusMigratedTours($data['error_tours'], 2);
        }
    }
}
