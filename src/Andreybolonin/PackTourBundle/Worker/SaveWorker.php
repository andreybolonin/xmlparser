<?php

namespace Andreybolonin\PackTourBundle\Worker;

use Andreybolonin\PackTourBundle\Db;
use GearmanJob;
use Laelaps\GearmanBundle\Annotation as Gearman;
use Laelaps\GearmanBundle\Worker;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Выполняет сохранение распаршенных туров
 */
class SaveWorker extends Worker
{
    /**
     * Запуск воркера - nohup php app/console gearman:worker:run src/Andreybolonin/PackTourBundle/Worker/ParserWorker.php &
     * Монитор - app/console gearman:status
     *
     * @Gearman\PointOfEntry(name="save")
     * @param  GearmanJob                                        $job
     * @param  \Symfony\Component\Console\Output\OutputInterface $output
     * @return boolean                                           returning false means job failure
     */
    public function doJob(GearmanJob $job, OutputInterface $output)
    {
        $pdo_backend = $this->get('pdo_backend');
        $array = unserialize($job->workload());

        $eachFiftiesTour = $array['eachFiftiesTour'];
        $operator_id = $array['operator_id'];

        Db::PdoMultiInsertHelper($pdo_backend, 'xml_tour.xml_tour_' . $operator_id, $eachFiftiesTour);
    }
}
