<?php

namespace Andreybolonin\PackTourBundle\Worker;

use GearmanJob;
use Laelaps\GearmanBundle\Annotation as Gearman;
use Laelaps\GearmanBundle\Worker;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Выполняет скачивание, анализ и парсинг туров
 */
class ParserWorker extends Worker
{
    /**
     * Запуск воркера - nohup php app/console gearman:worker:run src/Andreybolonin/PackTourBundle/Worker/ParserWorker.php &
     * Монитор - app/console gearman:status
     *
     * @Gearman\PointOfEntry(name="parser")
     * @param  GearmanJob                                        $job
     * @param  \Symfony\Component\Console\Output\OutputInterface $output
     * @return boolean                                           returning false means job failure
     */
    public function doJob(GearmanJob $job, OutputInterface $output)
    {
        $spo_manager = $this->get('spo_manager');
        $spo = unserialize($job->workload());

        $spo = $spo_manager->downloadSpo($spo);
        $spo = $spo_manager->analyzeSpo($spo);
        $spo_manager->parseSpo($spo);
    }
}
