$(document).ready(function() {

    // Выбор оператора
    $(document).on('change', "select[name='operator_id']", function () {
        if($(this).val() != 'no') {
            $.ajax({
                type: 'POST',
                url: 'city/ajax',
                dataType: 'json',
                data: ({
                    operator_id: $('select[name="operator_id"]').val()
                }),
                success: function(response){
                    getInfo(response);
                }
            });
        } else {
            $('.right_loop_scroll ul').empty();
            $('.right_loop_scroll_backup ul').empty();
        }
    });

    // Управление связями
    $(document).on('change', "input[name='related_city']", function () {
        var action = $(this).is(':checked') ? 'add' : 'remove';
        if (!$("input[name='main_city']").is(':checked') && $(this).is(':checked')) {
            alert('Выберите город');
            $(this).attr('checked', false);
        } else {
            $.ajax({
                type: 'POST',
                url: 'city/manage_relation',
                dataType: 'json',
                data: ({
                    operator_id: $("select[name='operator_id']").val(),
                    country_id: $('select[name="country_id"]').val(),
                    main_city_id: $("input[name='main_city']:checked").val(),
                    operator_city_id: $(this).val(),
                    action: action
                }),
                success: function(response){
                    getInfo(response);
                }
            });
        }
    });

    // Добавление города
    $("#form_add_city").submit(function () {
        if ($('#city_name').val() && $('select[name="operator_id"]').val() && $('select[name="main_country_id"]').val() != 'no') {
            $.ajax({
                type: 'POST',
                url: 'city/add',
                dataType: 'json',
                data: ({
                    operator_id: $('select[name="operator_id"]').val(),
                    name: $('#city_name').val(),
                    main_country_id: $('#main_country_id').val()
                }),
                success: function(response){
                    if (response.success) {
                        $('#city_name').val('');
                        $('#main_country_id').val('no');
                        getInfo(response);
                        alert(response.message);
                    } else {
                        $('#city_name').val('');
                        alert(response.message);
                    }
                }
            });
        } else {
            alert('Для добавления укажите город (на русском), страну и выберите оператора');
        }
        return false;
    });

//    $("input[name='main_city']").live('click', function() {
//        if (this.previous) {
//            this.checked = false;
//        }
//        this.previous = this.checked;
//    });
});

function getInfo(response) {
    $('.right_loop_scroll ul').empty();
    $('.right_loop_scroll_backup ul').empty();
    $('.left_loop_scroll ul').empty();

    if (response.main_city) {
        $.each(response.main_city, function(i, val) {
            var bold = null;
            if (val.related === true) { bold = 'class="bold"';}
            $('<li><label><input type="radio" name="main_city" value="' + val.id + '" /><span '+ bold +'>' + val.name + '</span></label></li>').appendTo(".left_loop_scroll ul");
        });
    }

    if(response.operator_city) {
        $.each(response.operator_city, function(i, val) {
            $('<li><label><input type="radio" name="related_city" value="' + val.id + '" >' + val.name + '</label> </li>').appendTo(".right_loop_scroll ul");
        });
    }

    if (response.related_city) {
        $.each(response.related_city, function(i, val) {
            $('<li><label><input type="checkbox" checked="checked" main="' + val.related_id + '" name="related_city" value="' + val.id + '" >' + val.name + ' (' + val.related_name + ')</label></li>').appendTo(".right_loop_scroll_backup ul");
        });
    }
}
