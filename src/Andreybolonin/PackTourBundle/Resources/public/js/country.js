$(document).ready(function() {

    //  Выбор оператора
    $("select[name='operator_id']").on('change', function () {
        if($(this).val() != 'no') {
            $.ajax({
                type: 'POST',
                url: 'country/ajax',
                dataType: 'json',
                data: ({
                    operator_id: $('select[name="operator_id"]').val()
                }),
                success: function(response){
                    update_tourobzor(response);
                    getInfo(response);
                }
            });
        } else {
            $('.right_loop_scroll ul').empty();
            $('.right_loop_scroll_backup ul').empty();
        }
    });

    // Управление связями
    $(document).on('change', "input[name='related_country']", function () {
        var action = $(this).is(':checked') ? 'add' : 'remove';
        if (!$("input[name='main_country']").is(':checked') && $(this).is(':checked')) {
            alert('выбери страну');
            $(this).attr('checked', false);
        } else {
            $.ajax({
                type: 'POST',
                url: 'country/manage_relation',
                dataType: 'json',
                data:({
                    operator_id: $("select[name='operator_id']").val(),
                    base: $("input[name='main_country']:checked").val(),
                    xml: $(this).val(),
                    action: action
                }),
                success: function(response){
                    getInfo(response);
                }
            });
        }
    });

//    $(document).on('click', "input[name='main_country']", function() {
//        if (this.previous) {
//            this.checked = false;
//        }
//        this.previous = this.checked;
//    });
});


function getInfo(response) {
    $('.right_loop_scroll ul').empty();
    $('.right_loop_scroll_backup ul').empty();

    if(response.operator_country) {
        $.each(response.operator_country, function(i, val) {
            if (val.url) {
                $('<li><label><input type="radio" name="related_country" value="' + val.id + '" >' + val.name + '</label></li>').appendTo(".right_loop_scroll ul");
            }
            else {
                $('<li><label><input type="radio" name="related_country" value="' + val.id + '" >' + val.name + '</label></li>').appendTo(".right_loop_scroll ul");
            }

        });
    }

    if (response.related_country) {
        $.each(response.related_country, function(i, val) {
            $('<li><label><input type="checkbox" checked="checked" main="' + val.related_id + '" name="related_country" value="' + val.id + '" >' + val.name + ' (' + val.related_name + ')</label></li>').appendTo(".right_loop_scroll_backup ul");
        });
    }
}

function update_tourobzor(response) {
    $('.left_loop_scroll ul').empty();

    $.each(response.main_country, function(i, val) {
        var bold = null;
        if (val.related === true) { bold = 'class="bold"';}
        $('<li><label><input type="radio" name="main_country" value="' + val.id + '" /><span '+ bold +'>' + val.name + '</span></label></li>').appendTo(".left_loop_scroll ul");
    });
}
