$(document).ready(function() {

    //  Выбор оператора
    $(document).on('change', "select[name='operator_id']", function () {
        if($(this).val() != 'no') {
            $.ajax({
                type: 'POST',
                url: 'hotel/ajax',
                dataType: 'json',
                data: ({
                    operator_id: $('select[name="operator_id"]').val()
                }),
                success: function(response){
                    $('select[name="country_id"]').empty();
                    getInfo(response);
                }
            });
        } else {
            $('.right_loop_scroll ul').empty();
            $('.right_loop_scroll_backup ul').empty();
        }
    });

    // Выбор страны
    $(document).on('change', 'select[name="country_id"]', function () {
        if($(this).val() != '0') {
            $.ajax({
                type: 'POST',
                url: 'hotel/ajax',
                dataType: 'json',
                data: ({
                    operator_id: $("select[name='operator_id']").val(),
                    country_id: $(this).val()
                }),
                success: function(response){
                    $('select[name="city_id"]').empty();
                    getInfo(response);
                }
            });
        }
    });

    // Управление связями
    $(document).on('change', "input[name='related_hotel']", function () {
        var action = $(this).is(':checked') ? 'add' : 'remove';
        if (!$("input[name='main_hotel']").is(':checked') && $(this).is(':checked')) {
            alert('Сначала нужно выбрать отель ТурОбзор слева.');
            $(this).attr('checked', false);
        } else {
            $.ajax({
                type: 'POST',
                url: 'hotel/manage_relation',
                dataType: 'json',
                data: ({
                    action: action,
                    operator_id: $("select[name='operator_id']").val(),
                    country_id: $("select[name='country_id']").val(),
                    main_hotel: $("input[name='main_hotel']:checked").val(),
                    operator_hotel: $(this).val()
                }),
                success: function(response){
                    getInfo(response);
                }
            });
        }
    });

    // Добавление отеля
    $(document).on('click', ".add_hotel_ajax", function () {
        var name = $(this).attr("name");
        if (confirm('Добавить отель ' + name + ' ?')) {
            $.ajax({
                type: 'POST',
                url: 'hotel/add',
                dataType: 'json',
                data: ({
                    operator_id: $('select[name="operator_id"]').val(),
                    country_id: $(this).attr("country_id"),
                    city_id: $(this).attr("city_id"),
                    name: name
                }),
                success: function(response){
                    if (response.success) {
                        alert(response.message);
                        getInfo(response);
                    } else {
                        alert(response.message);
                    }
                }
            });
        }
    });
});

function getInfo(response) {

//    if (response.main_hotel_str) {
//        $('.left_loop_scroll ul').empty();
//        $('.left_loop_scroll ul').append(response.main_hotel_str);

//        $(response.main_hotel_str).insertAfter('.left_loop_scroll ul');
//        $.each(response.main_hotel, function(i, val) {
//            var bold = null;
////            if (val.related === true) { bold = 'class="bold"';}
//            $('<li><label><input type="radio" name="main_hotel" value="' + val.id + '" /><span '+ bold +'>' + val.name + '</span></label></li>').appendTo(".left_loop_scroll ul");
//        });
//    }

//    if(response.operator_hotel) {
//        $.each(response.operator_hotel, function(i, val) {
//            $('<li><label><input type="radio" name="related_hotel" value="' + val.id + '" >' + val.name + '</label></li>').appendTo(".right_loop_scroll ul");
//        });
//    }

    if (response.hotel) {
        $('<li><label><input type="radio" name="main_hotel" value="' + response.hotel.id + '" >' + response.hotel.name + '</label></li>').appendTo(".left_loop_scroll ul");
    }

    if (response.country_list) {
        $('<option value="no"></option>').appendTo("select[name='country_id']");
        $.each(response.country_list, function(i, val) {
            var selected = response.current_main_country == val.base ? 'selected' : null;
            $('<option ' + selected + ' value="' + val.id + '">' + val.name + '</option>').appendTo("select[name='country_id']");
        });
    }

    if (response.operator_hotel_str) {
        $('.right_loop_scroll ul').empty();
        $('.right_loop_scroll ul').append(response.operator_hotel_str);
    }

//    if (response.related_hotel) {
//        $.each(response.related_hotel, function(i, val) {
//            $('<li><label><input type="checkbox" checked="checked" main="' + val.related_id + '" name="related_hotel" value="' + val.id + '" >' + val.name + ' (' + val.related_name + ')</label></li>').appendTo(".right_loop_scroll_backup ul");
//        });
//    }

    if (response.related_hotel_str) {
        $('.right_loop_scroll_backup ul').empty();
        $('.right_loop_scroll_backup ul').append(response.related_hotel_str);
    }

    if (response.add_hotel) {
        alert(response.add_hotel);
        $("input[name='hotel_add']").val('');
    }

}
