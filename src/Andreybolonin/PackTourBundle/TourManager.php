<?php

namespace Andreybolonin\PackTourBundle;

use Predis\Client;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Класс для работы с турами
 */
class TourManager
{

    // Не мигрированный тур
    const MOVED_NOT_MIGRATED = 0;
    // Успешно отмигрированный тур
    const MOVED_MIGRATED = 1;
    // Была попытка отмигрировать тур, но в relation не нашлось соответствия по одному или нескольким параметрам тура
    const MOVED_ERROR_MIGRATED = 2;

    public static $hotel_status_map = array(
        0 => 'Новый',
        1 => 'Заполнен не  полностью',
        2 => 'Ожидает подтвержения',
        3 => 'Подтвежден',
        4 => 'Проблемный',
        5 => 'Отправленых на доработку',
        6 => 'Городской отель'
    );

    /**
     * @param Container      $container
     * @param \Predis\Client $redis
     * @param \GearmanClient $gearman_client
     * @param SpoManager     $spo_manager
     * @param LogManager     $log_manager
     * @param \PDO           $pdo_master
     * @param \PDO           $pdo_backend
     */
    public function __construct(
        Container $container,
        Client $redis,
        \GearmanClient $gearman_client,
        SpoManager $spo_manager,
        LogManager $log_manager,
        \PDO $pdo_master,
        \PDO $pdo_backend
    ) {
        $this->container = $container;
        $this->redis = $redis;
        $this->gearman_client = $gearman_client;
        $this->spo_manager = $spo_manager;
        $this->log_manager = $log_manager;
        $this->pdo_master = $pdo_master;
        $this->pdo_backend = $pdo_backend;
    }

    /**
     * Возвращает список городов по оператору
     *
     * @param $operator_id
     * @return array
     */
    public function getCityListByOperatorId($operator_id)
    {
        $ids = array();
        $response_data = array();

        // Список связей по оператору
        $relation = $this->pdo_master->query("SELECT * FROM pack_tours.relation_city WHERE operator = '" . $operator_id . "'")->fetchAll();
        foreach ($relation as $city) {
            $ids[] = $city['xml'];
        }

        // Список городов TourObzor
        $main = $this->pdo_master->query("SELECT id, name FROM geo.resort ORDER BY name")->fetchAll();

        // Список городов оператора
        $operator = $this->pdo_master->query("SELECT * FROM pack_tours.city_" . $operator_id . " WHERE id NOT IN (".implode(", ", $ids).") ORDER BY name")->fetchAll();

        // Список связанных городов, джойним через таблицу relation
        $related = $this->pdo_master->query('SELECT city_' . $operator_id . '.*, geo.resort.name AS related_name
            FROM pack_tours.city_' . $operator_id . '
            INNER JOIN pack_tours.relation_city
            ON (city_' . $operator_id . '.id = pack_tours.relation_city.xml
                AND pack_tours.relation_city.operator = "' . $operator_id . '")
            INNER JOIN geo.resort
            ON (pack_tours.relation_city.base = geo.resort.id)
            ORDER BY city_' . $operator_id . '.name'
        )->fetchAll();

        $response_data['main_city'] = $main;
        $response_data['operator_city'] = $operator;
        $response_data['related_city'] = $related;

        return $response_data;
    }

    /**
     * Возвращает список стран по оператору
     *
     * @param $operator_id
     * @return array
     */
    public function getCountryListByOperatorId($operator_id)
    {
        $ids = array();
        $response_data = array();

        // Список связей по оператору
        $relation = $this->pdo_master->query("SELECT * FROM pack_tours.relation_country WHERE operator = '" . $operator_id . "'")->fetchAll();
        foreach ($relation as $city) {
            $ids[] = $city['xml'];
        }

        // Список городов TourObzor
        $main = $this->pdo_master->query("SELECT id, name FROM geo.country ORDER BY name")->fetchAll();

        // Список городов оператора
        $operator = $this->pdo_master->query("SELECT * FROM pack_tours.country_" . $operator_id . " WHERE id NOT IN (".implode(", ", $ids).") ORDER BY name")->fetchAll();

        // Список связанных городов, джойним через таблицу relation
        $related = $this->pdo_master->query('SELECT pack_tours.country_' . $operator_id . '.*, geo.country.name AS related_name
            FROM pack_tours.country_' . $operator_id . '
            INNER JOIN pack_tours.relation_country
            ON (country_' . $operator_id . '.id = pack_tours.relation_country.xml
                AND pack_tours.relation_country.operator = "' . $operator_id . '")
            INNER JOIN geo.country
            ON (pack_tours.relation_country.base = geo.country.id)
            ORDER BY country_' . $operator_id . '.name'
        )->fetchAll();

        $response_data['main_country'] = $main;
        $response_data['operator_country'] = $operator;
        $response_data['related_country'] = $related;

        return $response_data;
    }

    /**
     * Метод работоспособен без country_id.
     * Параметр country_id добавлен исключительно в целях разделения большого массива данных отелей на части по странам.
     *
     * @param $operator_id
     * @param  null  $country_id
     * @return mixed
     */
    public function getHotelListByOperatorId($operator_id, $country_id = null)
    {
        // Список связей по оператору
        $relation_city = $this->pdo_master->query("SELECT * FROM pack_tours.relation_hotel WHERE operator = '" . $operator_id . "'")->fetchAll();
        $ids = array();
        foreach ($relation_city as $city) {
            $ids[] = $city['xml'];
        }

        // Список отелей оператора. Условие сделано для того чтобы включать и убирать WHERE id NOT IN () когда есть массив id и когда его нет.
        if (count($ids) > 0) {
            $operator_hotel = $this->pdo_master->query("SELECT * FROM pack_tours.hotel_" . $operator_id . " WHERE id NOT IN (".implode(", ", $ids).") AND country = " . $country_id . " ORDER BY name")->fetchAll();
        } else {
            $operator_hotel = $this->pdo_master->query("SELECT * FROM pack_tours.hotel_" . $operator_id." WHERE country = " . $country_id . " ORDER BY name")->fetchAll();
        }

        // Список связанных отелей, джойним через таблицу relation
        $related_hotel = $this->pdo_master->query('SELECT pack_tours.hotel_' . $operator_id . '.*, base.hotel.name AS related_name, base.hotel.id AS related_id
            FROM pack_tours.hotel_' . $operator_id . '
            INNER JOIN pack_tours.relation_hotel
            ON (hotel_' . $operator_id . '.id = pack_tours.relation_hotel.xml
                AND pack_tours.relation_hotel.operator = "' . $operator_id . '")
            INNER JOIN base.hotel
            ON (pack_tours.relation_hotel.base = base.hotel.id)
            WHERE pack_tours.hotel_' . $operator_id . '.country = ' . $country_id . '
            ORDER BY pack_tours.hotel_' . $operator_id . '.name'
        )->fetchAll();

//        $main_hotel_str = '';
//        foreach ($main_hotel as $el) {
//            $main_hotel_str .= '<li><label><input type="radio" name="main_hotel" value="' . $el['id'] . '"/>' . $el['name'] . '</label></li>';
//        }

        $operator_hotel_str = '';
        foreach ($operator_hotel as $el) {
            $operator_hotel_str .= '<li><label><input type="radio" name="related_hotel" value="' . $el['id'] . '" >' . $el['name'] . '</label> <a name="' . $el['name'] . '" country_id="' . $el['country'] . '" city_id="' . $el['city'] . '" href="#" class="add_hotel_ajax">(Добавить отель)</a></li>';
        }

        $related_hotel_str = '';
        foreach ($related_hotel as $el) {
            $related_hotel_str .= '<li><label><input type="checkbox" checked="checked" main="' . $el['related_id'] . '" name="related_hotel" value="' . $el['id'] . '" >' . $el['name'] . ' (' . $el['related_name'] . ')</label></li>';
        }

//        $array['main_hotel_str'] = $main_hotel_str;
        $array['operator_hotel_str'] = $operator_hotel_str;
        $array['related_hotel_str'] = $related_hotel_str;

        return $array;
    }

    /**
     * Отели оператора сгруппированные по стране и приджойненная таблица стран оператора
     *
     * @param $operator_id
     * @return array
     */
    public function getHotelCountryList($operator_id)
    {
        $data['country_list'] = $this->pdo_master->query('SELECT * FROM pack_tours.hotel_' . $operator_id . '
            LEFT JOIN pack_tours.country_' . $operator_id . '
            ON hotel_' . $operator_id . '.country = pack_tours.country_' . $operator_id . '.id
            GROUP BY hotel_' . $operator_id . '.country'
        )->fetchAll();

        return $data;
    }

    /**
     * @param $operator_id
     * @return mixed
     */
    public function getPansionListByOperatorId($operator_id)
    {
        $ids = array();
        $response_data = array();

        // Список связей по оператору
        $relation = $this->pdo_master->query("SELECT * FROM pack_tours.relation_pansion WHERE operator = '" . $operator_id . "'")->fetchAll();
        foreach ($relation as $city) {
            $ids[] = $city['xml'];
        }

        // Список TourObzor
        $main = $this->pdo_master->query("SELECT id, name FROM cdc.ration ORDER BY name")->fetchAll();

        // Список оператора
        $operator = $this->pdo_master->query("SELECT * FROM pack_tours.pansion_" . $operator_id . " WHERE id NOT IN (".implode(", ", $ids).") ORDER BY name")->fetchAll();

        // Список связанных, джойним через таблицу relation
        $related = $this->pdo_master->query('SELECT pack_tours.pansion_' . $operator_id . '.*, cdc.ration.name AS related_name
            FROM pack_tours.pansion_' . $operator_id . '
            INNER JOIN pack_tours.relation_pansion
            ON (pansion_' . $operator_id . '.id = pack_tours.relation_pansion.xml
                AND pack_tours.relation_pansion.operator = "' . $operator_id . '")
            INNER JOIN cdc.ration
            ON (pack_tours.relation_pansion.base = cdc.ration.id)
            ORDER BY pansion_' . $operator_id . '.name'
        )->fetchAll();

        $response_data['main'] = $main;
        $response_data['operator'] = $operator;
        $response_data['related'] = $related;

        return $response_data;
    }

    /**
     * @param $operator_id
     * @return mixed
     */
    public function getRoomListByOperatorId($operator_id)
    {
        $ids = array();
        $response_data = array();

        // Список связей по оператору
        $relation = $this->pdo_master->query("SELECT * FROM pack_tours.relation_room WHERE operator = '" . $operator_id . "'")->fetchAll();
        foreach ($relation as $city) {
            $ids[] = $city['xml'];
        }

        // Список TourObzor
        $main = $this->pdo_master->query("SELECT id, name FROM cdc.room_types ORDER BY name")->fetchAll();

        // Список оператора
        $operator = $this->pdo_master->query("SELECT * FROM pack_tours.room_" . $operator_id . " WHERE id NOT IN (".implode(", ", $ids).") ORDER BY name")->fetchAll();

        // Список связанных, джойним через таблицу relation
        $related = $this->pdo_master->query('SELECT pack_tours.room_' . $operator_id . '.*, cdc.room_types.name AS related_name
            FROM pack_tours.room_' . $operator_id . '
            INNER JOIN pack_tours.relation_room
            ON (room_' . $operator_id . '.id = pack_tours.relation_room.xml
                AND pack_tours.relation_room.operator = "' . $operator_id . '")
            INNER JOIN cdc.room_types
            ON (pack_tours.relation_room.base = cdc.room_types.id)
            ORDER BY room_' . $operator_id . '.name'
        )->fetchAll();

        $response_data['main'] = $main;
        $response_data['operator'] = $operator;
        $response_data['related'] = $related;

        return $response_data;
    }

    /**
     * @param $operator_id
     * @return mixed
     */
    public function getStarListByOperatorId($operator_id)
    {
        $ids = array();
        $response_data = array();

        // Список связей по оператору
        $relation = $this->pdo_master->query("SELECT * FROM pack_tours.relation_star WHERE operator = '" . $operator_id . "'")->fetchAll();
        foreach ($relation as $city) {
            $ids[] = $city['xml'];
        }

        // Список TourObzor
        $main = $this->pdo_master->query("SELECT id, name FROM cdc.star ORDER BY name")->fetchAll();

        // Список оператора
        $operator = $this->pdo_master->query("SELECT * FROM pack_tours.star_" . $operator_id . " WHERE id NOT IN (".implode(", ", $ids).") ORDER BY star")->fetchAll();

        // Список связанных, джойним через таблицу relation
        $related = $this->pdo_master->query('SELECT pack_tours.star_' . $operator_id . '.*, cdc.star.name AS related_name
            FROM pack_tours.star_' . $operator_id . '
            INNER JOIN pack_tours.relation_star
            ON (pack_tours.star_' . $operator_id . '.id = pack_tours.relation_star.xml
                AND pack_tours.relation_star.operator = "' . $operator_id . '")
            INNER JOIN cdc.star
            ON (pack_tours.relation_star.base = cdc.star.id)
            ORDER BY pack_tours.star_' . $operator_id . '.star'
        )->fetchAll();

        $response_data['main'] = $main;
        $response_data['operator'] = $operator;
        $response_data['related'] = $related;

        return $response_data;
    }

    /**
     *
     */
    public function parser()
    {
        $gearman_enable = $this->container->getParameter('gearman_enable');

        // TODO Integrate this feature with this script - https://github.com/laelaps/symfony-gearman-bundle/issues/2#issuecomment-16257507
        //$this->automaticBalanced();

        $spo_array = $this->spo_manager->getSpoToParse();

        foreach ($spo_array as $spo) {
            if ($gearman_enable) {
                $this->gearman_client->doHighBackground('parser', serialize($spo));
            } else {
                $spo = $this->spo_manager->downloadSpo($spo);
                $spo = $this->spo_manager->analyzeSpo($spo);
                $this->spo_manager->parseSpo($spo);
            }
        }
    }

    /**
     *
     */
    public function migrator()
    {
        // This string to be commented for production
//        $this->redis->set('migrator_is_active', 0);
        $this->protectionMigrator();

        $this->migrateTours(TourManager::MOVED_NOT_MIGRATED);
        $this->migrateTours(TourManager::MOVED_ERROR_MIGRATED);

        $this->log_manager->migrationSaveLogs();
        $this->redis->set('migrator_is_active', 0);
    }

    /**
     * Выполняет миграцию новых или ошибочных туров
     *
     * @param $migrate_status
     * @internal param $operator_id
     * @return bool
     */
    public function migrateTours($migrate_status)
    {
        $count_tours = $this->container->getParameter('migrate_count_new');
        $operators = $this->container->getParameter('operators');

        foreach ($operators as $operator_id) {
            $stopwatch = new Stopwatch();
            $stopwatch->start('migrate');

            $tours = $this->getToursByOperatorAndMoved($migrate_status, $count_tours, $operator_id);
            if (!count($tours) > 0) {
                continue;
            }
            list($z, $in_base) = $this->migrateProcess($tours, $operator_id);

            echo 'Из ', $z, ' туров перенесено в базу : ', $in_base, PHP_EOL;
            $event = $stopwatch->stop('migrate');
            $speed = number_format($count_tours / ($event->getDuration() / 1000), 0, '.', '');
            echo 'Скорость миграции(туров/сек): ', $speed, PHP_EOL;
        }
    }

    /**
     * Процесс перебора туров для миграции
     *
     * @param $tours
     * @param $operator_id
     * @return array
     */
    public function migrateProcess($tours, $operator_id)
    {
        $z = 0;
        $in_base = 0;
        $migrate_insert_count = $this->container->getParameter('migrate_insert_count');
        $count_tours = count($tours);
        $moving_errors = array();

        foreach ($tours as $tour) {
            $z++;
            list($migrated_tour, $errors) = $this->migrateTourParams($tour);

            if ($errors > 0) {
                // Массив ошибочных туров
                /* Параметр tourOperator указываеться для определения таблицы из которой надо удалять перемещенные туры,
                 * опять таки по причине физического партицирования таблицы xml_tour.tour
                 */
                $moving_errors[$tour['tourOperator']][] = $tour['id'];
                $moving_tours_error_count[] = $tour['id'];
                if (count($moving_tours_error_count) == $migrate_insert_count || $z == $count_tours) {
                    $this->save(array(), array(), $moving_errors, $operator_id);
                    $moving_errors = array();
                    unset($moving_tours_error_count);
                    $count_tours--;
                }
                continue;
            }

            $migrated_tour = $this->setTourParams($tour, $migrated_tour);
            $migrated_tours[] = $migrated_tour;
            /* Параметр tourOperator указываеться для определения таблицы из которой надо удалять перемещенные туры,
             * опять таки по причине физического партицирования таблицы xml_tour.tour
             */
            $moved_tours[$tour['tourOperator']][] = $tour['id'];

            // Массив туров
            if (count($migrated_tours) == $migrate_insert_count || $z == $count_tours) {
                $this->save($migrated_tours, $moved_tours, $moving_errors, $operator_id);
                $in_base += count($moved_tours);
                unset($migrated_tours, $moved_tours, $moving_errors);
            }
            unset($insert_array, $tour);
        }

        return array($z, $in_base);
    }

    /**
     * Выолняет саму миграцию ключей, вместо ключей справочников базы парсера
     * заменяет на ключи справочников базы пакетных туров
     *
     * @param $tour
     * @return array
     */
    public function migrateTourParams($tour)
    {
        $operator = $tour['tourOperator'];
        $errors = 0;

        $city_from = $this->getCityID($operator, $tour['cityDepatured']);
        if ($city_from <= 0 || !$city_from) {
            $migrated_tour['cityFrom'] = 0;
        } else {
            $migrated_tour['cityFrom'] = $city_from;
        }

        // room
        $room = $this->getRoomId($operator, $tour['stayType']);
        if ($room > 0) {
            $migrated_tour['room'] = $room;
        } else {
            $this->log_manager->migrationSaveLog($operator, 'room', $tour['stayType']);
            $errors++;
        }

        // hotel
        $hotel = $this->getHotelId($operator, $tour['hotel']);
        if ($hotel > 0) {
            $migrated_tour['hotel'] = $hotel;
        } else {
            $this->log_manager->migrationSaveLog($operator, 'hotel', $tour['hotel']);
            $errors++;
        }

        // star
        $star = $this->getStarsId($operator, $tour['star']);
        if ($star > 0) {
            $migrated_tour['star'] = $star;
        } else {
            $this->log_manager->migrationSaveLog($operator, 'stars', $tour['star']);
            $errors++;
        }

        // city
        $resort = $this->getCityID($operator, $tour['city']);
        if ($resort > 0) {
            $migrated_tour['resort_id'] = $resort;
        } else {
            $this->log_manager->migrationSaveLog($operator, 'resort', $tour['city']);
            $errors++;
        }

        // country
        $country = $this->getCountryId($operator, $tour['country']);
        if ($country > 0) {
            $migrated_tour['country_id'] = $country;
        } else {
            $this->log_manager->migrationSaveLog($operator, 'country', $tour['country']);
            $errors++;
        }

        // pansion
        $ration = $this->getRationId($operator, $tour['pansion']);
        if ($ration > 0) {
            $migrated_tour['ration'] = $ration;
        } else {
            $this->log_manager->migrationSaveLog($operator, 'ration', $tour['pansion']);
            $errors++;
        }

        return array($migrated_tour, $errors);
    }

    /**
     * Устанавливает параметры тура которым не нужна миграция
     *
     * @param $tour
     * @param $migrated_tour
     * @return array
     */
    public function setTourParams($tour, $migrated_tour)
    {
        $migrated_tour['start'] = strtotime($tour['dateStart']);
        $migrated_tour['nights'] = $tour['nightCount'];
        $migrated_tour['adults'] = $tour['adult'];
        $migrated_tour['childs'] = ($tour['children'] <= 0 || !$tour['children']) ? '0' : $tour['children'];
        $migrated_tour['price'] = $tour['price'];
        $migrated_tour['currency'] = $tour['currency'];

        return $migrated_tour;
    }

    /**
     * Сохраняет отмигрированные туры
     *
     * @param array $pack_tours
     * @param array $moved_tours
     * @param array $error_tours
     * @param $operator_id
     */
    public function save($pack_tours = array(), $moved_tours = array(), $error_tours = array(), $operator_id)
    {
        if (count($pack_tours) > 0) {
            $this->saveTours($pack_tours, $operator_id);
        }
        if (count($moved_tours) > 0) {
            $this->deleteMigratedTours($moved_tours);
            //$this->changeStatusMigratedTours($moved_tours, TourManager::MOVED_MIGRATED);
        }
        if (count($error_tours) > 0) {
            $this->changeStatusMigratedTours($error_tours, TourManager::MOVED_ERROR_MIGRATED);
        }
    }

    /**
     * Удаление туров не актуальных по дате
     */
    public function deleteByDate()
    {
        $operators = $this->container->getParameter('operators');
        foreach ($operators as $operator_id) {
            $this->pdo_backend->exec("DELETE FROM xml_tour.xml_tour_" . $operator_id . " WHERE dateStart < CURDATE()");
        }
    }

    /**
     * Удаление пакетных туров не актуальных по дате
     */
    public function deletePackToursByDate()
    {
        $operators = $this->container->getParameter('operators');
        foreach ($operators as $operator_id) {
            $this->pdo_master->exec("DELETE FROM pack_tours.tour_" . $operator_id . " WHERE start < " . time());
        }
    }

    /**
     * Возвращает список операторов с кол-вом не проставленных связей по сущности $entity
     *
     * @param $entity
     * @return array
     */
    public function getOperatorsWithCount($entity)
    {
        if (!in_array($entity, array('city', 'country', 'hotel', 'pansion', 'room', 'star'))) {
            return false;
        }

        $array = array();
        $operators = $this->container->getParameter('operators');

        foreach ($operators as $operator) {
            $xml_count = $this->pdo_master->query('SELECT COUNT(id) FROM pack_tours.' . $entity . '_' . $operator)->fetchColumn();
            $relation_count = $this->pdo_master->query('SELECT COUNT(DISTINCT xml) FROM pack_tours.relation_' . $entity . ' WHERE operator = "' . $operator . '"')->fetchColumn();
            $count = $xml_count - $relation_count;

            $array[$operator]['name'] = $operator;
            $array[$operator]['title'] = $operator . ' (' . $count . ')';
        }

        return $array;
    }

    /**
     * Сохраняет статистику по спаршенным турам в кеш
     */
    public function saveStatistic()
    {
        $operators = $this->container->getParameter('operators');
        $array = array();
        $sum = 0;

        foreach ($operators as $operator) {
            // Для операторов со странами {operator}.country
            try {
                $array[$operator] = Db::rows('SELECT COUNT( xml_tour.xml_tour_' . $operator . '.id ) AS `rows` , `country` , ' . $operator . '.country.code, ' . $operator . '.country.name
FROM xml_tour.xml_tour_' . $operator . '
LEFT JOIN pack_tours.country_' . $operator . '
ON xml_tour.xml_tour_' . $operator . '.country = pack_tours.country_' . $operator . '.id
GROUP BY `country`
ORDER BY `country`');

                // Для операторов со странами xml_tour.iso_country
            } catch (\Exception $e) {
                $array[$operator] = Db::rows('SELECT COUNT( xml_tour.xml_tour_' . $operator . '.id ) AS `rows` , `country` , xml_tour.iso_country.countryName as `name`, xml_tour.iso_country.shortName2 as `code`
FROM xml_tour.xml_tour_' . $operator . '
LEFT JOIN xml_tour.iso_country
ON xml_tour.xml_tour_' . $operator . '.country = xml_tour.iso_country.id
GROUP BY `country`
ORDER BY `country`');
            }
        }

        // Подсчет общего количества туров
        foreach ($array as $key => $val) {
            foreach ($val as $country) {
                $sum += $country['rows'];
            }
        }

        $array['updated_at'] = date("Y-m-d H:i:s", time());
        $array['count_all'] = $sum;
        redis::set('parse_tour_statistic', $array);
    }

    /**
     * Возвращает список туров по оператору и параметру moved с ограничением по $count_tours
     * Параметр $operator_id необходим для физически партицированной таблицы xml_tour.tour
     *
     * @param $moved
     * @param $count_tours
     * @param $operator_id
     * @return array
     */
    public function getToursByOperatorAndMoved($moved, $count_tours, $operator_id)
    {
        return $this->pdo_backend->query(
            'SELECT * FROM xml_tour.xml_tour_' . $operator_id . ' WHERE moved = :moved LIMIT :limit',
            array(
                ':moved' => $moved,
                ':limit' => $count_tours,
            )
        );
    }

    /**
     * Выполняет вставку пакетных туров по operator_id из-за физического партицирования таблицы
     * "Умный" запрос написан для вставки количественно дублирующихся туров в поле at_all
     *
     * @param $array
     * @param $operator_id
     */
    public function saveTours($array, $operator_id)
    {
        $data_str = '';
        foreach ($array as $el) {
            unset($el['tour'], $el['spo'], $el['operator'], $el['end'], $el['params']);
            $data_str .= "('" . implode("','", $el) . "'), ";
        }
        $data_str = substr_replace($data_str , "", -2);

        $this->pdo_master->exec("INSERT INTO pack_tours.tour_" . $operator_id .
        " (cityFrom, room, hotel, star, resort_id, country_id, ration, start, nights, adults, childs, price, currency)" .
        " VALUES " . $data_str .
        " ON DUPLICATE KEY UPDATE price = CASE WHEN price > VALUES(price) THEN VALUES(price) ELSE price END, at_all = at_all + 1");
    }

    /**
     * Выполняет удаление мигрированных туров по id туров
     *
     * @param $tours
     */
    public function deleteMigratedTours($tours)
    {
        foreach ($tours as $operator_id => $ids) {
            $this->pdo_backend->exec('DELETE FROM xml_tour.xml_tour_' . $operator_id . ' WHERE id IN (' . implode(', ', $ids) . ')');
        }
    }

    /**
     * Выполняет изменения статуса мигрированных туров по id туров
     *
     * @param $tours
     * @param $moved
     */
    public function changeStatusMigratedTours($tours, $moved)
    {
        foreach ($tours as $operator_id => $ids) {
            $this->pdo_backend->exec('UPDATE xml_tour.xml_tour_' . $operator_id . ' SET moved = ' . $moved . ' WHERE id IN (' . implode(', ', $ids) . ')');
        }
    }

    /**
     * @param $param
     * @return string
     */
    public function addCity($param)
    {
        $stmt = $this->pdo_master->query('SELECT * FROM geo.resort WHERE name = :name AND country_id = :country_id');
        $stmt->bindParam('name', trim($param['name']));
        $stmt->bindParam('country_id', $param['country_id']);
        $stmt->execute();
        $row = $stmt->fetch();

        if (!empty($row)) {
            $result = 'Такой город уже существует';
        } else {
            $param['region_id'] = 0;
            $query_result = PdoHelper::InsertOrUpdate($this->pdo_master, 'geo.resort', $param);
            if ($query_result) {
                $result = 'Город успешно добавлен';
            } else {
                $result = 'Произошла ошибка при добавлении';
            }
        }

        return $result;
    }

    /**
     * @param $operator
     * @param $id
     * @return int|string
     */
    public function getCountryId($operator, $id)
    {
        $cache_key = 'CDC_country_' . $operator . '_' . $id;
//        $data = cache::get ($cache_key);
//        if ($data === false) {

        $stmt = $this->pdo_master->prepare('SELECT base FROM pack_tours.relation_country WHERE operator = :operator AND xml = :xml');
        $stmt->execute(array(':operator' => $operator, ':xml' => $id));
        $data = $stmt->fetchColumn();

        if ($data === null) {
            $data = 0;
        }
//            cache::set ($cache_key, $data, cache::TTL_HOUR);
//        }
        return $data;
    }

    /**
     * @param $operator
     * @param $id
     * @return bool|string
     */
    public function getCityId($operator, $id)
    {
        $cache_key = 'CDC_city_' . $operator . '_' . $id;
//        $data = cache::get ($cache_key);
//        if ($data === false) {

        $stmt = $this->pdo_master->prepare('SELECT base FROM pack_tours.relation_city WHERE operator = :operator AND xml = :xml');
        $stmt->execute(array(':operator' => $operator, ':xml' => $id));
        $data = $stmt->fetchColumn();

        if ($data === null) {
            $data = 0;
        }
//            cache::set ($cache_key, $data, cache::TTL_HOUR);
//        }
        return $data;
    }

    /**
     * @param $operator
     * @param $id
     * @return bool|string
     */
    public function getRoomId($operator, $id)
    {
//        $cache_key = 'CDC_room_' . $operator . '_' . $id;
//        $data = cache::get($cache_key);
//        if ($data === false) {

        $stmt = $this->pdo_master->prepare('SELECT base FROM pack_tours.relation_room WHERE operator = :operator AND xml = :xml');
        $stmt->execute(array(':operator' => $operator, ':xml' => $id));
        $data = $stmt->fetchColumn();

        if ($data === null) {
            $data = 0;
        }
//            cache::set($cache_key, $data, cache::TTL_HOUR);
//        }
        return $data;
    }

    /**
     * Возвращает tourobzor_hotel_id
     * по operator_id и xml_hotel_id
     *
     * @param $operator_id
     * @param $xml_id
     * @return bool|string
     */
    public function getHotelId($operator_id, $xml_id)
    {
//        $cache_key = 'CDC_hotel_' . $operator_id . '_' . $xml_id;
//        $data = cache::get ($cache_key);
//
//        if ($data === false) {

        $stmt = $this->pdo_master->prepare('SELECT base FROM pack_tours.relation_hotel WHERE operator = :operator AND xml = :xml');
        $stmt->execute(array(':operator' => $operator_id, ':xml' => $xml_id));
        $data = $stmt->fetchColumn();

        if ($data === null) {
            $data = 0;
        }
//            cache::set($cache_key, $data, cache::TTL_HOUR);
//        }
        return $data;
    }

    /**
     * @param $operator
     * @param $id
     * @return bool|string
     */
    public function getStarsId($operator, $id)
    {
//        $cache_key = 'CDC_start_' . $operator . '_' . $id;
//        $data = cache::get ($cache_key);
//
//        if ($data === false) {

        $stmt = $this->pdo_master->prepare('SELECT base FROM pack_tours.relation_star WHERE operator = :operator AND xml = :xml');
        $stmt->execute(array(':operator' => $operator, ':xml' => $id));
        $data = $stmt->fetchColumn();

        if ($data === null) {
            $data = 0;
        }
//            cache::set ($cache_key, $data, cache::TTL_HOUR);
//        }
        return $data;
    }

    /**
     * @param $operator
     * @param $id
     * @return bool|string
     */
    public function getRationId($operator, $id)
    {
//        $cache_key = 'CDC_ration_' . $operator . '_' . $id;
//        $data = cache::get ($cache_key);
//
//        if ($data === false) {

        $stmt = $this->pdo_master->prepare('SELECT base FROM pack_tours.relation_pansion WHERE operator = :operator AND xml = :xml');
        $stmt->execute(array(':operator' => $operator, ':xml' => $id));
        $data = $stmt->fetchColumn();

        if ($data === null) {
            $data = 0;
        }
//            cache::set ($cache_key, $data, cache::TTL_HOUR);
//        }
        return $data;
    }

    /**
     * Добавление отеля
     *
     * @param $param
     * @return string
     */
    public function addHotel($param)
    {
        $stmt = $this->pdo_master->query('SELECT * FROM base.hotel WHERE name = :name AND country_id = :country_id AND resort_id = :resort_id');
        $stmt->bindParam('country_id', $param['country_id']);
        $stmt->bindParam('resort_id', $param['resort_id']);
        $stmt->bindParam('name', trim($param['name']));
        $stmt->execute();
        $row = $stmt->fetch();

        if (!empty($row)) {
            $result = 'Такой отель уже существует';
        } else {
            $param['status'] = 0;
            $param['description'] = '';
            $param['booking'] = 0;

            $result = PdoHelper::InsertOrUpdate($this->pdo_master, 'base.hotel', $param);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function findAllWithCountryAndResort()
    {
        return $this->pdo_master->query('SELECT base.hotel.*, geo.country.name AS country_name, geo.resort.name AS resort_name
            FROM base.hotel
            LEFT JOIN geo.country
            ON base.hotel.country_id = geo.country.id
            LEFT JOIN geo.resort
            ON base.hotel.resort_id = geo.resort.id')
            ->fetchAll();
    }

    /**
     * @param $mixed
     * @param  null   $domElement
     * @param  null   $DOMDocument
     * @return string
     */
    public function xmlEncode($mixed, $domElement = null, $DOMDocument = null)
    {
        if (is_null($DOMDocument)) {
            $DOMDocument = new \DOMDocument;
            $DOMDocument->formatOutput = true;
            $this->xmlEncode($mixed, $DOMDocument, $DOMDocument);

            return $DOMDocument->saveXML();
        } else {
            if (is_array($mixed)) {
                foreach ($mixed as $index => $mixedElement) {
                    if (is_int($index)) {
                        if ($index === 0) {
                            $node = $domElement;
                        } else {
                            $node = $DOMDocument->createElement($domElement->tagName);
                            $domElement->parentNode->appendChild($node);
                        }
                    } else {
                        $plural = $DOMDocument->createElement($index);
                        $domElement->appendChild($plural);
                        $node = $plural;
//                        if (!(rtrim($index, '') === $index)) {
                        $singular = $DOMDocument->createElement(rtrim($index, 's'));
                        $plural->appendChild($singular);
                        $node = $singular;
//                        }
                    }

                    $this->xmlEncode($mixedElement, $node, $DOMDocument);
                }
            } else {
                $domElement->appendChild($DOMDocument->createTextNode($mixed));
            }
        }
    }

    /**
     * Защита от нового запуска XML Migrator пока
     * предыдущие задачи с ключом `migrator` не будут выполнены до конца +
     * защита от параллельного запуска migrator.php
     */
    public function protectionMigrator()
    {
//        if (Registry::get('gearman_enable')) {
//            $config_gearman = Registry::get('gearman');
//            $gearmanManager = new \Net_Gearman_Manager($config_gearman['host'] . ':' . $config_gearman['port']);
//            $status = $gearmanManager->status();
//
//            if ($status['migrator']['in_queue'] != 0 || $status['migrator']['jobs_running'] != 0) {
//                echo 'Защита от перезаписи. Дождитесь выполнения текущих задач c ключом `migrator`.', PHP_EOL;
//                exit;
//            }
//        }

        // Защита от запуска во время выполнения очистки таблиц (00:00:00)
        $time = date("H:i:s", time());
        if ($time > '22:00:00' && $time < "1:00:00") {
            echo 'Защита от запуска мигратора во время очистки таблиц.', PHP_EOL;
            exit;
        }

        if ($this->redis->get('migrator_is_active')/*redis::get('migrator_is_active') == 1*/) {
            echo 'Защита от запуска нескольких миграторов. Дождитесь выполнения текущего мигратора. ' . date("H:i:s Y-m-d", time()), PHP_EOL;
            exit;
        }

        $this->redis->set('migrator_is_active', 1);
        $this->redis->expire('migrator_is_active', 3600);
    }

    /**
     * Считает статистику по непроставленным связям для всех операторов по сущности $entity
     *
     * @param $entity
     * @return string
     */
    public function relationCount($entity)
    {
        if (!in_array($entity, array('city', 'country', 'hotel', 'pansion', 'room', 'star'))) {
            return false;
        }

        $key = $entity . 'stat';
        $count = $this->redis->get($key);

        if (!$count) {
            $operators = $this->container->getParameter('operators');
            $count = 0;
            foreach ($operators as $operator_id) {
                $operator_count = $this->pdo_master->query('SELECT COUNT(id) FROM pack_tours.' . $entity . '_' . $operator_id)->fetchColumn();
                $relation_count = $this->pdo_master->query('SELECT COUNT(DISTINCT xml) FROM pack_tours.relation_' . $entity . ' WHERE operator = "' . $operator_id . '"')->fetchColumn();
                $count = $count + ($operator_count - $relation_count);
            }
            $this->redis->set($key, $count);
            $this->redis->expire($key, 3600);
        }

        return '(' . $count . ')';
    }

    /**
     * Автоматически балансирует нагрузку с помощью управления кол-вом воркеров
     */
    public function automaticBalanced()
    {
        $gearman_enable = $this->container->getParameter('gearman_enable');
        $gearman_host = $this->container->getParameter('gearman_host');
        $gearman_port = $this->container->getParameter('gearman_port');
        // Получаем кол-во воркеров установленных в конфиге приложения
        $gearman_count_parser = $this->container->getParameter('gearman_count_parser');
        $gearman_count_save = $this->container->getParameter('gearman_count_save');

        if ($gearman_enable) {
            $gearmanManager = new \Net_Gearman_Manager($gearman_host . ':' . $gearman_port);
            $status = $gearmanManager->status();

            // Получаем кол-во активных воркеров на текущий момент
            $parser_capable_workers = $status['parser']['capable_workers'];
            $save_capable_workers = $status['save']['capable_workers'];

            // Проверяем, если меньше чем установлено в конфиге, значит добавляем недостающее кол-во воркеров
            if ($save_capable_workers < $gearman_count_save) {
                for ($i = 1; $i <= $gearman_count_save - $save_capable_workers; $i++) {
//                    $process = new Process('/usr/bin/php worker_save.php');
//                    $process->start();
//                    var_dump('save:', $process->getPid());
//                    $shell = shell_exec('nohup php worker_save.php &');
                    shell_exec('/usr/bin/nohup php worker_save.php >/dev/null 2>&1 &');
//                    $fp = popen('nohup php -f worker_save.php &', 'r');
//                    pclose($fp);
                }
            }

            // Проверяем, если меньше чем установлено в конфиге, значит добавляем недостающее кол-во воркеров
            if ($parser_capable_workers < $gearman_count_parser) {
                for ($i = 1; $i <= $gearman_count_parser - $parser_capable_workers; $i++) {
                    shell_exec('/usr/bin/nohup php worker_parser.php >/dev/null 2>&1 &');
//                    $shell = shell_exec('nohup php worker_parser.php &');
//                    $process = new Process('nohup php worker_parser.php &');
//                    $process->start();
//
//                    var_dump('parser:', $process->getPid());
                    //$shell = system('nohup php -q worker_parser.php &');
//                    $fp = popen('nohup php -f worker_parser.php &', 'r');
//                    pclose($fp);
                }
            }

//            if ($status['migrator']['capable_workers'] == 0) {
//                $fp = popen('nohup php -f worker_migrator.php &', 'r');
//                pclose($fp);
//            }

            // Проверяем, если задач по parser меньше чем 10 - запускаем, если больше - exit
            // Или если задач по save меньше 50 - запускаем, если больше - exit
            if ($status['parser']['in_queue'] > 50 || $status['save']['in_queue'] > 100) {
                echo 'Дождитесь выполнения текущих задач по ключам `parser` или `save`.' . PHP_EOL;
                exit;
            }
        }
    }
}
