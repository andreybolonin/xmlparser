<?php

namespace Andreybolonin\PackTourBundle;

use Buzz\Browser;
use Symfony\Component\DependencyInjection\Container;

abstract class XmlPullReader
{
    /**
     * @var array
     */
    private $arrayResultNodes = array();
    /**
     * @var array
     */
    private $arrayResultNodesOnlyAttr = array();
    /**
     * @var string
     */
    private $handlerFunction = '';
    /**
     * @var array
     */
    public $error = array();
    /**
     * @var bool
     */
    public $flgShowStat = false;
    /**
     * @var bool
     */
    private $flgReadingElement = false;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var array
     */
    protected $test_array = array();

    /**
     * @var
     */
    public $parser;

    /**
     * @param $operator_id
     * @return mixed
     */
    abstract public function getAndSaveSpoList($operator_id);

    /**
     * @param $node
     * @return mixed
     */
    abstract public function getAllTourFromSpo($node);

    /**
     * Вместо метода __construct(),
     * чтобы полностью уйти от зависимостей в конструкторах классов-потомков (AlfParser, AnextourParser ...)
     *
     * @param Container      $container
     * @param LogManager     $log_manager
     * @param Browser        $buzz
     * @param \PDO           $pdo_backend
     * @param \PDO           $pdo_master
     * @param \GearmanClient $gearman_client
     */
    public function setParams(Container $container, LogManager $log_manager, Browser $buzz, \PDO $pdo_backend, \PDO $pdo_master, \GearmanClient $gearman_client)
    {
        $this->container = $container;
        $this->log_manager = $log_manager;
        $this->buzz = $buzz;
        $this->pdo_backend = $pdo_backend;
        $this->pdo_master = $pdo_master;
        $this->gearman_client = $gearman_client;
    }

    /**
     * Конструктор для парсера
     */
    public function construct()
    {
        $this->parser = xml_parser_create();

        // Работаем в рамках текущего класса
        xml_set_object($this->parser, $this);

        // Назначаем обработчкиков событий
        xml_set_element_handler($this->parser, "nodeStart", "nodeEnd");

        // Запрещает автоматическое преобразование анализатором всех тегов в верхний регистр
        xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, false);

        // символьные данные
        xml_set_character_data_handler($this->parser,'nodeBody');
    }

    /**
     * Очищаем данные объекта
     */
    private function cleanObj()
    {
        $this->construct();

        $this->arrayResultNodes = array();
        $this->arrayResultOnlyAttr = array();
        $this->handlerFunction = '';
        $this->flgReadingElement = '';
        $this->error = '';
    }

    /**
     *
     */
    public function __destruct()
    {
        if (isset($this->reader)) {
            $this->reader->close();
        }
    }

    /**
     * Определяем массив искомых элементов
     *
     * @param $arrayResultNodes
     */
    public function setReadBanch($arrayResultNodes)
    {
        if (!empty($arrayResultNodes) && !is_array($arrayResultNodes)) {
            $arrayResultNodes = array($arrayResultNodes);
        }

        $this->arrayResultNodes = $arrayResultNodes;
        $this->setReadBanchOnlyAttr($this->arrayResultNodes);
    }

    /**
     * Определяем массив искомых элементов
     *
     * @param $arrayResultOnlyAttr
     * @return bool
     */
    public function setReadBanchOnlyAttr($arrayResultOnlyAttr)
    {
        if (empty($arrayResultOnlyAttr) || !is_array($arrayResultOnlyAttr)) {
            return false;
        }

        foreach ($arrayResultOnlyAttr as $el) {
            $arrayResultOnlyAttr[$el] = true;
        }

        $this->arrayResultOnlyAttr = $arrayResultOnlyAttr;
    }

    /**
     * Определяем ф-ю обработчки искомых элементов
     *
     * @param $funcName
     */
    public function setFunctionHandler($funcName)
    {
        $this->handlerFunction = $funcName;
    }

    /**
     * @param $file
     */
    public function readXmlByUrl($file)
    {
        $this->file = $file;
        $this->fileSize = null;
        $stat = false;
        // Ищем размер файла
        if (is_file($this->file)) {
            $stat = @stat($this->file);
        }
        if ($stat !== false) {
            $this->fileSize = intval($stat[7] / 1024); // Kb
        }
    }

    /**
     * Начало блока
     *
     * @param $parser
     * @param $name
     * @param $attrs
     */
    public function nodeStart($parser, $name, $attrs)
    {
        $this->name = $name;

        // Начинаем читать элемент
        // Учитываем что одноимённых элементов может быть много
        $this->flgReadingElement[$name][] = '';

        $this->node[$this->name]['body'] = '';
        if (in_array($name, $this->arrayResultNodes) && !empty($attrs)) {
            $this->node[$name]['attributes'] = $attrs;

            // Если есть в списке только атрибуты - отдаём их сразу
            if (isset($this->arrayResultOnlyAttr[$name])) {
                if ($this->handlerFunction) {
                    call_user_func(
                        $this->handlerFunction,
                        array(
                            'name' => $name,
                            'node' => array('attributes' => $attrs)
                        )
                    );
                }
            }

        }
    }

    /**
     * Тело блока
     *
     * @param $parser
     * @param $body
     */
    public function nodeBody($parser, $body)
    {
        if (!isset($this->node[$this->name]['body'])) {
            $this->node[$this->name]['body'] = '';
        }
        $this->node[$this->name]['body'] .= $body;
    }

    /**
     * Окончание блока
     *
     * @param $parser
     * @param $name
     */
    public function nodeEnd($parser, $name)
    {
        // Если это искомый элемент отдаём ф-и обработчику
        if (in_array($name, $this->arrayResultNodes)) {
            // Отправляем элемент ф-и обработчику только атрибуты
            if (isset($this->arrayResultOnlyAttr[$name])) {

                // Отправляем элемент ф-и обработчику все данные
            } else {

                if ($this->handlerFunction) {
                    call_user_func($this->handlerFunction, array(
                        'name' => $name,
                        'node' => array(
                            'attributes' => empty($this->node[$name]['attributes']) ? null : $this->node[$name]['attributes'],
                            'body' => empty($this->node[$name]['body']) ? null : $this->node[$name]['body']
                        )
                    ));
                }
                unset($this->node[$name]);

            }
        }

        // Такого элемента не существует
        if (!isset($this->flgReadingElement[$name])) {
            xml_parser_free($parser);
        }

        // Закончили читать элемент
        $num = count($this->flgReadingElement[$name]) - 1;
        if ($this->flgReadingElement[$name][$num] > 1) {
            unset($this->flgReadingElement[$name][$num]);
        } else {
            // Убираем родительский элемент
            unset($this->flgReadingElement[$name]);
        }
    }

    /**
     * @return array|bool
     */
    public function getXmlAssoc()
    {
        $this->construct();
        if (!($fp = fopen($this->file, "r"))) {
            print "Could not open XML input";
            $this->cleanObj();

            return false;
        }

        $error = '';
        while ($data = fread($fp, 4096)) {
            if (!xml_parse($this->parser, $data, feof($fp))) {
                // Ошибка чтения
                $error =
                    'XML error: ' . xml_error_string(xml_get_error_code($this->parser))
                    . ' at line '
                    . xml_get_current_line_number($this->parser);
                break;
            }
        }

        if (empty($this->flgReadingElement)) {
            // Флаг последней итерации
            call_user_func($this->handlerFunction, array('name' => 'eof'));
        } else {
            // Флаг ошибки парсинга (нет закрывающего тега)
            $message = 'File: ' . $this->file;
            #$message .= ' ' . implode(', ', $this->flgReadingElement);
            if (!empty($error)) {
                $message .= ' Нет закрывающих тегов: ' . $error;
            }
            call_user_func($this->handlerFunction, array('error' => $message));
        }

        xml_parser_free($this->parser);

        // Возвращаем данные о обработанном файле
        $workFile = array(
            'fileName' => $this->file,
            'fileSize' => $this->fileSize
        );

        // Очищаем данные объекта
        $this->cleanObj();

        return $workFile;
    }

    /**
     * Пост обработка парсинга SPO из XML файла
     *
     * @param $spo
     * @param $this_modul_xml
     * @param bool $current_spo
     */
    public function postXmlParsing($spo, $this_modul_xml, $current_spo = true)
    {
        // Дата парсинга spo и кол-во актуальных туров
        $spo['date_parsed'] = date("Y-m-d");
        $spo['actual_tour'] = $this->actualTour;

        // Закканчиваем работу с текущим файлом
        $spo['status'] = 'end';
        if ($current_spo && isset($this_modul_xml->currentSpo)) {
            $spo['spo'] = $this_modul_xml->currentSpo;
        }

        // Определяем какой статус присвоить файлу по итогу обработки
        if ($this_modul_xml->notLoad === true) {
            if (file_exists($spo['path'])) {
                unlink($spo['path']);
            }
            $spo['status'] = 'notload';
            $spo['path'] = null;
        } else {
            // Добавление в список спо
            if (file_exists($spo['path'])) {
                unlink($spo['path']);
            }
            $spo['status'] = 'parsed';
            $spo['path'] = null;
        }
        $stmt = $this->pdo_master->prepare('UPDATE xml_tour.spo SET status = :status, path = :path, date_parser = :date_parsed, actual_tour = :actual_tour WHERE id = :id');
        $stmt->execute(array(
            'id' => $this->spo['id'],
            'status' => $spo['status'],
            'path' => $spo['path'],
            'date_parsed' =>  $spo['date_parsed'],
            'actual_tour' => $spo['actual_tour']
        ));

        $time = (microtime(true) - $this->time);
        $speed = number_format($this->countTour / $time, 0, '.', '');
        echo PHP_EOL .
            'Spo: ' . $this->currentSpoId .
            '  Туров: ' . $this->countTour .
            '  Актуальных туров: ' . $this->actualTour .
            '  Скорость парсинга(туров/сек): ' . $speed .
            PHP_EOL;

        $this->log_manager->saveLogs();
    }

    /**
     * Проверяет расширение перевадаемого пути файла,
     * если расширение входит в список допустимых - true, если не входит - false
     *
     * @param $filepath
     * @param  null $needle_ext
     * @return bool
     */
    public function checkExtension($filepath, $needle_ext = null)
    {
        if ($needle_ext) {
            $ext_array[] = $needle_ext;
        } else {
            $ext_array = array('xml', 'zip', 'gz', 'rar');
        }

        $search_ext = pathinfo($filepath, PATHINFO_EXTENSION);

        if (in_array($search_ext, $ext_array)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function getFileExtension($filename)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        return $extension;
    }

    /**
     * Устанавливает начальные значения переменных класса
     * и стартовый статус для SPO
     *
     * @internal param $spo
     */
    public function intializeParams()
    {
        $this->time = microtime(true);

        $this->CountActualTour = 0;
        $this->variant = array();
        $this->hotelServiceVariant = array();
        $this->flightServiceVariant = array();
        $this->flight = array();
        $this->tempTourOne = array();
        $this->eachFiftiesTour = array();
        $this->eachTour = array();
        $this->countActualDate = 0;
        $this->countTour = 0;
        $this->actualTour = 0;
        $this->oldTour = 0;
        $this->countIdService = 0;
        $this->currentSpoId = html_entity_decode($this->spo['id']);
        $this->spoId = $this->spo['id'];
        $this->currentSpoAuto = $this->spo['id'];
        $this->curentSpoFile = $this->spo['path'];
        $this->noIdInCatalog = false;
        $this->errorTour = false;
        $this->notLoad = false;
        $this->catalogNoId = false;
        $this->tour_type = null;
        $this->spoName = null;
        $this->migrate_on_fly = 0;

        $stmt = $this->pdo_master->prepare('UPDATE xml_tour.spo SET status = :status WHERE id = :id');
        $stmt->execute(array('id' => $this->spo['id'], 'status' => 'start'));
    }

    /**
     *  Запись стека туров
     *
     * @param $operator_id
     */
    public function saveTours($operator_id)
    {
        $xml_insert_count = $this->container->getParameter('xml_insert_count');
        $gearman_enable = $this->container->getParameter('gearman_enable');

        if (count($this->eachFiftiesTour) == $xml_insert_count) {
            $this->actualTour += $xml_insert_count;
            if ($gearman_enable) {
                $this->gearman_client->doHighBackground('save', serialize(array(
                    'eachFiftiesTour' => $this->eachFiftiesTour,
                    'operator_id' => $operator_id
                )));
            } else {
                PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.xml_tour_' . $operator_id, $this->eachFiftiesTour);
            }
            $this->eachFiftiesTour = array();
        }
    }

    /**
     * Запись оставшихся туров
     *
     * @param $node
     * @param $operator_id
     */
    public function saveLastTours($node, $operator_id)
    {
        $gearman_enable = $this->container->getParameter('gearman_enable');

        if (isset($node['name']) && $node['name'] == 'eof') {
            if (!empty($this->eachFiftiesTour)) {
                $this->actualTour += count($this->eachFiftiesTour);
                if ($gearman_enable) {
                    $this->gearman_client->doHighBackground('save', serialize(array(
                        'eachFiftiesTour' => $this->eachFiftiesTour,
                        'operator_id' => $operator_id
                    )));
                } else {
                    PdoHelper::MultiInsert($this->pdo_backend, 'xml_tour.xml_tour_' . $operator_id, $this->eachFiftiesTour);
                }
                $this->eachFiftiesTour = array();
            }
        }

        // Файл не докачан
        if (isset($node['error'])) {
            $this->log_manager->log(LogManager::ERR, $operator_id, LogManager::NOT_LOAD, $this->currentSpoId);
            $this->notLoad = true;
        }
    }

    /**
     * @param  array      $spo
     * @return mixed|void
     */
    public function params($spo)
    {
        $this->spo = $spo;
        $this->initializeArray();
        $this->intializeParams($spo);

        if ($this->readXmlByUrl($spo['path']) === false) {
            $this->log_manager->log(LogManager::ERR, CRON_PARSE, $this->spo['operator'], LogManager::ERROR_XML, $spo['url']);
            $spo['status'] = 'error';
            $this->spo_manager->insertOrUpdateList($spo);

            return false;
        }

        $this->setReadBanch($this->xml_read_keys);
        $this->setFunctionHandler(array($this, 'getAllTourFromSpo'));
        $this->getXmlAssoc();

        $this->postXmlParsing($spo, $this);
    }

    /**
     * Инициализация справочных данных оператора
     *
     * @param  string     $entity
     * @return mixed|void
     */
    public function initializeArray($entity = 'all')
    {
        $operator_id = $this->spo['operator'];

        if ($entity == 'city' || $entity == 'all') {
            $region = $this->pdo_master->query('SELECT * FROM pack_tours.city_' . $operator_id)->fetchAll();
            foreach ($region as $val) {
                $this->region[htmlspecialchars_decode($val['name'])] = $val['id'];
            }
        }

        if ($entity == 'country' || $entity == 'all') {
            $country = $this->pdo_backend->query('SELECT * FROM xml_tour.iso_country')->fetchAll();
            foreach ($country as $val) {
                $this->country[$val['shortName']] = array(
                    'id' => $val['id']
                );
            }

            $country_self = $this->pdo_master->query('SELECT * FROM pack_tours.country_' . $operator_id)->fetchAll();
            foreach ($country_self as $val) {
                $this->countrySelf[$val['code']] = $val['id'];
            }
        }

        if ($entity == 'hotel' || $entity == 'all') {
            $hotel = $this->pdo_master->query('SELECT * FROM pack_tours.hotel_' . $operator_id)->fetchAll();
            foreach ($hotel as $val) {
                $this->hotel[$val['id']] = '';
            }
        }

        if ($entity == 'pansion' || $entity == 'all') {
            $pansion = $this->pdo_master->query('SELECT * FROM pack_tours.pansion_' . $operator_id)->fetchAll();
            foreach ($pansion as $val) {
                $this->pansionIds[$val['id']] = '';
            }
        }

        if ($entity == 'room' || $entity == 'all') {
            $room = $this->pdo_master->query('SELECT * FROM pack_tours.room_' . $operator_id)->fetchAll();
            foreach ($room as $val) {
                $this->room[$val['id']] = '';
            }

            $room_ids = $this->pdo_master->query('SELECT * FROM pack_tours.room_ids_' . $operator_id)->fetchAll();
            foreach ($room_ids as $val) {
                $this->roomIds[$val['id']] = '';
            }

            $roomAccomod = $this->pdo_master->query('SELECT * FROM pack_tours.room_accomodations_' . $operator_id)->fetchAll();
            foreach ($roomAccomod as $val) {
                $this->roomAccomodation[$val['id']] = $val;
            }
        }

        if ($entity == 'star' || $entity == 'all') {
            $hotel_star = $this->pdo_master->query('SELECT * FROM pack_tours.star_' . $operator_id)->fetchAll();
            foreach ($hotel_star as $val) {
                $this->hotelStar[$val['star']] = $val['id'];
            }
        }

        if ($entity == 'stay_type' || $entity == 'all') {
            $stay_type = $this->pdo_master->query('SELECT * FROM pack_tours.stay_type_' . $operator_id)->fetchAll();
            foreach ($stay_type as $val) {
                $this->stayTypeIds[$val['id']] = '';
            }
        }

        if ($entity == 'tour_type' || $entity == 'all') {
            $tour_type = $this->pdo_master->query('SELECT * FROM pack_tours.tourtype_' . $operator_id)->fetchAll();
            foreach ($tour_type as $val) {
                $this->tourtype[$val['name']] = $val['id'];
            }
        }
    }

    /**
     * Добавляет операторский город
     *
     * @param $data
     */
    public function addCity($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.city_' . $this->spo['operator'], $data);
        $this->initializeArray('city');
    }

    /**
     * Добавляет операторскую страну
     *
     * @param $data
     */
    public function addCountry($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.country_' . $this->spo['operator'], $data);
        $this->initializeArray('country');
    }

    /**
     * Добавляет операторский отель
     *
     * @param $data
     */
    public function addHotel($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.hotel_' . $this->spo['operator'], $data);
        $this->initializeArray('hotel');
    }

    /**
     * Добавляет операторский тип питания
     *
     * @param $data
     */
    public function addPansion($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.pansion_' . $this->spo['operator'], $data);
        $this->initializeArray('pansion');
    }

    /**
     * Добавляет операторскую комнату
     *
     * @param $data
     */
    public function addRoom($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.room_' . $this->spo['operator'], $data);
        $this->initializeArray('room');
    }

    /**
     * Добавляет операторскую звездность
     *
     * @param $data
     */
    public function addStar($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.star_' . $this->spo['operator'], $data);
        $this->initializeArray('star');
    }

    /**
     * Добавляет операторский тип размещения
     *
     * @param $data
     */
    public function addStayType($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.stay_type_' . $this->spo['operator'], $data);
        $this->initializeArray('stay_type');
    }

    /**
     * Добавляет операторский тип тура
     *
     * @param $data
     */
    public function addTourType($data)
    {
        PdoHelper::InsertOrUpdate($this->pdo_master, 'pack_tours.tourtype_' . $this->spo['operator'], $data);
        $this->initializeArray('tour_type');
    }

    /**
     * Логирование ошибки города
     */
    public function errorCity()
    {
        $this->errorTour = true;
        $this->log_manager->log(LogManager::WARN, $this->spo['operator'], LogManager::EMPTY_CITY, $this->currentSpoId);
    }

    /**
     * Логирование ошибки страны
     */
    public function errorCountry()
    {
        $this->errorTour = true;
        $this->log_manager->log(LogManager::WARN, $this->spo['operator'], LogManager::EMPTY_COUNTRY_TAG, $this->currentSpoId);
    }

    /**
     * Логирование ошибки отеля
     */
    public function errorHotel()
    {
        $this->errorTour = true;
        $this->log_manager->log(LogManager::WARN, $this->spo['operator'], LogManager::EMPTY_HOTEL, $this->currentSpoId);
    }

    /**
     * Логирование ошибки отеля
     */
    public function errorPansion()
    {
        $this->errorTour = true;
        $this->log_manager->log(LogManager::WARN, $this->spo['operator'], LogManager::EMPTY_PANSION, $this->currentSpoId);
    }

    /**
     * Логирование ошибки комнаты
     */
    public function errorRoom()
    {
        $this->errorTour = true;
        $this->log_manager->log(LogManager::WARN, $this->spo['operator'], LogManager::EMPTY_ROOM, $this->currentSpoId);
    }

    /**
     * Логирование ошибки типа размещения
     */
    public function errorStayType()
    {
        $this->errorTour = true;
        $this->log_manager->log(LogManager::WARN, $this->spo['operator'], LogManager::EMPTY_STAY_TYPE, $this->currentSpoId);
    }

    /**
     * Function initializeArray implements Doctrine methods
     * Deprecated because to be removed
     *
     * @param string $entity
     * @internal param $operator_id
     * @deprecated
     */
    public function initializeArray_doctrine($entity = 'all')
    {
        $operator_id = $this->spo->getOperator();

        if ($entity == 'city' || $entity == 'all') {
            $city = $this->doctrine->getRepository('AndreyboloninPackTourBundle:City' . ucfirst($operator_id))->findAll();
            foreach ($city as $val) {
                $this->city[$val->getName()] = $val;
            }
        }

        if ($entity == 'country' || $entity == 'all') {
            $country = $this->doctrine->getRepository('AndreyboloninPackTourBundle:Country' . ucfirst($operator_id))->findAll();
            foreach ($country as $val) {
                $this->country[$val->getName()] = $val;
            }
        }

        if ($entity == 'hotel' || $entity == 'all') {
            $hotel = $this->doctrine->getRepository('AndreyboloninPackTourBundle:Hotel' . ucfirst($operator_id))->findAll();
            foreach ($hotel as $val) {
                $this->hotel[$val->getId()] = $val;
            }
        }

        if ($entity == 'star' || $entity == 'all') {
            $star = $this->doctrine->getRepository('AndreyboloninPackTourBundle:Star' . ucfirst($operator_id))->findAll();
            foreach ($star as $val) {
                $this->star[$val->getStar()] = $val;
            }
        }

        if ($entity == 'room' || $entity == 'all') {
            $room = $this->doctrine->getRepository('AndreyboloninPackTourBundle:Room' . ucfirst($operator_id))->findAll();
            foreach ($room as $val) {
                $this->room[$val->getId()] = $val;
            }
        }

        if ($entity == 'stay_type' || $entity == 'all') {
            $stay_type = $this->doctrine->getRepository('AndreyboloninPackTourBundle:StayType' . ucfirst($operator_id))->findAll();
            foreach ($stay_type as $val) {
                $this->stay_type[$val->getId()] = $val;
            }
        }

        if ($entity == 'pansion' || $entity == 'all') {
            $pansion = $this->doctrine->getRepository('AndreyboloninPackTourBundle:Pansion' . ucfirst($operator_id))->findAll();
            foreach ($pansion as $val) {
                $this->stay_type[$val->getId()] = $val;
            }
        }

        if ($entity == 'tour_type' || $entity == 'all') {
            $tour_type = $this->doctrine->getRepository('AndreyboloninPackTourBundle:Tourtype' . ucfirst($operator_id))->findAll();
            foreach ($tour_type as $val) {
                $this->tour_type[$val->getName()] = $val;
            }
        }
    }

}
