<?php

namespace Andreybolonin\PackTourBundle;

class PdoHelper
{

    /**
     * TODO Добавить экранирование символов как при использованиии bindParam или bindValue
     *
     * @param \PDO $pdo
     * @param $table_name
     * @param $data
     * @return int
     */
    public static function InsertOrUpdate(\PDO $pdo, $table_name, $data)
    {
        $sql = '';
        $bind = array();
        $sql .= 'INSERT INTO ' . $table_name . ' SET ';

        $setData = array( );
        foreach ($data as $column => $value) {
            $setData[] = "`{$column}` = :{$column}";
            $bind[':' . $column] = $value;
        }

        $sql .= implode ( ',', $setData );
        $sql .= ' ON DUPLICATE KEY UPDATE ' . implode ( ',', $setData );
        $stmt = $pdo->prepare($sql);

        return $stmt->execute($bind);
    }

    /**
     * Метод выполняет вставку нескольких записей за один запрос.
     * За основу взят метод Db::insertOrUpdateMulty()
     * Если выдает ошибку "Notice: array to string conversion", нужно проверить входные массивы массива $array,
     * они не должны содержать в себе вложенных массивов, только ключ => значение.
     *
     * @param $array
     * @param $table_name
     * @param  \PDO  $pdo
     * @return mixed
     */
    public static function MultiInsert(\PDO $pdo, $table_name, $array)
    {
        $sql = '';
        $count = 0;

        $sql .= 'INSERT INTO ' . $table_name . ' ';
        foreach ($array as $key => $data) {
            if ($count == 0) {
                $sql .= '(`' . implode("`,`", array_keys($data)) . '`)';
                $sql .= ' VALUES (\'' . implode("','", $data) . '\')';
            } else {
                $sql .= ', (\'' . implode("','",$data) . '\')';
            }

            $count++;
        }

        return $pdo->exec($sql);
    }

    /**
     * @param \PDO $pdo
     * @param $array
     * @param $table_name
     * @return mixed
     */
    public static function MultiInsertOrUpdate(\PDO $pdo, $table_name, $array)
    {
        $sql = '';
        $count = 0;

        $sql .= 'INSERT INTO ' . $table_name . ' ';
        foreach ($array as $key => $data) {
            if ($count == 0) {
                $sql .= '(`' . implode("`,`", array_keys($data)) . '`)';
                $sql .= ' VALUES (\'' . implode("','",$data) . '\') ';
                foreach ($data as $column => $value) {
                    $bind[':' . $column] = $value;
                    $setDataDuplicate[] = "`{$column}` = VALUES(`{$column}`)";
                }
            } else {
                $sql .= ' , (\'' . implode("','",$data) . '\') ';
            }

            $count++;
        }
        $sql .=  ' ON DUPLICATE KEY UPDATE ' . implode ( ',', $setDataDuplicate ) ;

        return $pdo->exec($sql);
    }

}
