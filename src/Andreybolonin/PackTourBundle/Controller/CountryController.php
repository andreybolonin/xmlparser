<?php

namespace Andreybolonin\PackTourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CountryController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $pdo = $this->get('pdo_master');
        $tour_manager = $this->get('tour_manager');

        $operators = $tour_manager->getOperatorsWithCount('country');
        $relations_count = $pdo->query('SELECT COUNT(id) FROM pack_tours.relation_country')->fetchColumn();

        return array(
            'operators' => $operators,
            'relations_count' => $relations_count
        );
    }

    /**
     * @Template()
     */
    public function ajaxAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response_data = array();
        $response = new JsonResponse();
        $operator_id = $request->get('operator_id');
        $tour_manager = $this->get('tour_manager');

        if ($operator_id) {
            $response_data = $tour_manager->getCountryListByOperatorId($operator_id);
        }

        $response->setData($response_data)->send();
    }

    /**
     * @Template()
     */
    public function manageRelationAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response = new JsonResponse();
        $pdo = $this->get('pdo_master');
        $tour_manager = $this->get('tour_manager');
        $user_id = $this->get('security.context')->getToken()->getUserName();

        $action = $request->get('action');
        $operator_id = $request->get('operator_id');
        $xml = $request->get('xml');
        $base = $request->get('base');

        if ($action == 'add' && $operator_id && $base && $xml) {
            $sth = $pdo->prepare('INSERT INTO pack_tours.relation_country (operator, base, xml) VALUES (:operator, :base, :xml)');
            $sth->bindValue('operator', $operator_id);
            $sth->bindValue('base', $base);
            $sth->bindValue('xml', $xml);
            $sth->execute();
            $relation_id = $pdo->lastInsertId();
        }

        if ($action == 'remove' && $operator_id && $xml) {
            $sth = $pdo->prepare('SELECT id FROM pack_tours.relation_country WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $xml);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
            $relation_id = $sth->fetchColumn();

            $sth = $pdo->prepare('DELETE FROM pack_tours.relation_country WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $xml);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
        }

        // Логирование
        $sth = $pdo->prepare('INSERT INTO pack_tours.relation_log (entity_id, relation_id, action, user_id, created_at) VALUES (:entity_id, :relation_id, :action, :user_id, :created_at)');
        $sth->bindValue('entity_id', 'country');
        $sth->bindValue('relation_id', $relation_id);
        $sth->bindValue('action', $action);
        $sth->bindValue('user_id', $user_id);
        $sth->bindValue('created_at', date('Y-m-d H:i:s'));
        $sth->execute();

        $response_data = $tour_manager->getCountryListByOperatorId($operator_id);
        $response->setData($response_data)->send();
    }

}
