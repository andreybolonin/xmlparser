<?php

namespace Andreybolonin\PackTourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $operators = $this->container->getParameter('operators');
        $pdo = $this->get('pdo_backend');
        $redis = $this->container->get('snc_redis.default');

        $spo_count = $pdo->query('SELECT COUNT(id) FROM xml_tour.spo')->fetchColumn();
        $parse_tours = $redis->get('parse_tour_statistic');
        $pack_tours = $redis->get('pack_tour_statistic');

        return array(
            'operators' => count($operators),
            'spo' => $spo_count,
            'parse_tours' => $parse_tours,
            'pack_tours' => $pack_tours
        );
    }

    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'AndreyboloninPackTourBundle:Default:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }

    /**
     * @Template()
     */
    public function spoAction()
    {
        $pdo = $this->get('pdo_backend');

        $spo_by_status = $pdo->query('SELECT COUNT(id) AS rows, spo.status FROM xml_tour.spo GROUP BY status')->fetchAll();
        $spo_by_operators = $pdo->query('SELECT COUNT(id) AS rows, spo.operator FROM xml_tour.spo GROUP BY operator')->fetchAll();

        return array(
            'rows' => $spo_by_status,
            'operators' => $spo_by_operators
        );
    }

    /**
     * Статистика спаршенных туров
     *
     * @Template()
     */
    public function parseToursAction()
    {
        $redis = $this->container->get('snc_redis.default');
        $parse_tour = $redis->get('parse_tour_statistic');

        return array(
            'parse_tour' => $parse_tour,
            'operators' => array()
        );
    }

    /**
     * Статистика пакетных туров
     *
     * @Template()
     */
    public function packToursAction()
    {
        $operators = $this->container->getParameter('operators');
        $pdo = $this->get('pdo_master');

        $pack_tour = array();
        $operators_enable = array();

        foreach ($operators as $operator) {
            $query = $pdo->query('SELECT COUNT(pack_tours.tour_' . $operator . '.id) as `rows`, `country_id`, geo.country.name as `name`, at_all
                    FROM pack_tours.tour_' . $operator . '
                    LEFT JOIN geo.country
                    ON pack_tours.tour_' . $operator . '.country_id = geo.country.id
                    GROUP BY `country_id`
                    ORDER BY `country_id`');
            if ($query) {
                $pack_tour[$operator] = $query->fetchAll();
                $operators_enable[] = $operator;
            }
        }

        return array(
            'pack_tour' => $pack_tour,
            'operators' => $operators_enable
        );
    }

    /**
     * Список активных операторов
     *
     * @Template()
     */
    public function operatorsAction()
    {
        $operators = $this->container->getParameter('operators');

        return array('operators' => $operators);
    }

    /**
     * Обязательные параметры пакетных и экскурсионных туров
     *
     * @Template()
     */
    public function tourParamsAction()
    {
        return array();
    }

    /**
     * Лог мигратора
     *
     * @Template()
     */
    public function migratorErrorsAction()
    {
        $pdo = $this->get('pdo_master');
        $migrator_errors = $pdo->query('SELECT * FROM pack_tours.migration_log')->fetchAll();

        return array('migrator_errors' => $migrator_errors);
    }

    /**
     * Вывод лога связей
     *
     * @Template()
     */
    public function relationLogAction($period)
    {
        $log_manager = $this->get('log_manager');
        $log = $log_manager->getRelationLogs($period);

        return array('log' => $log);
    }

    /**
     * Шаблон xml для экскурсионных туров
     */
    public function xmlExcursionsToursAction()
    {
        $tour = array(
            'id' => 'Идентификатор тура',
            'name' => 'Имя тура',
            'tour_type' => 'Тип отдыха',
            'date_start' => 'Дата начала',
            'night_count' => 'Кол-во ночей',
            'price' => 'Цена',
            'currency' => 'Валюта (USD, EUR, RUB, UAH)',
            'city' => 'Город выезда',
            'countries' => array(
                'country' => 'Посещаемый город №1',
                'country' => 'Посещаемый город №1'
            ),
            'tour_program' => array(
                'day_1' => 'Прилет в а.э. Сбор группы, встреча с гидом.
Трансфер в Коломбо.
Обзорная экскурсия по Коломбо – проезжая через город Вы увидите форт и Петтах – торговая площадка и линию деревьев корицы вдоль дороги, Индуистский храм в Си-Стрит, посещение магазинов и ювелирных лавок, Голландской церкви ( построенной в 1749 г). Посещение Площади независимости, здания Индепенденс-холл, конференц – центра, экспоцентра, храма Гангарама, копии статуи Будды.
Возвращение в отель.',
                'day_2' => 'Завтрак. Свободное время',
                'day_3' => 'Завтрак.
Трансфер в Тринкомали. Отдых на пляже.
Заселение в отель в Нилавели.'
            )
        );
        $array_collection['tours'] = array(
            $tour,
            $tour,
            $tour
        );

        $tour_manager = $this->get('tour_manager');

        $jsonContent = $tour_manager->xmlEncode($array_collection);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/xml');
        $response->setContent($jsonContent);
        $response->send();

        return array();
    }

    /*
     * TODO May be delete this old action?
     */
    public function paramAction()
    {
        $response = new JsonResponse();
        //$bookingGroupName = array(1 => 'Общие', 5 => 'Спорт и отдых', 2 => 'Сервисы', 3 => 'Интернет', 4 => 'Парковка');
        //$params = db::rows ('SELECT f.id,s.id group_id,f.name_long, s.name FROM base.params f join base.params_group s on f.group_id = s.id');
        //$bookinParam = db::rows ('SELECT f.*,s.id isset FROM relation.hotel_list_service f left join relation.params s on f.id = s.booking_param');

        if ( isset($_REQUEST['param']) && $_REQUEST['param'] == 'selected_param' ) {
            if ($_REQUEST['action'] == 'insert') {
                $main_param = explode('|',$_REQUEST['main_param']);
                $data['param'] = $main_param[0];
                $data['group'] = $main_param[1];
                $data['booking_param'] = $_REQUEST['book_param'];
                $data['booking_group'] = $_REQUEST['bookParam'];

                db::insert($data, 'relation.params');
            }
            if ($_REQUEST['action'] == 'delete') {
                db::exec('DELETE FROM relation.params WHERE id = :id', array(':id' => $_REQUEST['book_param']));
            }

            $object['bookinParam'] = db::rows ('SELECT f.*,
                                             s.id isset ,
                                             t.name_long rel_param
                                        FROM relation.hotel_list_service f
                                        left join relation.params s
                                          on s.booking_param = f.id
                                        left join base.params t on s.param = t.id
                                      WHERE f.group = :group',
                array(':group' => $_REQUEST['bookParam']));
            $object['param'] = db::rows ('SELECT f.id,
                                       s.id group_id,
                                       f.name_long,
                                       s.name
                                    FROM base.params f
                                    join base.params_group s
                                       on f.group_id = s.id
                                    Where s.id not in (\'4\',\'100\',\'106\',\'111\') ORDER BY f.name_long ASC ');

            $response->setData($object)->send();
        }
    }

}
