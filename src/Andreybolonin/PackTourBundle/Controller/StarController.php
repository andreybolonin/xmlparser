<?php

namespace Andreybolonin\PackTourBundle\Controller;

use Andreybolonin\PackTourBundle\Operator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StarController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $pdo = $this->get('pdo_master');
        $tour_manager = $this->get('tour_manager');

        $operators = $tour_manager->getOperatorsWithCount('star');
        $relations_count = $pdo->query('SELECT COUNT(id) FROM pack_tours.relation_star')->fetchColumn();

        return array(
            'relations_count' => $relations_count,
            'operators' => $operators
        );
    }

    /**
     * @Template()
     */
    public function ajaxAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response_data = array();
        $response = new JsonResponse();
        $operator_id = $request->get('operator_id');
        $tour_manager = $this->get('tour_manager');

        if ($operator_id) {
            $response_data = $tour_manager->getStarListByOperatorId($operator_id);
        }

        $response->setData($response_data)->send();
    }

    public function manageRelationAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response = new JsonResponse();
        $pdo = $this->get('pdo_master');
        $user_id = $this->get('security.context')->getToken()->getUserName();
        $tour_manager = $this->get('tour_manager');

        $operator_id = $request->get('operator_id');
        $action = $request->get('action');
        $xml = $request->get('xml');
        $base = $request->get('base');

        if ($action == 'add' && $operator_id && $base && $xml) {
            $sth = $pdo->prepare('INSERT INTO pack_tours.relation_star (operator, base, xml) VALUES (:operator, :base, :xml)');
            $sth->bindValue('operator', $operator_id);
            $sth->bindValue('base', $base);
            $sth->bindValue('xml', $xml);
            $sth->execute();
            $relation_id = $pdo->lastInsertId();
        }

        if ($action == 'remove' && $operator_id && $xml) {
            $sth = $pdo->prepare('SELECT id FROM pack_tours.relation_star WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $xml);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
            $relation_id = $sth->fetchColumn();

            $sth = $pdo->prepare('DELETE FROM pack_tours.relation_star WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $xml);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
        }

        // Логирование
        $sth = $pdo->prepare('INSERT INTO pack_tours.relation_log (entity_id, relation_id, action, user_id, created_at) VALUES (:entity_id, :relation_id, :action, :user_id, :created_at)');
        $sth->bindValue('entity_id', 'star');
        $sth->bindValue('relation_id', $relation_id);
        $sth->bindValue('action', $action);
        $sth->bindValue('user_id', $user_id);
        $sth->bindValue('created_at', date('Y-m-d H:i:s'));
        $sth->execute();

        $response_data = $tour_manager->getStarListByOperatorId($operator_id);
        $response->setData($response_data)->send();
    }

}
