<?php

namespace Andreybolonin\PackTourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TourtypeController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $others = BaseMonCatalogTourtype::getAll();

        return array('others' => $others);
    }

    /**
     * @Route("/ajax")
     * @Template()
     */
    public function ajaxAction()
    {
        $request = Request::createFromGlobals();
        $this->disable_rendering = true;
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $object = array();
        $response = new JsonResponse();
        $param = $request->get('param');
        $action = $request->get('action');
        $operator = $request->get('operator');
        $main_country = $request->get('main_country');
        $main_city = $request->get('main_city');
        $main_hotel = $request->get('main_hotel');
        $current_rel_hotel = $request->get('current_rel_hotel');
        $add = $request->get('add');
        $xml = $request->get('xml');
        $base = $request->get('base');
        $hotel_add = $request->get('hotel_add');

        if ($param == 'selected_operator') {
            if ($action == 'insert') {
                RelationTourtype::insert(array('operator' => $operator, 'base' => $base, 'xml' => $xml));
            }
            if ($action == 'delete') {
                RelationTourtype::delete(array('operator' => $operator, 'xml' => $xml));
            }

            // Страны оператора
            $operator_other = RelationTourtype::getTourtypeByOperator($operator);
            // Связи
            $otherRelated = RelationTourtype::getAllByOperator($operator);

            if (!empty($otherRelated)) {
                foreach ($otherRelated as $relOther) {
                    $sortBaseOther[$relOther['base']] = $relOther;
                    $sortXmlOther[$relOther['xml']] = $relOther;
                }
            } else {
                $sortBaseOther = array();
                $sortXmlOther = array();
            }

            // Все звезды
            $others = BaseMonCatalogTourtype::getAll();
            foreach ($others as $other) {
                $eachOther['related'] = isset($sortBaseOther[$other['id']]) ? true : false;
                $eachOther['id'] = $other['id'];
                $eachOther['name'] = $other['name'];

                // Отображается в левой панели. все страны с меткой true false
                $object['main_other'][$other['id']] = $eachOther;
            }

            // Формируем массивы приасаненых и неприасаненых стран
            foreach ($operator_other as $op_contr) {
                if ( isset($sortXmlOther[$op_contr['id']])) {
                    $op_contr['related_name'] = $object['main_other'][$sortXmlOther[$op_contr['id']]['base']]['name'];
                    $op_contr['related_id'] = $object['main_other'][$sortXmlOther[$op_contr['id']]['base']]['id'];
                    $object['operator_other_related'][] = $op_contr;
                } else {
                    $object['operator_other'][] = $op_contr;
                }
            }
        }
        $response->setData($object)->send();
    }

}
