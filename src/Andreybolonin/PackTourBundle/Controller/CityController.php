<?php

namespace Andreybolonin\PackTourBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CityController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $tour_manager = $this->get('tour_manager');
        $pdo = $this->get('pdo_master');

        $operators = $tour_manager->getOperatorsWithCount('city');
        $relations_count = $pdo->query('SELECT COUNT(id) FROM pack_tours.relation_city')->fetchColumn();
        $main_country = $pdo->query('SELECT id, name FROM geo.country')->fetchAll();

        return array(
            'relations_count' => $relations_count,
            'main_country' => $main_country,
            'operators' => $operators
        );
    }

    /**
     * @Template()
     */
    public function ajaxAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $operator_id = $request->get('operator_id');
        $response_data = array();
        $response = new JsonResponse();
        $tour_manager = $this->get('tour_manager');

        if ($operator_id) {
            $response_data = $tour_manager->getCityListByOperatorId($operator_id);
        }

        $response->setData($response_data)->send();
    }

    /**
     * @Template()
     */
    public function manageRelationAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response = new JsonResponse();
        $pdo = $this->get('pdo_master');
        $user_id = $this->get('security.context')->getToken()->getUserName();
        $tour_manager = $this->get('tour_manager');

        $action = $request->get('action');
        $operator_id = $request->get('operator_id');
        $main_city_id = $request->get('main_city_id');
        $operator_city_id = $request->get('operator_city_id');

        if ($action == 'add' && $operator_id && $main_city_id && $operator_city_id) {
            $sth = $pdo->prepare('INSERT INTO pack_tours.relation_city (operator, base, xml) VALUES (:operator, :base, :xml)');
            $sth->bindValue('operator', $operator_id);
            $sth->bindValue('base', $main_city_id);
            $sth->bindValue('xml', $operator_city_id);
            $sth->execute();
            $relation_id = $pdo->lastInsertId();
        }

        if ($action == 'remove' && $operator_id && $operator_city_id) {
            $sth = $pdo->prepare('SELECT id FROM pack_tours.relation_city WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $operator_city_id);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
            $relation_id = $sth->fetchColumn();

            $sth = $pdo->prepare('DELETE FROM pack_tours.relation_city WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $operator_city_id);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
        }

        // Логирование
        $sth = $pdo->prepare('INSERT INTO pack_tours.relation_log (entity_id, relation_id, action, user_id, created_at) VALUES (:entity_id, :relation_id, :action, :user_id, :created_at)');
        $sth->bindValue('entity_id', 'city');
        $sth->bindValue('relation_id', $relation_id);
        $sth->bindValue('action', $action);
        $sth->bindValue('user_id', $user_id);
        $sth->bindValue('created_at', date('Y-m-d H:i:s'));
        $sth->execute();

        $response_data = $tour_manager->getCityListByOperatorId($operator_id);
        $response->setData($response_data)->send();
    }

    /**
     * Добавление города
     */
    public function addAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response = new JsonResponse();
        $operator_id = $request->get('operator_id');
        $main_country_id = $request->get('main_country_id');
        $name = $request->get('name');
        $tour_manager = $this->get('tour_manager');

        $success = false;
        $message = 'Город не добавлен';

        if ($name && $operator_id && $main_country_id) {
            $tour_manager->addCity(array(
                'name' => $name,
                'country_id' => $main_country_id
            ));
            $success = true;
            $message = 'Город добавлен';
        }

        $response_data = $tour_manager->getCityListByOperatorId($operator_id);
        $response_data['success'] = $success;
        $response_data['message'] = $message;
        $response->setData($response_data)->send();
    }

}
