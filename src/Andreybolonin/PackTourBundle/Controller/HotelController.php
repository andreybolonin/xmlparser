<?php

namespace Andreybolonin\PackTourBundle\Controller;

use Andreybolonin\PackTourBundle\TourManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HotelController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $pdo = $this->get('pdo_master');
        $tour_manager = $this->get('tour_manager');

        $operators = $tour_manager->getOperatorsWithCount('hotel');
        $relations_count = $pdo->query('SELECT COUNT(id) FROM pack_tours.relation_hotel')->fetchColumn();
        $main_hotel = $pdo->query('SELECT id, name FROM base.hotel')->fetchAll();

        return array(
            'relations_count' => $relations_count,
            'operators' => $operators,
            'main_hotel' => $main_hotel
        );
    }

    /**
     * @Template()
     */
    public function ajaxAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response_data = array();
        $response = new JsonResponse();
        $operator_id = $request->get('operator_id');
        $country_id = $request->get('country_id');
        $tour_manager = $this->get('tour_manager');

        // Отдаем список отелей
        if ($operator_id && $country_id) {
            $response_data = $tour_manager->getHotelListByOperatorId($operator_id, $country_id);

            // Отдаем список стран оператора
        } elseif ($operator_id) {
            $response_data = $tour_manager->getHotelCountryList($operator_id);
        }

        $response->setData($response_data)->send();
    }

    /**
     * Управление связями отелей: добавление, удаление
     *
     * @Template()
     */
    public function manageRelationAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response = new JsonResponse();
        $pdo = $this->get('pdo_master');
        $user_id = $this->get('security.context')->getToken()->getUserName();
        $tour_manager = $this->get('tour_manager');

        $action = $request->get('action');
        $operator_id = $request->get('operator_id');
        $country_id = $request->get('country_id');
        $main_hotel_id = $request->get('main_hotel');
        $operator_hotel_id = $request->get('operator_hotel');

        if ($action == 'add' && $operator_id && $main_hotel_id && $operator_hotel_id) {
            $sth = $pdo->prepare('INSERT INTO pack_tours.relation_hotel (operator, base, xml) VALUES (:operator, :base, :xml)');
            $sth->bindValue('operator', $operator_id);
            $sth->bindValue('base', $main_hotel_id);
            $sth->bindValue('xml', $operator_hotel_id);
            $sth->execute();
            $relation_id = $pdo->lastInsertId();
        }

        if ($action == 'remove' && $operator_id && $operator_hotel_id) {
            $sth = $pdo->prepare('SELECT id FROM pack_tours.relation_hotel WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $operator_hotel_id);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
            $relation_id = $sth->fetchColumn();

            $sth = $pdo->prepare('DELETE FROM pack_tours.relation_hotel WHERE xml = :xml AND operator = :operator');
            $sth->bindParam('xml', $operator_hotel_id);
            $sth->bindParam('operator', $operator_id);
            $sth->execute();
        }

        // Логирование
        $sth = $pdo->prepare('INSERT INTO pack_tours.relation_log (entity_id, relation_id, action, user_id, created_at) VALUES (:entity_id, :relation_id, :action, :user_id, :created_at)');
        $sth->bindValue('entity_id', 'hotel');
        $sth->bindValue('relation_id', $relation_id);
        $sth->bindValue('action', $action);
        $sth->bindValue('user_id', $user_id);
        $sth->bindValue('created_at', date('Y-m-d H:i:s'));
        $sth->execute();

        $response_data = $tour_manager->getHotelListByOperatorId($operator_id, $country_id);
        $response->setData($response_data)->send();
    }

    public function addAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            $this->redirect('/');
        }

        $response = new JsonResponse();
        $operator_id = $request->get('operator_id');
        $name = $request->get('name');
//        $db = $this->get('db');
        $pdo_master = $this->get('pdo_master');
        // Operator city и country
        $country_id = $request->get('country_id');
        $city_id = $request->get('city_id');
        $tour_manager = $this->get('tour_manager');

        $success = false;
        $message = '';
        $hotel = '';

        // Получаем base city и country через relation, если не получили оба параметра, значит возвращаем false.
        // Если все получено, выполняем добавление отеля.
        if ($name && $operator_id && $country_id && $city_id) {
            $base_country_id = $tour_manager->getCountryId($operator_id, $country_id);
            $base_city_id = $tour_manager->getCityId($operator_id, $city_id);

            if ($base_country_id && $base_city_id) {
                $result = $tour_manager->addHotel(array(
                    'name' => $name,
                    'country_id' => $base_country_id,
                    'resort_id' => $base_city_id
                ));
                if ($result && $result['id'] > 0) {
//                    $hotel = $db->row('SELECT * FROM base.hotel WHERE id = ' . $result['id']);
                    $hotel = $pdo_master->query('SELECT * FROM base.hotel WHERE id = ' . $result['id'])->fetch();
                    $success = true;
                    $message = 'Отель успешно добавлен';
                } else {
                    $message = 'Отель не добавлен. Такой отель уже существует.';
                }
            } else {
                $message = 'Отель не добавлен. Для данного отеля не привязаны страна и/или город.';
            }
        }

        $response_data = $tour_manager->getHotelListByOperatorId($operator_id, $country_id);
        $response_data['success'] = $success;
        $response_data['message'] = $message;
        if (is_array($hotel)) {
            $response_data['hotel'] = $hotel;
        }
        $response->setData($response_data)->send();
    }

   /**
    * Статистика отелей
    *
    * @Template()
    */
    public function statAction()
    {
        $tour_manager = $this->get('tour_manager');
        $hotels = $tour_manager->findAllWithCountryAndResort();
        foreach ($hotels as $key => $val) {
            $hotels[$key]['status_name'] = TourManager::$hotel_status_map[$val['status']];
        }

        return array('hotels' => $hotels);
    }

}
