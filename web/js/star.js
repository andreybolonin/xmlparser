$(document).ready(function() {

    //  Выбор оператора
    $("select[name='operator_id']").on('change', function () {
        if($(this).val() != 'no') {
            var action = document.getElementsByTagName("FORM")[0].getAttribute("action");
            $.ajax({
                type: 'POST',
                url: action,
                dataType: 'json',
                data: ({
                    operator_id: $('select[name="operator_id"]').val()
                }),
                success: function(response){
                    update_tourobzor(response);
                    getInfo(response);
                }
            });
        } else {
            $('.right_loop_scroll ul').empty();
            $('.right_loop_scroll_backup ul').empty();
        }
    });

    // Управление связями
    $(document).on('change', "input[name='related_other']", function () {
        var url_action = document.getElementsByTagName("FORM")[0].getAttribute("action_manage");
        var action = $(this).is(':checked') ? 'add' : 'remove';
        if (!$("input[name='main_star']").is(':checked') && $(this).is(':checked')) {
            alert('Сначале выберите звезду в левом окне');
            $(this).attr('checked', false);
        } else {
            $.ajax({
                type: 'POST',
                url: url_action,
                dataType: 'json',
                data: ({
                    operator_id: $("select[name='operator_id']").val(),
                    base: $("input[name='main_star']:checked").val(),
                    xml: $(this).val(),
                    action: action
                }),
                success: function(response){
                    getInfo(response);
                }
            });
        }
    });
});

function getInfo(response) {
    $('.right_loop_scroll ul').empty();
    $('.right_loop_scroll_backup ul').empty();

    if(response.operator) {
        $.each(response.operator, function(i, val) {
            $('<li><label><input type="radio" name="related_other" value="' + val.id + '" >' + val.star + /*'    id:'+val.id +*/ '</label></li>').appendTo(".right_loop_scroll ul");
        });
    }

    if (response.related) {
        $.each(response.related, function(i, val) {
            $('<li><label><input type="checkbox" checked="checked" main="' + val.related_id + '" name="related_other" value="' + val.id + '" >' + val.star + /*'    id:'+val.id +*/' (' + val.related_name + ')</label></li>').appendTo(".right_loop_scroll_backup ul");
        });
    }
}

function update_tourobzor(response) {
    $('.left_loop_scroll ul').empty();

    $.each(response.main, function(i, val) {
        var bold = null;
        if (val.related === true) {
            bold = 'class="bold"';
        }
        $('<li><label><input type="radio" name="main_star" value="' + val.id + '" /><span '+ bold +'>' + val.name + /*'    id:'+val.id +*/'</span></label></li>').appendTo(".left_loop_scroll ul");
    });
}
